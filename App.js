import React from 'react';
import {theme} from './src/core/theme';
import {Provider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import SplashScreen from './src/screens/SplashScreen';
import Login from './src/screens/Authorize/Login';
import Dashboard from './src/screens/Dashboard/Dashboard';
import Register from './src/screens/Authorize/Register';
import Setting from './src/screens/Setting/Setting';
import UpdateInformation from './src/screens/ManagerAcount/Information/UpdateInformation';
import AddPhoneNumber from './src/screens/ManagerAcount/Information/AddPhoneNumber';
import ChangePassword from './src/screens/Authorize/ChangePassword';
import ManagerInformation from './src/screens/ManagerAcount/Information/ManagerInformation';
import ListPost from './src/screens/ManagerAcount/ManagerPost/ListPost';
import DetailPost from './src/screens/ManagerAcount/ManagerPost/DetailPost';
import FilterPost from './src/screens/ManagerAcount/ManagerPost/FilterPost';
import InformationBalance from './src/screens/ManagerAcount/ManagerTransaction/InformationBalance';
import Information from './src/screens/ManagerAcount/Information/Information';
import DetailPostDashboard from './src/screens/Dashboard/DetailPostDashboard';
import Maps from './src/screens/Maps/Maps';
import Post from './src/screens/Post/Post';
import FilterPostDashboard from './src/screens/Dashboard/FilterPostDashboard';
import Search from './src/screens/Dashboard/Search';
import HistoryTransaction from './src/screens/ManagerAcount/ManagerTransaction/HistoryTransaction';
import ListPromotion from './src/screens/ManagerAcount/ManagerPromotion/ListPromotion';
import DetailPromotion from './src/screens/ManagerAcount/ManagerPromotion/DetailPromotion';
import MethodDeposit from './src/screens/ManagerAcount/ManagerDeposit/MethodDeposit';
import DetailDeposit from './src/screens/ManagerAcount/ManagerDeposit/DetailDeposit';
import HistoryBalance from './src/screens/ManagerAcount/ManagerTransaction/HistoryBalance';
import ForgotPassword from './src/screens/Authorize/ForgotPassword';
import CheckOtp from './src/screens/Authorize/CheckOtp';
import NewPassword from './src/screens/Authorize/NewPassword';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
function Root() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{
          swipeEnabled: false,
        }}
      />
      <Drawer.Screen
        name="Dashboard"
        component={Dashboard}
        options={{
          swipeEnabled: false,
        }}
      />
      <Drawer.Screen
        name="Maps"
        component={Maps}
        options={{
          swipeEnabled: false,
        }}
      />
      <Stack.Screen
        name="Post"
        component={Post}
        options={{unmountOnBlur: true, swipeEnabled: false}}
      />
      <Drawer.Screen
        name="ManagerInformation"
        component={ManagerInformation}
        options={{
          swipeEnabled: false,
        }}
      />
      <Drawer.Screen
        name="Login"
        component={Login}
        options={{unmountOnBlur: true, swipeEnabled: false}}
      />
      <Drawer.Screen
        name="Setting"
        component={Setting}
        options={{
          swipeEnabled: false,
        }}
      />
    </Drawer.Navigator>
  );
}
export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Root"
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="Root" component={Root} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
          <Stack.Screen name="CheckOtp" component={CheckOtp} />
          <Stack.Screen name="NewPassword" component={NewPassword} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen
            name="UpdateInformation"
            component={UpdateInformation}
          />
          <Stack.Screen name="AddPhoneNumber" component={AddPhoneNumber} />
          <Stack.Screen name="ChangePassword" component={ChangePassword} />
          <Stack.Screen name="Information" component={Information} />
          <Stack.Screen name="ListPost" component={ListPost} />
          <Stack.Screen name="DetailPost" component={DetailPost} />
          <Stack.Screen
            name="FilterPostDashboard"
            component={FilterPostDashboard}
          />
          <Stack.Screen
            name="InformationBalance"
            component={InformationBalance}
          />
          <Stack.Screen
            name="DetailPostDashboard"
            component={DetailPostDashboard}
          />
          <Stack.Screen name="Search" component={Search} />
          <Stack.Screen
            name="HistoryTransaction"
            component={HistoryTransaction}
          />
          <Stack.Screen name="HistoryBalance" component={HistoryBalance} />
          <Stack.Screen name="ListPromotion" component={ListPromotion} />
          <Stack.Screen name="DetailPromotion" component={DetailPromotion} />
          <Stack.Screen name="MethodDeposit" component={MethodDeposit} />
          <Stack.Screen name="DetailDeposit" component={DetailDeposit} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
