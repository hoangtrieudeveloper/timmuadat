import React, {Component} from 'react';
import {StyleSheet, Image, View} from 'react-native';
import PropTypes from 'prop-types';
type Props = {};
import BottomNavigation, {
  FullTab,
} from 'react-native-material-bottom-navigation';
import {NavigationActions, StackActions} from 'react-navigation';
import {Dimensions} from 'react-native';
import SanFrancisco from './SanFrancisco';

const screenWidth = Dimensions.get('window').width;

export class MainBottomTabComponent extends Component<Props> {
  static propTypes = {
    activeTab: PropTypes.string,
    navigation: PropTypes.object,
  };

  constructor(props) {
    super(props);
  }

  state = {
    activeTab: this.props.activeTab,
  };

  tabs = [
    {
      key: 'home',
      label: 'Trang chủ',
      barColor: '#f6f6f6',
      pressColor: '#999999',
      icon: require('../assets/icon_bottomTab/home.png'),
      icon_active: require('../assets/icon_bottomTab/home_active.png'),
      router: 'Dashboard',
    },
    {
      key: 'maps',
      label: 'Bản đồ',
      barColor: '#afafaf',
      pressColor: '#999999',
      icon: require('../assets/icon_bottomTab/map.png'),
      icon_active: require('../assets/icon_bottomTab/map_active.png'),
      router: 'Maps',
    },
    {
      key: 'edit',
      label: '',
      barColor: '#afafaf',
      pressColor: 'rgba(205, 200, 209, 0.16)',
      icon: require('../assets/icon_bottomTab/pencil.png'),
      icon_active: require('../assets/icon_bottomTab/pencil.png'),
      router:
        !global.token ||
        global.token === '' ||
        global.token === null ||
        global.token === undefined
          ? 'Login'
          : 'Post',
    },
    {
      key: 'login',
      label: 'Tài khoản',
      barColor: '#afafaf',
      pressColor: '#999999',
      icon: require('../assets/icon_bottomTab/user.png'),
      icon_active: require('../assets/icon_bottomTab/user_active.png'),
      router:
        !global.token ||
        global.token === '' ||
        global.token === null ||
        global.token === undefined
          ? 'Login'
          : 'ManagerInformation',
    },
    {
      key: 'setting',
      label: 'Thêm',
      barColor: '#afafaf',
      pressColor: '#999999',
      icon: require('../assets/icon_bottomTab/align-left.png'),
      icon_active: require('../assets/icon_bottomTab/align-left_active.png'),
      router: 'Setting',
    },
  ];
  renderTab = ({tab, isActive}) => (
    <FullTab
      style={[
        tab.key === 'edit'
          ? {
              backgroundColor: isActive ? '#ffffff' : '#ffffff',
              height: 100,
              minWidth: 80 / this.tabs.length + '%',
              maxWidth: 80 / this.tabs.length + '%',
              position: 'relative',
              bottom: 25,
              borderRadius: 50,
            }
          : {
              backgroundColor: isActive ? '#ffffff' : '#ffffff',
              marginBottom: 0,
              paddingBottom: 6,
              minWidth: 107.5 / this.tabs.length + '%',
              maxWidth: 107.5 / this.tabs.length + '%',
            },
      ]}
      isActive={isActive}
      key={tab.key}
      label={tab.key !== 'edit' ? tab.label : ''}
      labelStyle={{
        color: isActive ? '#004182' : '#999999',
        ...SanFrancisco.medium,
        fontSize: 10,
      }}
      renderIcon={this.renderIcon(
        isActive ? tab.icon_active : tab.icon,
        isActive ? '#004182' : '#999999',
        tab,
        tab.key,
      )}
    />
  );

  renderIcon = (icon, color, tab, key) => ({isActive}) => {
    return key === 'edit' ? (
      <View
        style={{
          width: 56,
          height: 56,
          backgroundColor: '#429134',
          textAlign: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 50,
          position: 'relative',
          bottom: 7,
        }}>
        <Image
          style={{
            height: 22,
            width: 22,
          }}
          source={icon}
        />
      </View>
    ) : (
      <Image
        style={{height: 20, width: 20, marginTop: 0, marginBottom: 3}}
        source={icon}
      />
    );
  };

  render() {
    return (
      <BottomNavigation
        tabs={this.tabs}
        activeTab={this.state.activeTab}
        onTabPress={newTab => this.props.navigation.navigate(newTab.router)}
        renderTab={this.renderTab}
        useLayoutAnimation={true}
        style={{
          height: 49,
          flexDirection: 'column',
          position: 'relative',
          paddingBottom: 0,
          borderTopWidth: 0.5,
          borderTopColor: '#0000004D',
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  itemTab: {
    lineHeight: 40,
    backgroundColor: '#ee78b7',
    textAlign: 'center',
    color: '#fff',
    borderStyle: 'solid',
    borderColor: '#ee78b7',
    borderBottomWidth: 4,
  },
  itemTabActive: {
    lineHeight: 40,
    backgroundColor: '#ee78b7',
    textAlign: 'center',
    color: '#fff',
    borderStyle: 'solid',
    borderColor: '#FFC000',
    borderBottomWidth: 4,
  },
});
