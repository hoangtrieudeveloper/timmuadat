import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
} from 'react-native';
import SanFrancisco from '../SanFrancisco';
import Const from '../Const';
import Selection from '../Selection';
import TextInput from '../TextInput';
import {WalletModel} from '../../models/wallet';

function ListBank({visible, onChange, data, keyFilter, header, ...props}) {
  const [filterObject, setFilterObject] = useState('');
  const [object, setObject] = useState(data || '');
  const [listPlaceData, setListPlaceData] = useState([]);
  const [listPlaceDataRoot, setListPlaceDataRoot] = useState([]);
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        borderBottomWidth: 1,
        borderBottomColor: '#E6E6E6',
        paddingTop: 12,
        paddingBottom: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}
      onPress={() => setWallet(item)}>
      <Text
        style={[
          {
            ...SanFrancisco.regular,
            fontSize: 17,
            lineHeight: 22,
            color: Const.BLACK_ROOT,
          },
          item.id === object.id
            ? {color: Const.BLUE}
            : {
                color: Const.BLACK_ROOT,
              },
        ]}>
        {item.bank_name}
      </Text>
      {item.id === object.id ? (
        <Image source={require('../../assets/post/Symbol.png')} />
      ) : null}
    </TouchableOpacity>
  );
  useEffect(() => {
    if (!visible) {
      return;
    }
    setObject(data);
    setListPlaceData([]);
    setListPlaceDataRoot([]);
    WalletModel.bank_template()
      .then(data => {
        if (data.status == 1) {
          setListPlaceData(data.data);
          setListPlaceDataRoot(data.data);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }, [visible]);

  const setWallet = item => {
    onChange(item);
  };

  if (!visible) {
    return <View />;
  }
  const onChangeHandler = () => {
    let Search = filterObject;
    console.log('Search', listPlaceData);
    if (Search !== '') {
      //const b = listPlaceData.filter(item => Slug(item.name) === Slug(Search));
      const b = listPlaceData.filter(
        item =>
          item &&
          item.bank_name &&
          item.bank_name.toLowerCase().includes(Search.toLowerCase()),
      );
      setListPlaceData(b);
    } else {
      setListPlaceData(listPlaceDataRoot);
    }
  };

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            marginBottom: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#004182',
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
            // borderTopLeftRadius: 10,
            // borderTopRightRadius: 10,
          }}>
          <View />
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.WHITE,
              marginTop: 10,
            }}>
            {header}
          </Text>
          <TouchableOpacity onPress={() => setWallet('')} style={{}}>
            <Image
              source={require('../../assets/Acount/MethodDeposit/BarsLight.png')}
            />
          </TouchableOpacity>
        </View>
        <Selection size={16} style={{marginBottom: 120}}>
          <View style={{paddingBottom: 8}}>
            <TouchableOpacity
              onPress={() => {
                onChangeHandler();
              }}
              style={{
                position: 'absolute',
                padding: 10,
                paddingRight: 4,
                zIndex: 9999,
              }}>
              <Image
                source={require('../../assets/dashboard/SearchGlyphLight.png')}
              />
            </TouchableOpacity>
            <TextInput
              style={[
                {
                  backgroundColor: '#7676801F',
                  height: 44,
                  paddingLeft: 30,
                  fontSize: 17,
                  letterSpacing: -0.3,
                  ...SanFrancisco.regular,
                  color: '#3C3C4399',
                  borderTopRightRadius: 10,
                  borderTopLeftRadius: 10,
                  borderBottomRightRadius: 10,
                  borderBottomLeftRadius: 10,
                  textAlign: 'left',
                },
              ]}
              underlineColor="transparent"
              placeholder="Tìm kiếm..."
              placeholderTextColor="#999999"
              returnKeyType="next"
              // value={filterObject.value}
              onChangeText={text => setFilterObject(text)}
              // error={!!filterObject.error}
              // errorText={filterObject.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
              marginVerNone={0}
            />
          </View>
          <FlatList data={listPlaceData} renderItem={renderItem} />
        </Selection>
      </View>
    </View>
  );
}

export default ListBank;
