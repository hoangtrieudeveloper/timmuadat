import React from 'react';
import {Text, View, Image, ActivityIndicator} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import UploadImagePicker from '../components/UploadImagePicker';

function UploadImage({width = 100, height = 100, value, onChange = () => {}}) {
  return (
    <UploadImagePicker value={value} onChange={onChange}>
      {({loading, photo, err}) => {
        return (
          <View
            style={{
              position: 'relative',
            }}>
            {err?.message && <Text style={{color: 'red'}}>{err?.message}</Text>}
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width,
                height,
                borderWidth: 1,
                borderColor: '#ccc',
                borderRadius: 5,
              }}>
              {!photo?.uri ? (
                <MaterialCommunityIcons
                  name="cloud-upload-outline"
                  size={32}
                  color="#666"
                />
              ) : (
                []
              )}
              {loading ? (
                <>
                  <View
                    style={{
                      position: 'absolute',
                      zIndex: 1,
                      backgroundColor: '#222',
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      opacity: 0.5,
                    }}
                  />
                  <View
                    style={{
                      position: 'absolute',
                      zIndex: 1,
                    }}>
                    <ActivityIndicator size="large" color="white" />
                  </View>
                </>
              ) : (
                []
              )}
              {photo?.uri ? (
                <Image
                  source={photo}
                  style={{
                    width,
                    height,
                    borderWidth: 1,
                    borderColor: '#ccc',
                    borderRadius: 5,
                    resizeMode: 'cover',
                  }}
                />
              ) : (
                []
              )}
            </View>
          </View>
        );
      }}
    </UploadImagePicker>
  );
}

export default UploadImage;
