import React, {useState, useEffect} from 'react';
import {Animated, TouchableWithoutFeedback} from 'react-native';

const listColor = {
  primary: ['#eee', '#00B59C'],
  default: ['#ddd', '#45c47c'],
};

function CustomSwitch({color, value, onChange, size = 24}) {
  const [backgroundColor] = useState(new Animated.Value(value ? 75 : -75));
  const [transformSwitch] = useState(new Animated.Value(size));
  const ratio = 1.82;

  const interpolatedColorAnimation = backgroundColor.interpolate({
    inputRange: [-75, 75],
    outputRange: listColor[color] || listColor.default,
  });

  useEffect(() => {
    const a = Animated.parallel([
      Animated.timing(transformSwitch, {
        toValue: !value ? 1 : size * ratio - size,
        duration: 200,
        useNativeDriver: false,
      }),
      Animated.timing(backgroundColor, {
        toValue: value ? 75 : -75,
        duration: 200,
        useNativeDriver: false,
      }),
    ]);
    a.start();

    return () => a.stop();
  }, [value]);

  return (
    <TouchableWithoutFeedback
      style={{
        height: size,
        width: size * ratio,
      }}
      onPress={() => {
        onChange(!value);
      }}>
      <Animated.View
        style={{
          height: size,
          width: size * ratio,
          backgroundColor: interpolatedColorAnimation,
          borderRadius: 25,
        }}>
        <Animated.View
          style={{
            height: size - 2,
            width: size - 2,
            left: transformSwitch,
            top: 1,
            borderRadius: 50,
            backgroundColor: '#fff',
          }}
        />
      </Animated.View>
    </TouchableWithoutFeedback>
  );
}

CustomSwitch.defaultProps = {
  onChange: () => {},
  value: false,
};

export default CustomSwitch;
