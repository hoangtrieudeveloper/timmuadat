import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {Dimensions, View} from 'react-native';
const {width, height} = Dimensions.get('screen');
function LoadingSkeleton({loading, children, count, skeleton, ...props}) {
  if (!loading) {
    return <>{children}</>;
  }

  return (
    <>
      {new Array(count).fill(0).map(x => (
        <View {...props}>{skeleton}</View>
      ))}
    </>
  );
}

LoadingSkeleton.defaultProps = {
  loading: false,
  count: 3,
  skeleton: <DefultSkeleton />,
};

function DefultSkeleton(props) {
  return (
    <SkeletonPlaceholder>
      <View
        style={{
          alignItems: 'center',
          alignSelf: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          marginBottom: 20,
          marginLeft: 16,
          marginRight: 16,
        }}>
        <View>
          <View
            style={{
              marginTop: 6,
              width: '100%',
              height: 155,
              borderRadius: 10,
            }}
          />
          <View
            style={{
              marginTop: 6,
              width: width - 32,
              height: 16,
              borderRadius: 4,
            }}
          />
          <View
            style={{
              marginTop: 6,
              width: width - 150,
              height: 16,
              borderRadius: 4,
            }}
          />
          <View
            style={{
              marginTop: 6,
              width: width - 200,
              height: 16,
              borderRadius: 4,
            }}
          />
          <View
            style={{
              marginTop: 32,
              width: '100%',
              height: 50,
              borderRadius: 4,
              marginRight: 16,
            }}
          />
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <View
              style={{
                width: 150,
                height: 93,
                borderRadius: 4,
                marginRight: 16,
              }}
            />
            <View>
              <View
                style={{
                  width: 120,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 80,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 160,
                  height: 16,
                  borderRadius: 4,
                }}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <View
              style={{
                width: 150,
                height: 93,
                borderRadius: 4,
                marginRight: 16,
              }}
            />
            <View>
              <View
                style={{
                  width: 120,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 80,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 160,
                  height: 16,
                  borderRadius: 4,
                }}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <View
              style={{
                width: 150,
                height: 93,
                borderRadius: 4,
                marginRight: 16,
              }}
            />
            <View>
              <View
                style={{
                  width: 120,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 80,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 160,
                  height: 16,
                  borderRadius: 4,
                }}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <View
              style={{
                width: 150,
                height: 93,
                borderRadius: 4,
                marginRight: 16,
              }}
            />
            <View>
              <View
                style={{
                  width: 120,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 80,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 160,
                  height: 16,
                  borderRadius: 4,
                }}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <View
              style={{
                width: 150,
                height: 93,
                borderRadius: 4,
                marginRight: 16,
              }}
            />
            <View>
              <View
                style={{
                  width: 120,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 80,
                  height: 16,
                  borderRadius: 4,
                  marginBottom: 16,
                }}
              />
              <View
                style={{
                  marginTop: 6,
                  width: 160,
                  height: 16,
                  borderRadius: 4,
                }}
              />
            </View>
          </View>
        </View>
      </View>
    </SkeletonPlaceholder>
  );
}

export default LoadingSkeleton;
