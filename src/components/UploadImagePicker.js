import React from 'react';
import {TouchableOpacity, PermissionsAndroid} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {ProductModel} from '../models/product';

function UploadImagePicker({value, children, onChange = () => {}, ...props}) {
  const [loading, setLoading] = React.useState(0);
  const [photo, setPhoto] = React.useState(value);
  const [err, setErr] = React.useState({});

  const handle = async () => {
    const options = {
      title: '',
      cancelButtonTitle: 'Đóng',
      didCancel: true,
      chooseFromLibraryButtonTitle: 'Chọn từ thư viện',
      takePhotoButtonTitle: 'Ảnh mới',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        privateDirectory: true,
      },
      permissionDenied: {
        title: 'Quyền bị từ chối',
        text:
          'Bạn không có quyền truy cập vào máy ảnh, thư viện ảnh và bộ nhớ máy.',
        reTryTitle: 'Thử lại',
        okTitle: 'Đồng ý',
      },
    };

    ImagePicker.showImagePicker(options, ({data, ...response}) => {
      console.log('data', response.didCancel);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        setErr({message: response.error});
        console.log('ImagePicker Error: ', response.error);
      } else {
        setErr({});
        setLoading(1);
        const photo = {
          uri: response.uri.replace('file://', ''),
          name: response.fileName || response.uri.split('/').pop(),
          type: response.type,
        };

        setPhoto(photo);

        const form = new FormData();

        form.append('image', photo);
        ProductModel.uploadFile(response)
          .then(data => {
            if (data.status == 1) {
              console.log('data', data);
              setPhoto({
                uri: data?.data?.full_path,
              });
              onChange({uri: data?.data?.full_path});
            }
          })
          .catch(err => {
            setErr(err);
          })
          .finally(() => {
            setLoading(0);
          });
      }
    });
  };
  return (
    <TouchableOpacity
      onPress={() => {
        handle();
      }}>
      {children({photo, loading, err})}
    </TouchableOpacity>
  );
}

export default UploadImagePicker;
