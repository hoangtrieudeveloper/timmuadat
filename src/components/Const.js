import React, {Component} from 'react';

export class Const extends Component {
  static WHITE = '#ffffff';
  static WHITE_v2 = '#FAFAFA';
  static BLACK = '#232522';
  static BLACK_ROOT = '#000000';
  static BLUE = '#004182';
  static BLUE_v2 = '#007AFF';
  static ORANGE = '#FF9500';
  static ORANGE_v2 = '#FF950029';
  static GRAY = '#F5F5F5';
  static GRAY_v2 = '#3C3C4399';
  static GRAY_v3 = '#7676801F';
  static GRAY_v4 = '#777777';
  static GRAY_v5 = '#999999';
  static GRAY_v6 = '#E6E6E6';
  static GRAY_v7 = '#F7F7F7';
  static GRAY_v8 = '#C7C7CC';
  static RED = '#FF3B30';
  static RED_v2 = '#FF453A';
  static GREEN = '#429134';
  static PINK = '#F4F4F5';
}

export default Const;
