import {StyleSheet, Platform} from 'react-native';

const SanFrancisco = StyleSheet.create({
  ultralight: {
    fontFamily:
      Platform.OS === 'android' ? 'SanFranciscoDisplay-Ultralight' : 'System',
    fontWeight: '200',
  },
  regular: {
    fontFamily:
      Platform.OS === 'android' ? 'SanFranciscoDisplay-Regular' : 'System',
    fontWeight: '300',
  },
  medium: {
    fontFamily:
      Platform.OS === 'android' ? 'SanFranciscoDisplay-Medium' : 'System',
    fontWeight: '400',
  },
  semiBold: {
    fontFamily:
      Platform.OS === 'android' ? 'SanFranciscoDisplay-Semibold' : 'System',
    fontWeight: '500',
  },

  // semiBold: {
  //     fontFamily:
  //         Platform.OS === 'android'
  //             ? 'SanFranciscoDisplay-Semibold'
  //             : 'System',
  //     fontWeight: '600',
  // },
});

export default SanFrancisco;
