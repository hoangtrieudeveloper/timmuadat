import React from 'react';
import {View, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {TextButton} from 'react-native-material-buttons';
const renderColor = type => {
  if (type === 'danger') {
    return ['#ED1B24', '#ED1B24'];
  }
  if (type === 'default') {
    return ['#C4C4C4', '#C4C4C4'];
  }
  if (type === 'primary') {
    return ['#19769F', '#19769F'];
  }
  if (type === 'yellow') {
    return ['#FF9933', '#FF9933'];
  }
  if (type === 'green') {
    return ['#04D08C', '#04D08C'];
  }
  if (type === 'blue') {
    return ['#004182', '#004182'];
  }
  return ['#fff', '#fff'];
};

export default function Button({
  style,
  loading,
  title,
  type,
  titleColor = '#fff',
  disabledColor = '#fff',
  ...rest
}) {
  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 0, y: 0}}
      colors={renderColor(type)}
      style={[styles.field, style]}>
      <TextButton
        {...rest}
        disabledTitleColor={disabledColor}
        titleColor={titleColor}
        style={[{color: '#fff'}, styles.textField, styles.field, style]}
        title={loading ? 'Loading' : title}
      />
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    marginVertical: 10,
    paddingVertical: 2,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 26,
  },
  lightTitle: {
    fontSize: 18,
    fontWeight: '700',
  },
  textField: {
    fontSize: 15,
  },
  fieldTitle: {
    fontSize: 16,
    fontWeight: '700',
  },
  error: {
    fontSize: 14,
  },
  container: {
    alignSelf: 'stretch',
  },
  field: {
    textTransform: 'lowercase',
  },
});
