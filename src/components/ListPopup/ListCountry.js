import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import styles from '../../core/Component/listcountry/styles';
import {Users} from '../../models/users';

function PopupListCountry({visible, onChange, data, ...props}) {
  const [walletId, setWalletId] = useState(data);
  const [listPlaceData, setListPlaceData] = useState([]);
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={[styles.boxTextPosition]}
      onPress={() => setWallet(item)}>
      <Text
        style={[
          styles.textPosition,
          item.value === walletId ? styles.boxTextPositionActive : '',
        ]}>
        {item.name}
      </Text>
    </TouchableOpacity>
  );
  useEffect(() => {
    if (!visible) {
      return;
    }
    setWalletId(data);
    Users.list_countryAction()
      .then(data => {
        const PackageListData = [];
        if (data.status === 1) {
          data.data.forEach(item => {
            PackageListData.push({
              name: item.name,
              value: item.id,
            });
          });
          setListPlaceData(PackageListData);
        }
      })
      .catch(e => {
        console.log('e', e);
      });
  }, [visible]);

  const setWallet = item => {
    onChange(item);
  };

  if (!visible) {
    return <View />;
  }

  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        position: 'absolute',
        zIndex: 9999999,
      }}>
      <View
        style={{
          width: '100%',
          height: '100%',
          flex: 1,
          zIndex: 9999999,
        }}
      />
      <View
        style={{
          height: 200,
          backgroundColor: '#253042',
          borderTopRightRadius: 10,
          borderTopLeftRadius: 10,
          padding: 15,
          marginBottom: 0,
        }}>
        <View style={{}}>
          <Text
            style={{
              fontWeight: '500',
              fontSize: 14,
              color: '#FFF',
              textAlign: 'center',
              marginBottom: 10,
            }}>
            COUNTRY
          </Text>
        </View>
        <FlatList data={listPlaceData} renderItem={renderItem} />
      </View>
    </View>
  );
}

export default PopupListCountry;
