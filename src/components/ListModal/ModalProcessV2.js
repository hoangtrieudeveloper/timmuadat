import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import SanFrancisco from '../SanFrancisco';
import Const from '../Const';
import Selection from '../Selection';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

function ModalProcessV2({
  visible,
  onChange,
  title,
  content,
  typeButton = 1,
  data,
  id = '',
  titleOk = 'OK',
  titleBack = 'Cancel',
  ...props
}) {
  useEffect(() => {
    if (!visible) {
      return;
    }
  }, [visible]);

  const setWallet = item => {
    onChange(item);
  };

  if (!visible) {
    return <View />;
  }

  return (
    <Modal
      isVisible={visible}
      style={{
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        zIndex: 9999,
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          borderRadius: 14,
          width: 270,
        }}>
        <Selection size={16} style={{paddingBottom: 16}}>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 19,
              textAlign: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              marginBottom: 8,
            }}>
            <MaterialCommunityIcons
              name={'checkbox-marked'}
              size={20}
              color={typeButton === 2 ? '#429134' : Const.RED}
            />
            <Text
              style={{
                ...SanFrancisco.semiBold,
                fontSize: 17,
                lineHeight: 22,
                color: typeButton === 2 ? '#429134' : Const.RED,
                marginLeft: 8,
              }}>
              {title}
            </Text>
          </View>
          <Text
            style={{
              ...SanFrancisco.regular,
              fontSize: 14,
              lineHeight: 16,
              color: Const.BLACK,
              textAlign: 'center',
              marginLeft: 16,
              marginRight: 16,
            }}>
            {content}
          </Text>
          {id !== '' ? (
            <View style={{marginTop: 16}}>
              <Text
                style={{
                  ...SanFrancisco.regular,
                  fontSize: 14,
                  lineHeight: 16,
                  color: Const.BLACK,
                  textAlign: 'center',
                  marginLeft: 16,
                  marginRight: 16,
                }}>
                Mã đăng tin: <Text style={{fontWeight: 'bold'}}>{id}</Text>
              </Text>
            </View>
          ) : null}
        </Selection>
        {typeButton == 1 ? (
          <TouchableOpacity
            onPress={() => setWallet(3)}
            style={{
              textAlign: 'center',
              alignItems: 'center',
              backgroundColor: '#004182',
              borderBottomLeftRadius: 14,
              borderBottomRightRadius: 14,
              paddingTop: 12,
              paddingBottom: 12,
            }}>
            <Text
              style={{
                ...SanFrancisco.semiBold,
                fontSize: 17,
                lineHeight: 22,
                color: Const.WHITE,
              }}>
              {titleOk}
            </Text>
          </TouchableOpacity>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              borderTopWidth: 1,
              borderTopColor: '#E6E6E6',
            }}>
            <TouchableOpacity
              onPress={() => setWallet(1)}
              style={{
                paddingTop: 12,
                paddingBottom: 12,
                borderRightWidth: 0.5,
                borderRightColor: '#E6E6E6',
                width: '50%',
              }}>
              <Text
                style={{
                  ...SanFrancisco.regular,
                  fontSize: 17,
                  lineHeight: 22,
                  color: Const.BLUE,
                  textAlign: 'center',
                  alignItems: 'center',
                  alignContent: 'center',
                  alignSelf: 'center',
                }}>
                {titleOk}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setWallet(2)}
              style={{
                textAlign: 'center',
                alignItems: 'center',
                paddingTop: 12,
                paddingBottom: 12,
                width: '50%',
              }}>
              <Text
                style={{
                  ...SanFrancisco.semiBold,
                  fontSize: 17,
                  lineHeight: 22,
                  color: Const.GRAY_v4,
                }}>
                {titleBack}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </Modal>
  );
}

export default ModalProcessV2;
