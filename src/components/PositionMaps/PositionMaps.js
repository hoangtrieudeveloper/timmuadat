import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, Image, Dimensions} from 'react-native';
import SanFrancisco from '../SanFrancisco';
import Const from '../Const';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';

const {width, height} = Dimensions.get('screen');

function PositionMaps({visible, onChange, data, ...props}) {
  //Map
  const ASPECT_RATIO = width / height;
  const LATITUDE_DELTA = 0.005; // Mức thu phóng rất cao
  const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
  const [listMarker, setListMarker] = useState([
    data || {
      latitude: 21.0277644,
      longitude: 105.8341598,
    },
  ]);
  const [region, setRegion] = useState({
    latitude: 21.0277644,
    longitude: 105.8341598,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  useEffect(() => {
    if (!visible) {
      return;
    }
    setListMarker(data);
  }, [visible]);

  const senData = () => {
    onChange(listMarker);
  };

  if (!visible) {
    return <View />;
  }

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            marginBottom: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#F9F9F9F0',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
          }}>
          <TouchableOpacity onPress={() => senData()}>
            <Image source={require('../../assets/back.png')} />
          </TouchableOpacity>
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.BLACK_ROOT,
            }}>
            Vị trí
          </Text>
          <TouchableOpacity onPress={() => senData()}>
            <Text
              style={{
                ...SanFrancisco.regular,
                fontSize: 17,
                lineHeight: 20,
                color: Const.BLUE,
              }}>
              Xong
            </Text>
          </TouchableOpacity>
        </View>
        <MapView
          style={{flex: 1, height: height}}
          initialRegion={region}
          onRegionChangeComplete={coordinate => {
            console.log('coordinatezxc', coordinate);
          }}
          onPress={e => {
            console.log('e.nativeEvent.coordinate', e.nativeEvent);
            // setListMarker([...listMarker, e.nativeEvent.coordinate]);
            setListMarker([e.nativeEvent.coordinate]);
          }}
          region={region}
          showsUserLocation
          provider={PROVIDER_GOOGLE}>
          {listMarker.map(marker => (
            <Marker coordinate={marker} />
          ))}
        </MapView>
        {/*<TouchableOpacity*/}
        {/*  style={{*/}
        {/*    position: 'absolute',*/}
        {/*    top: 200,*/}
        {/*    right: 20,*/}
        {/*    zIndex: 99999,*/}
        {/*  }}*/}
        {/*  activeOpacity={0.9}*/}
        {/*  onPress={() => handleToCenter(center)}>*/}
        {/*  <Text>Zoom</Text>*/}
        {/*</TouchableOpacity>*/}
      </View>
    </View>
  );
}

export default PositionMaps;
