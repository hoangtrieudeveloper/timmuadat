import React, {useState, useEffect} from 'react';
import DatePicker from 'react-native-date-picker';
import Dialog, {
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import {ScrollView, Text, View, TouchableOpacity} from 'react-native';
import moment from 'moment';
export default function CustomDatePicker({date, style, visible, ...props}) {
  const [dateState, setDateState] = useState(new Date());

  const setVisible = visible => {
    props.onChangeVisible(visible);
  };
  const acceptHandler = date => {
    props.onChangeDate(dateState);
    setVisible(false);
  };

  useEffect(() => {
    if (visible) {
      if (date instanceof moment) {
        // eslint-disable-next-line no-underscore-dangle
        setDateState(date._d);
      } else {
        setDateState(date);
      }
    }
  }, [visible]);

  return (
    <View>
      {/*<TouchableOpacity onPress={() => setVisible(true)}>*/}
      {/*  <Text style={style}>{moment(date).format('DD/MM/YYYY')}</Text>*/}
      {/*</TouchableOpacity>*/}
      <Dialog
        visible={visible}
        onTouchOutside={setVisible}
        footer={
          <DialogFooter>
            <DialogButton
              text="Huỷ"
              onPress={() => {
                setVisible(false);
              }}
            />
            <DialogButton text="Chọn" onPress={acceptHandler} />
          </DialogFooter>
        }
        onHardwareBackPress={() => {
          setVisible(false);
          return true;
        }}>
        <DialogContent>
          <DatePicker
            date={dateState}
            onDateChange={date => setDateState(date)}
            mode="date"
            locale="vi"
          />
        </DialogContent>
      </Dialog>
    </View>
  );
}
