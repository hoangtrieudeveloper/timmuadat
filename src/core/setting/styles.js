import {DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';
import SanFrancisco from '../../components/SanFrancisco';
import Const from '../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  //
  vecterNext: {
    marginTop: 8,
    marginRight: 16,
  },
  blockText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12,
    paddingBottom: 12,
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
  },
  textName: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK_ROOT,
  },
  blockAll: {
    paddingTop: 16,
  },
  blockWrapper: {
    paddingLeft: 16,
  },
};
