import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    marginBottom: 16,
  },
  //issetInfo
  blockTitleQLTK: {
    paddingTop: 13,
    paddingBottom: 13,
  },
  titleQLTK: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 22,
    color: Const.GREEN,
    textAlign: 'right',
  },

  itemQlTKTP: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
    paddingBottom: 12,
    marginTop: 28,
  },
  itemQlTKTPLast: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
    paddingBottom: 12,
  },
  itemQlTKTPShoWNoti: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    paddingBottom: 12,
  },
  textLeftInfo: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK_ROOT,
  },
  //
  vecterNext: {
    marginTop: 8,
  },
  itemQlTK: {
    paddingBottom: 10,
    flexShrink: 1,
  },
  textLeftIcon: {
    fontSize: 17,
    lineHeight: 22,
    ...SanFrancisco.regular,
    color: Const.BLUE,
    marginRight: 11,
  },
  blockAllIcon: {
    flexDirection: 'row',
  },
  ErrorItem: {
    textAlign: 'right',
    fontSize: 14,
    lineHeight: 16,
    ...SanFrancisco.regular,
    color: Const.RED,
    marginTop: 10,
  },
  mrt16: {
    marginTop: 16,
  },
};
