import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    marginBottom: 16,
  },
  inputLogin: {
    height: 60,
    fontSize: 17,
    lineHeight: 20,
    ...SanFrancisco.regular,
    color: '#000000',
    textAlign: 'left',
  },
  inputeye: {
    position: 'absolute',
    right: 0,
    top: 25,
  },
  mrb16: {
    marginBottom: 16,
  },
};
