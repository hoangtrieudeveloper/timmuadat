import {DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#1C232D',
  },
};
