import {DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';
import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  blockPost: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EAF4FE',
    flex: 1,
  },
  blockPostBottom: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DEEEFD',
    flex: 1,
  },
  textBBSN: {
    ...SanFrancisco.regular,
    fontSize: 14,
    lineHeight: 20,
    color: Const.BLACK,
    marginTop: 16,
    textTransform: 'uppercase',
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
    textTransform: 'capitalize',
  },
  buttonLogin: {
    borderRadius: 25,
    height: 50,
    width: 152,
    borderWidth: 1,
    borderColor: Const.BLUE,
    marginTop: 16,
  },
};
