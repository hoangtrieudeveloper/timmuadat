import {StyleSheet} from 'react-native';

const styles = {
  boxTextPosition: {
    paddingTop: 10,
    borderTopWidth: 0.59,
    borderColor: '#dadada',
    paddingBottom: 10,
    flexDirection:'row',
    justifyContent: 'space-between',
  },
  textPosition: {
    fontWeight: '500',
    fontSize: 14,
    color: '#FFF',
  },
  boxTextPositionActive: {
    color: '#3CD683',
  },
};

export default styles;
