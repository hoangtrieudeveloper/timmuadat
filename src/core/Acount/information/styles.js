import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLUE,
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    marginBottom: 13,
  },
  //issetInfo
  blockTitle: {
    marginTop: 11,
  },
  infoUser: {
    flexDirection: 'row',
    marginBottom: 14,
  },
  username_info: {
    ...SanFrancisco.semiBold,
    fontSize: 19,
    lineHeight: 23,
    color: Const.BLACK,
  },
  level_info: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.ORANGE,
  },
  value_money: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.GRAY_v4,
    paddingTop: 4,
    paddingBottom: 4,
  },
  info_full: {
    marginLeft: 16,
  },
  blockTitleQLTK: {
    paddingLeft: 16,
    paddingTop: 13,
    paddingBottom: 12,
    borderTopWidth: 1,
    borderTopColor: '#E6E6E6',
  },
  titleQLTK: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 20,
    color: Const.BLACK,
    textTransform: 'uppercase',
  },
  itemQlTK: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    paddingTop: 12,
    paddingBottom: 12,
    flexShrink: 1,
  },
  textLeftInfo: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK_ROOT,
    width: '50%',
  },
  textRightInfo: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK,
    flexShrink: 1,
  },
  textRightInfoNodata: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.GRAY_v5,
    flexShrink: 1,
  },
  mrt13: {
    marginTop: 13,
  },
};
