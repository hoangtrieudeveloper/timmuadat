import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  textLeftInfo: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 24,
    color: Const.BLACK,
  },
  /**/
  blockText: {
    marginBottom: 16,
    marginTop: 16,
  },
  textItemBLock: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 20,
    color: Const.BLACK,
  },
  listItemHistory: {
    borderWidth: 1,
    // borderColor: Const.BLUE,
    borderRadius: 8,
    padding: 12,
    marginBottom: 16,
    flexDirection: 'row',
  },
  textAcount: {
    ...SanFrancisco.regular,
    fontSize: 13,
    lineHeight: 17,
    color: Const.GRAY_v4,
    marginTop: 8,
    flexShrink: 1,
  },
  blockInfo: {
    marginLeft: 22,
    marginRight: 12,
    flexShrink: 1,
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
    textTransform: 'capitalize',
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    marginBottom: 16,
  },
};
