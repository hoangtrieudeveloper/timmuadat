import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  iconFind: {
    backgroundColor: '#EEEEF0',
    paddingTop: 13,
    paddingBottom: 13,
    paddingLeft: 12,
    paddingRight: 12,
    borderRadius: 10,
    marginLeft: 12,
    width: 44,
    height: 44,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTotal: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 20,
    color: Const.GRAY_v4,
    marginTop: 18,
  },
  imageItem: {
    width: 74,
    height: 74,
    borderRadius: 8,
    marginRight: 16,
    marginTop: 4,
  },
  blockItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
    paddingBottom: 12,
    marginTop: 16,
    marginRight: 16,
    marginLeft: 16,
  },
  rightItem: {
    flexShrink: 1,
  },
  titleRightItem: {
    ...SanFrancisco.semiBold,
    fontSize: 15,
    lineHeight: 21,
    color: Const.BLACK,
    flexShrink: 1,
    marginRight: 17,
  },
  PriceInfo: {
    ...SanFrancisco.regular,
    fontSize: 14,
    lineHeight: 21,
    color: Const.BLACK,
    marginTop: 8,
    marginBottom: 22,
  },
  timeRightItem: {
    ...SanFrancisco.regular,
    fontSize: 14,
    lineHeight: 16,
    color: Const.BLACK,
  },
  textTrash: {
    marginTop: 5,
    ...SanFrancisco.semiBold,
    fontSize: 16,
    lineHeight: 16,
    color: Const.BLACK,
    marginLeft: 10,
  },
  titleImage: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageRightTitle: {
    marginTop: 14,
  },
  StatusSwipeableLeft: {
    width: '21%',
    backgroundColor: '#C4C4C4',
    minHeight: '100%',
    paddingRight: 5,
    paddingLeft: 5,
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
  StatusSwipeableRight: {
    width: '21%',
    backgroundColor: '#EC464D',
    minHeight: '100%',
    paddingRight: 5,
    paddingLeft: 5,
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
  textStatus: {
    fontSize: 15,
    ...SanFrancisco.medium,
    color: '#FFFFFF',
    lineHeight: 20,
    paddingTop: 11,
    textAlign: 'center',
  },
  blockImageInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  blockTimeTrash: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  blockTrash: {
    flexDirection: 'row',
  },
};
