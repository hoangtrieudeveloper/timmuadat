import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  wrapHeader: {
    backgroundColor: '#F9F9F9F0',
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  titleHeaderCenter: {
    justifyContent: 'center',
    height: 44,
  },
  blockContentHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleHeader: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 20,
    color: '#000000',
  },
  blockContent: {
    marginTop: 16,
    flexShrink: 1,
  },
  timeToFrom: {
    ...SanFrancisco.regular,
    fontSize: 13,
    lineHeight: 15,
    color: Const.GRAY_v4,
    marginBottom: 12,
  },
  titleSort: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 24,
    color: Const.BLACK,
    marginBottom: 16,
  },
  titleContent: {
    ...SanFrancisco.semiBold,
    fontSize: 16,
    lineHeight: 24,
    color: Const.BLACK,
    marginBottom: 16,
    flexShrink: 1,
  },
  textContent: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 24,
    color: Const.BLACK,
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
    textTransform: 'capitalize',
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    marginBottom: 16,
  },
  mrt16: {
    marginTop: 16,
  },
};
