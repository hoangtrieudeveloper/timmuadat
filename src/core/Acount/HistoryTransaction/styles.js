import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  textLeftInfo: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 24,
    color: Const.GRAY_v4,
  },
  titleHeaderCenter: {
    justifyContent: 'center',
    height: 44,
  },
  wrapHeader: {
    backgroundColor: '#F9F9F9F0',
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  blockContentHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleHeader: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 20,
    color: '#000000',
  },
  /**/
  blockText: {
    marginBottom: 16,
    marginTop: 16,
  },
  textItemBLock: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 20,
    color: Const.BLACK,
    flexShrink: 1,
  },
  listItemHistory: {
    borderWidth: 1,
    borderColor: Const.GRAY_v6,
    borderRadius: 8,
    padding: 16,
    marginBottom: 16,
  },
  textAcount: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.BLACK,
  },
  textAcountCxl: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.GRAY_v4,
  },
  textAcountht: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.GREEN,
  },
  blockAcount: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 12,
  },
  blockAcountPrice: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};
