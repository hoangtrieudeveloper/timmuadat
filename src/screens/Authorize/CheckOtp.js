import React, {useEffect, useState} from 'react';
import {Image, SafeAreaView, TouchableOpacity, View} from 'react-native';
import Button from '../../components/Button';
import {styles} from '../../core/Authorize/checkotp/styles';
import Header from '../../components/Header';
import Selection from '../../components/Selection';
import {Container} from 'native-base';
import ModalProcess from '../../components/ListModal/ModalProcess';
import {Users} from '../../models/users';
import OtpInputs from 'react-native-otp-inputs';
import {Text} from 'react-native-paper';

export default function CheckOtp({route, navigation}) {
  const {object} = route?.params;
  const [otp, setOtp] = useState('');
  const [username, setUsername] = useState(object || '');
  const [contentModal, setContentModal] = useState('');
  const [titleModal, setTitleModal] = useState('');
  const [modalProcess, setModalProcess] = useState(false);
  const setCloseModal = item => {
    setModalProcess(false);
  };
  const onSubmit = () => {
    if (otp === '') {
      setModalProcess(true);
      setTitleModal('Thông báo');
      setContentModal('Vui lòng nhập mã OTP');
    } else {
      Users.checkOtpForgot(username, otp)
        .then(data => {
          if (data.status == 1) {
            navigation.navigate('NewPassword', {
              object: {username: username, otp: otp},
            });
          } else {
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          }
        })
        .catch(e => {
          navigation.navigate('Dashboard', {object: {}});
        });
    }
  };
  useEffect(() => {
    setUsername(object);
  }, [object]);
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Quên mật khẩu" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ModalProcess
          visible={modalProcess}
          title={titleModal}
          content={contentModal}
          typeButton={1}
          onChange={item => setCloseModal(item)}
        />
        <Selection size={16}>
          <View style={styles.blockCdtime}>
            <Text style={styles.textCoundown}>
              Nhập mã số xác thực đã được gửi SMS đến số điện thoại của bạn.
            </Text>
            <Text style={styles.textCoundown}>
              <Text style={{fontWeight: '700'}}>Lưu ý:</Text> Mã OTP chỉ có hiệu
              lực trong 15 phút!
            </Text>
          </View>
          {/*<Text style={styles.timeCdown}>15:00</Text>*/}
          <View style={styles.blockOTP}>
            <OtpInputs
              inputContainerStyles={styles.containerStyle}
              inputStyles={styles.inputStyle}
              handleChange={code => setOtp(code)}
              numberOfInputs={6}
            />
          </View>
          <View style={styles.mt16}>
            <Button
              titleStyle={styles.buttonTitle}
              style={styles.buttonLogin}
              type="blue"
              title="Tiếp tục"
              mode="contained"
              onPress={() => {
                onSubmit();
              }}
            />
          </View>
        </Selection>
      </SafeAreaView>
    </Container>
  );
}
