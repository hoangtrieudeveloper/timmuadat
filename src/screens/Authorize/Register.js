import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Alert,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../core/Authorize/register/styles';
import {emailValidator} from '../../helpers/emailValidator';
import {passwordValidator} from '../../helpers/passwordValidator';
import {Users} from '../../models/users';
import {nameValidator} from '../../helpers/nameValidator';
import {Container} from 'native-base';
import Header from '../../components/Header';
import Selection from '../../components/Selection';
import TextInput from '../../components/TextInput';
import Button from '../../components/Button';
import Option from '../../components/Option';
import {fullnameValidator} from '../../helpers/fullname';
import {phoneValidator} from '../../helpers/phone';
import ModalProcess from '../../components/ListModal/ModalProcess';

export default function Register({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [username, setUsername] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [re_password, setRe_password] = useState({value: '', error: ''});
  const [fullname, setFullname] = useState({value: '', error: ''});
  const [phone, setPhone] = useState({value: '', error: ''});
  const [type, setType] = useState(1);
  const [checkShowPassword, setCheckShowPassword] = useState(true);
  const [checkShowRe_Password, setCheckShowRe_Password] = useState(true);
  //modal
  const [modalProcess, setModalProcess] = useState(false);
  const [titleModal, setTitleModal] = useState('');
  const [contentModal, setContentModal] = useState('');
  const setCloseModal = item => {
    setModalProcess(false);
  };

  const onRegisterPressed = () => {
    const usernameError = nameValidator(username.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    const re_passwordError = passwordValidator(re_password.value);
    const fullnameError = fullnameValidator(fullname.value);
    const phoneError = phoneValidator(phone.value);
    setUsername({...username, error: usernameError});
    setEmail({...email, error: emailError});
    setPassword({...password, error: passwordError});
    setRe_password({...re_password, error: re_passwordError});
    setFullname({...fullname, error: fullnameError});
    setPhone({...phone, error: phoneError});
    if (
      !passwordError &&
      !usernameError &&
      !emailError &&
      !re_passwordError &&
      !fullnameError &&
      !phoneError
    ) {
      Users.register(
        type,
        username.value,
        email.value,
        password.value,
        re_password.value,
        fullname.value,
        phone.value,
      )
        .then(data => {
          if (data.status == 1) {
            setCheckShowPassword(true);
            setCheckShowRe_Password(true);
            setUsername({value: '', error: ''});
            setEmail({value: '', error: ''});
            setPassword({value: '', error: ''});
            setRe_password({value: '', error: ''});
            setFullname({value: '', error: ''});
            setPhone({value: '', error: ''});
            setType(1);
            setModalProcess(true);
            setTitleModal('Tạo tài khoản thành công!');
            setContentModal(data.message);
          } else {
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          }
        })
        .catch(e => {
          navigation.navigate('Dashboard', {object: {}});
        });
    }
  };
  return (
    <Container style={{flex: 1}}>
      <ModalProcess
        visible={modalProcess}
        title={titleModal}
        content={contentModal}
        onChange={item => setCloseModal(item)}
      />
      <SafeAreaView style={styles.container}>
        <Header keyHeader="Đăng ký tài khoản" navigation={navigation} />
        <ScrollView>
          <Selection size={16}>
            <View
              style={{flexDirection: 'row', marginTop: 26, marginBottom: 16}}>
              <Option
                label={'Cá nhân'}
                value={1}
                checked={type}
                onChange={() => {
                  setType(1);
                }}
                style={{marginRight: 27}}
              />
              <Option
                label={'Công ty'}
                value={2}
                checked={type}
                onChange={() => {
                  setType(2);
                }}
              />
            </View>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                  },
                ]}
                placeholder="Tên đăng nhập*"
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={username.value}
                onChangeText={text => setUsername({value: text, error: ''})}
                error={!!username.error}
                errorText={username.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                marginVerNone={5}
              />
            </View>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                  },
                ]}
                placeholder="Email*"
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={email.value}
                onChangeText={text => setEmail({value: text, error: ''})}
                error={!!email.error}
                errorText={email.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                marginVerNone={5}
              />
            </View>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                    paddingRight: 20,
                  },
                ]}
                placeholder="Mật khẩu*"
                placeholderTextColor="#999999"
                returnKeyType="done"
                value={password.value}
                onChangeText={text => setPassword({value: text, error: ''})}
                error={!!password.error}
                errorText={password.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                secureTextEntry={checkShowPassword}
                marginVerNone={5}
              />
              <TouchableOpacity
                onPress={() => {
                  setCheckShowPassword(!checkShowPassword);
                }}
                style={styles.inputeye}>
                <Image
                  source={
                    checkShowPassword
                      ? require('../../assets/ic_authorize/icon_login/bx-show.png')
                      : require('../../assets/ic_authorize/ic_register/bx-hide.png')
                  }
                />
              </TouchableOpacity>
            </View>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                    paddingRight: 20,
                  },
                ]}
                placeholder="Nhập lại mật khẩu*"
                placeholderTextColor="#999999"
                returnKeyType="done"
                value={re_password.value}
                onChangeText={text => setRe_password({value: text, error: ''})}
                error={!!re_password.error}
                errorText={re_password.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                secureTextEntry={checkShowRe_Password}
                marginVerNone={5}
              />
              <TouchableOpacity
                onPress={() => {
                  setCheckShowRe_Password(!checkShowRe_Password);
                }}
                style={styles.inputeye}>
                <Image
                  source={
                    checkShowRe_Password
                      ? require('../../assets/ic_authorize/icon_login/bx-show.png')
                      : require('../../assets/ic_authorize/ic_register/bx-hide.png')
                  }
                />
              </TouchableOpacity>
            </View>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                  },
                ]}
                placeholder="Họ và tên*"
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={fullname.value}
                onChangeText={text => setFullname({value: text, error: ''})}
                error={!!fullname.error}
                errorText={fullname.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                marginVerNone={5}
              />
            </View>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                  },
                ]}
                placeholder="Số điện thoại*"
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={phone.value}
                onChangeText={text => setPhone({value: text, error: ''})}
                error={!!phone.error}
                errorText={phone.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                marginVerNone={4}
                keyboardType="numeric"
              />
            </View>
            <View style={styles.mrt16}>
              <Button
                titleStyle={styles.buttonTitle}
                style={styles.buttonLogin}
                type="blue"
                title="Đăng ký"
                mode="contained"
                onPress={() => onRegisterPressed()}
              />
            </View>
          </Selection>
        </ScrollView>
      </SafeAreaView>
    </Container>
  );
}
