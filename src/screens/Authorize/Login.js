import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Alert,
  AsyncStorage,
} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import {styles} from '../../core/Authorize/login/styles';
import {emailValidator} from '../../helpers/emailValidator';
import {passwordValidator} from '../../helpers/passwordValidator';
import {Users} from '../../models/users';
import {MainBottomTabComponent} from '../../components/BottomTab';
import Header from '../../components/Header';
import Selection from '../../components/Selection';
import {Container} from 'native-base';
import {nameValidator} from '../../helpers/nameValidator';
import ModalProcess from '../../components/ListModal/ModalProcess';

export default function Login({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [checkShowPw, setCheckShowPw] = useState(true);
  const [loading, setLoading] = useState(false);
  //modal
  const [modalProcess, setModalProcess] = useState(false);
  const [titleModal, setTitleModal] = useState('');
  const [contentModal, setContentModal] = useState('');
  const setCloseModal = item => {
    setModalProcess(false);
  };
  const onLoginPressed = () => {
    const emailError = nameValidator(email.value);
    const passwordError = passwordValidator(password.value);
    setEmail({...email, error: emailError});
    setPassword({...password, error: passwordError});
    if (!emailError && !passwordError) {
      setLoading(true);
      Users.login(email.value, password.value)
        .then(data => {
          setLoading(false);
          if (data.status == 1) {
            AsyncStorage.setItem('user_token', data.optional.token);
            global.token = data.optional.token;
            global.userinfo = data.data;
            navigation.navigate('ManagerInformation');
          } else {
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          }
        })
        .catch(e => {
          navigation.navigate('Dashboard', {object: {}});
        });
    }
  };

  return (
    <Container style={{flex: 1}}>
      <ModalProcess
        visible={modalProcess}
        title={titleModal}
        content={contentModal}
        onChange={item => setCloseModal(item)}
      />
      <SafeAreaView style={styles.container}>
        <View>
          <Header
            type={'Noback'}
            keyHeader="Đăng nhập"
            navigation={navigation}
          />
          <Selection size={16}>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                  },
                ]}
                placeholder="Tên đăng nhập/Email"
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={email.value}
                onChangeText={text => setEmail({value: text, error: ''})}
                error={!!email.error}
                errorText={email.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
                marginVerNone={16}
              />
            </View>
            <View style={styles.mrb16}>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                    paddingRight: 20,
                  },
                ]}
                placeholder="Mật khẩu"
                placeholderTextColor="#999999"
                returnKeyType="done"
                value={password.value}
                onChangeText={text => setPassword({value: text, error: ''})}
                error={!!password.error}
                errorText={password.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                secureTextEntry={checkShowPw}
                marginVerNone={4}
              />
              <TouchableOpacity
                onPress={() => {
                  setCheckShowPw(!checkShowPw);
                }}
                style={styles.inputeye}>
                <Image
                  source={require('../../assets/ic_authorize/icon_login/bx-show.png')}
                />
              </TouchableOpacity>
            </View>
            <Button
              titleStyle={styles.buttonTitle}
              style={styles.buttonLogin}
              type="blue"
              title="Đăng nhập"
              loading={loading}
              mode="contained"
              disabled={loading}
              onPress={() => onLoginPressed()}
            />
            <View style={styles.blockNagation}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('ForgotPassword');
                }}>
                <Text style={styles.textAuthen}>Quên mật khẩu?</Text>
              </TouchableOpacity>
              <Text style={styles.createAcount}>Bạn chưa có tài khoản? </Text>
            </View>
            <View style={styles.IconMxh}>
              <Image
                source={require('../../assets/ic_authorize/icon_login/Group2134.png')}
              />
              <Image
                source={require('../../assets/ic_authorize/icon_login/Group2133.png')}
                style={styles.pdlr32}
              />
              <Image
                source={require('../../assets/ic_authorize/icon_login/Group2132.png')}
              />
            </View>
            <View style={styles.blockTextHow}>
              <Text style={styles.textHow}>Bạn chưa là thành viên? </Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Register');
                }}>
                <Text style={styles.textRegister}>Đăng ký tài khoản</Text>
              </TouchableOpacity>
            </View>
          </Selection>
        </View>
      </SafeAreaView>
      <MainBottomTabComponent navigation={navigation} activeTab="login" />
    </Container>
  );
}
