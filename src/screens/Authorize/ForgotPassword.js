import React, {useState} from 'react';
import {Image, SafeAreaView, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../components/Button';
import {styles} from '../../core/Authorize/forgotpassword/styles';
import Header from '../../components/Header';
import Selection from '../../components/Selection';
import {Container} from 'native-base';
import TextInput from '../../components/TextInput';
import ModalProcess from '../../components/ListModal/ModalProcess';
import {Users} from '../../models/users';
import {usernameValidator} from '../../helpers/usernameValidator';

export default function ForgotPassword({navigation}) {
  const [username, setUsername] = useState('');
  const [contentModal, setContentModal] = useState('');
  const [titleModal, setTitleModal] = useState('');
  const [modalProcess, setModalProcess] = useState(false);
  const setCloseModal = item => {
    setModalProcess(false);
  };
  const onSubmit = () => {
    const usernameError = usernameValidator(username.value);

    setUsername({...username, error: usernameError});
    if (!usernameError) {
      Users.forgotPassword(username.value)
        .then(data => {
          if (data.status == 1) {
            navigation.navigate('CheckOtp', {object: username.value});
          } else {
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          }
        })
        .catch(e => {
          navigation.navigate('Dashboard', {object: {}});
        });
    }
  };
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Quên mật khẩu" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ModalProcess
          visible={modalProcess}
          title={titleModal}
          content={contentModal}
          typeButton={1}
          onChange={item => setCloseModal(item)}
        />
        <Selection size={16}>
          <View>
            <TextInput
              style={styles.inputLogin}
              placeholder="Email/Số điện thoại"
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={username.value}
              onChangeText={text => setUsername({value: text, error: ''})}
              error={!!username.error}
              errorText={username.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
          </View>
          <View style={styles.mt16}>
            <Button
              titleStyle={styles.buttonTitle}
              style={styles.buttonLogin}
              type="blue"
              title="Tiếp theo"
              mode="contained"
              onPress={() => {
                onSubmit();
              }}
            />
          </View>
        </Selection>
      </SafeAreaView>
    </Container>
  );
}
