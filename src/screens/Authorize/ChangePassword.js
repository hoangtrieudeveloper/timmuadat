import React, {useState} from 'react';
import {Image, SafeAreaView, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../components/Button';
import {styles} from '../../core/Authorize/changepassword/styles';
import Header from '../../components/Header';
import Selection from '../../components/Selection';
import {Container} from 'native-base';
import TextInput from '../../components/TextInput';
import ModalProcess from '../../components/ListModal/ModalProcess';
import {passwordValidator} from '../../helpers/passwordValidator';
import {rePasswordValidator} from '../../helpers/rePasswordValidator';
import {Users} from '../../models/users';
import {confirmpasswordValidator} from '../../helpers/confirmpasswordValidator';

export default function ChangePassword({navigation}) {
  const [loading, setLoading] = useState(false);
  const [modalProcess, setModalProcess] = useState(false);
  const [contentModal, setContentModal] = useState('');
  const [titleModal, setTitleModal] = useState('');
  const setCloseModal = item => {
    setModalProcess(false);
  };
  const [password, setPassword] = useState({value: '', error: ''});
  const [rePassword, setRePassword] = useState({value: '', error: ''});
  const [confirmPassword, setConfirmPassword] = useState({
    value: '',
    error: '',
  });
  const [checkShowPassword, setCheckShowPassword] = useState(true);
  const [checkShowRe_Password, setCheckShowRe_Password] = useState(true);
  const [checkShowConfirm_Password, setCheckShowConfirm_Password] = useState(
    true,
  );
  const onSubmit = () => {
    const passwordError = passwordValidator(password.value);
    setPassword({...password, error: passwordError});
    const rePasswordError = rePasswordValidator(rePassword.value);
    setRePassword({...rePassword, error: rePasswordError});
    const confirmPasswordError = confirmpasswordValidator(
      confirmPassword.value,
    );
    setConfirmPassword({...confirmPassword, error: confirmPasswordError});
    if (!passwordError && !rePasswordError && !confirmPasswordError) {
      setLoading(true);
      Users.changePassword(
        password.value,
        rePassword.value,
        confirmPassword.value,
      )
        .then(data => {
          setLoading(false);
          if (data.status == 1) {
            setCheckShowPassword(true);
            setCheckShowRe_Password(true);
            setCheckShowConfirm_Password(true);
            setPassword({value: '', error: ''});
            setRePassword({value: '', error: ''});
            setConfirmPassword({
              value: '',
              error: '',
            });
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          } else {
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          }
        })
        .catch(e => {
          navigation.navigate('Dashboard', {object: {}});
        });
    }
  };
  return (
    <Container style={{flex: 1}}>
      <ModalProcess
        visible={modalProcess}
        title={titleModal}
        content={contentModal}
        typeButton={1}
        onChange={item => setCloseModal(item)}
      />
      <Header keyHeader="Đổi mật khẩu" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <Selection size={16}>
          <View>
            <TextInput
              style={styles.inputLogin}
              placeholder="Mật khẩu cũ*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={password.value}
              onChangeText={text => setPassword({value: text, error: ''})}
              error={!!password.error}
              errorText={password.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
              secureTextEntry={checkShowPassword}
            />
            <TouchableOpacity
              onPress={() => {
                setCheckShowPassword(!checkShowPassword);
              }}
              style={styles.inputeye}>
              <Image
                source={require('../../assets/ic_authorize/icon_login/bx-show.png')}
              />
            </TouchableOpacity>
          </View>
          <View>
            <TextInput
              style={styles.inputLogin}
              placeholder="Mật khẩu mới*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={rePassword.value}
              onChangeText={text => setRePassword({value: text, error: ''})}
              error={!!rePassword.error}
              errorText={rePassword.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
              secureTextEntry={checkShowRe_Password}
            />
            <TouchableOpacity
              onPress={() => {
                setCheckShowRe_Password(!checkShowRe_Password);
              }}
              style={styles.inputeye}>
              <Image
                source={require('../../assets/ic_authorize/icon_login/bx-show.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.mrb16}>
            <TextInput
              style={styles.inputLogin}
              placeholder="Nhập lại mật khẩu mới*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={confirmPassword.value}
              onChangeText={text =>
                setConfirmPassword({value: text, error: ''})
              }
              error={!!confirmPassword.error}
              errorText={confirmPassword.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
              secureTextEntry={checkShowConfirm_Password}
            />
            <TouchableOpacity
              onPress={() => {
                setCheckShowConfirm_Password(!checkShowConfirm_Password);
              }}
              style={styles.inputeye}>
              <Image
                source={require('../../assets/ic_authorize/icon_login/bx-show.png')}
              />
            </TouchableOpacity>
          </View>
          <Button
            titleStyle={styles.buttonTitle}
            style={styles.buttonLogin}
            type="blue"
            title="Lưu"
            mode="contained"
            loading={loading}
            disabled={loading}
            onPress={() => {
              onSubmit();
            }}
          />
        </Selection>
      </SafeAreaView>
    </Container>
  );
}
