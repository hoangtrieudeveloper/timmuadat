import React, {useEffect, useState} from 'react';
import {Image, SafeAreaView, TouchableOpacity, View} from 'react-native';
import Button from '../../components/Button';
import {styles} from '../../core/Authorize/newpassword/styles';
import Header from '../../components/Header';
import Selection from '../../components/Selection';
import {Container} from 'native-base';
import TextInput from '../../components/TextInput';
import ModalProcess from '../../components/ListModal/ModalProcess';
import {Users} from '../../models/users';
import {passwordValidator} from '../../helpers/passwordValidator';
import {rePasswordValidator} from '../../helpers/rePasswordValidator';

export default function NewPassword({route, navigation}) {
  const {object} = route?.params;
  const [objectUser, setObjectUser] = useState('');
  const [password, setPassword] = useState({value: '', error: ''});
  const [rePassword, setRePassword] = useState({value: '', error: ''});
  const [contentModal, setContentModal] = useState('');
  const [titleModal, setTitleModal] = useState('');
  const [modalProcess, setModalProcess] = useState(false);
  const [checkShowPassword, setCheckShowPassword] = useState(true);
  const [checkShowRe_Password, setCheckShowRe_Password] = useState(true);
  const setCloseModal = item => {
    setModalProcess(false);
  };
  const onSubmit = () => {
    const passwordError = passwordValidator(password.value);
    setPassword({...password, error: passwordError});
    const rePasswordError = rePasswordValidator(rePassword.value);
    setRePassword({...rePassword, error: rePasswordError});
    if (!passwordError && !rePasswordError) {
      Users.resetPassword(
        objectUser.username,
        password.value,
        rePassword.value,
        objectUser.otp,
      )
        .then(data => {
          if (data.status == 1) {
            setCheckShowPassword(true);
            setCheckShowRe_Password(true);
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
            setTimeout(() => {
              setModalProcess(false);
              navigation.navigate('Login');
            }, 1000);
          } else {
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          }
        })
        .catch(e => {
          navigation.navigate('Dashboard', {object: {}});
        });
    }
  };
  useEffect(() => {
    setObjectUser(object);
  }, [object]);
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Đổi mật khẩu" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ModalProcess
          visible={modalProcess}
          title={titleModal}
          content={contentModal}
          typeButton={1}
          onChange={item => setCloseModal(item)}
        />
        <Selection size={16}>
          <View>
            <TextInput
              style={styles.inputLogin}
              placeholder="Mật khẩu mới*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={password.value}
              onChangeText={text => setPassword({value: text, error: ''})}
              error={!!password.error}
              errorText={password.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
              secureTextEntry={checkShowPassword}
            />
            <TouchableOpacity
              onPress={() => {
                setCheckShowPassword(!checkShowPassword);
              }}
              style={styles.inputeye}>
              <Image
                source={require('../../assets/ic_authorize/icon_login/bx-show.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.mrb16}>
            <TextInput
              style={styles.inputLogin}
              placeholder="Nhập lại mật khẩu mới*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={rePassword.value}
              onChangeText={text => setRePassword({value: text, error: ''})}
              error={!!rePassword.error}
              errorText={rePassword.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
              secureTextEntry={checkShowRe_Password}
            />
            <TouchableOpacity
              onPress={() => {
                setCheckShowRe_Password(!checkShowRe_Password);
              }}
              style={styles.inputeye}>
              <Image
                source={require('../../assets/ic_authorize/icon_login/bx-show.png')}
              />
            </TouchableOpacity>
          </View>
          <Button
            titleStyle={styles.buttonTitle}
            style={styles.buttonLogin}
            type="blue"
            title="Đổi mật khẩu"
            mode="contained"
            onPress={() => {
              onSubmit();
            }}
          />
        </Selection>
      </SafeAreaView>
    </Container>
  );
}
