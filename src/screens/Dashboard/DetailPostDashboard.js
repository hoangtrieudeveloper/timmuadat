import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  FlatList,
} from 'react-native';
import {styles} from '../../core/dashboard/Detailpost/styles';
import {Container} from 'native-base';
import Selection from '../../components/Selection';
import {SliderBox} from 'react-native-image-slider-box';
import Header from '../../components/Header';
const {width, height} = Dimensions.get('screen');
import {useFocusEffect} from '@react-navigation/native';
import {ProductModel} from '../../models/product';
import {RenderHTML} from 'react-native-render-html';
import LoadingSkeleton from '../../components/LoadingSkeleton';
export default function DetailPostDashboard({route, navigation}) {
  const {infor} = route?.params;
  const [loading, setLoading] = useState(true);
  const [loadingRelease, setLoadingRelease] = useState(true);
  const [inforPost, setInforPost] = useState(infor || '');
  const [detailData, setDetailData] = useState('');
  const [checkShowHidden, setCheckShowHidden] = useState(8);
  const [listRelease, setListRelease] = useState([]);
  const [indexTotal, setIndexTotal] = useState(1);
  const [slider, setSlider] = useState([]);
  const getRelease = () => {
    ProductModel.listRelease(inforPost?.id)
      .then(data => {
        if (data.status == 1) {
          setListRelease(data?.data);
          setLoadingRelease(false);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  const getDetail = () => {
    ProductModel.detailProduct(inforPost?.id)
      .then(data => {
        if (data.status == 1) {
          setDetailData(data.data);
          setSlider(
            data?.data?.list_image && data?.data?.list_image.length > 0
              ? data?.data?.list_image
              : [require('../../assets/dashboard/no_image.png')],
          );
          setLoading(false);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('DetailPostDashboard', {infor: item});
      }}
      style={styles.blockImage_header}>
      <View style={styles.blockItemNew}>
        {/*{item?.is_hot == 0 ? (*/}
        {/*  <Image*/}
        {/*    source={require('../../assets/dashboard/Path616.png')}*/}
        {/*    style={styles.star5}*/}
        {/*  />*/}
        {/*) : (*/}
        {/*  <View />*/}
        {/*)}*/}
        <Text style={styles.titleNew}>
          {/*{item?.is_hot == 0 ? '      ' : null}*/}
          {item?.title}
        </Text>
        <View style={styles.blockItemW}>
          <View>
            {item?.image && item?.image === '' && item?.image === null ? (
              <Image
                source={{
                  uri: item?.image,
                }}
                style={styles.wblock}
              />
            ) : (
              <Image
                source={require('../../assets/dashboard/no_image.png')}
                style={styles.wblock}
              />
            )}
            <View style={styles.totalImage}>
              <Text style={styles.totalIm}>{item?.total_images || 0}</Text>
              <Image source={require('../../assets/dashboard/bx-image.png')} />
            </View>
          </View>
          <View style={styles.blockInfor}>
            <Text style={styles.price_m_w}>
              {item?.price} · {item?.area || 0} m² ·{' '}
              {item?.bedroom_numbers || 0} PN · {item?.toilet_numbers || 0} WC
            </Text>
            <Text style={[styles.price_m, {marginBottom: 12}]}>
              {item?.address}
            </Text>
            <Text style={styles.time_m}>{item?.start_date}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
  useFocusEffect(
    React.useCallback(() => {
      setLoading(true);
      setLoadingRelease(true);
      setInforPost(infor || '');
      getDetail();
      getRelease();
    }, [infor]),
  );
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Chi tiết tin đăng" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <LoadingSkeleton loading={loading} count={1}>
            <View style={styles.blockCount}>
              <Text style={styles.totalCount}>
                {slider?.length > 0 ? indexTotal : 0}/
                {slider?.length > 0 ? slider?.length : 0}
              </Text>
            </View>
            <SliderBox
              images={slider}
              resizeMethod={'resize'}
              resizeMode={'cover'}
              paginationBoxVerticalPadding={5}
              currentImageEmitter={index => {
                setTimeout(() => {
                  setIndexTotal(index + 1);
                }, 400);
              }}
              sliderBoxHeight={210}
            />
            <Selection size={16}>
              <Text style={styles.time}>{detailData.start_date}</Text>
              <Text style={styles.title}>{detailData?.title}</Text>
              <Text style={styles.price}>{detailData.price}</Text>
              <Text style={styles.price_dt}>{detailData?.address}</Text>
              {/*<TouchableOpacity*/}
              {/*  onPress={() => {*/}
              {/*    navigation.navigate('Maps');*/}
              {/*  }}*/}
              {/*  style={styles.blockMap}>*/}
              {/*  <Image*/}
              {/*    source={require('../../assets/dashboard/Post/Path1270.png')}*/}
              {/*  />*/}
              {/*  <Text style={styles.textMap}>Xem bản đồ</Text>*/}
              {/*</TouchableOpacity>*/}
              <View>
                <Text style={styles.textMT}>Mô tả</Text>
                <RenderHTML
                  contentWidth={width}
                  baseStyle={styles.contentMT}
                  source={{html: detailData?.description}}
                  defaultTextProps={
                    checkShowHidden !== null
                      ? {numberOfLines: checkShowHidden}
                      : {}
                  }
                />
                {detailData?.description !== '' ? (
                  <TouchableOpacity
                    onPress={() => {
                      setCheckShowHidden(checkShowHidden === null ? 8 : null);
                    }}>
                    <Text style={styles.ShowHidden}>
                      {checkShowHidden !== null ? 'Hiển thị thêm' : 'Thu gọn'}
                    </Text>
                  </TouchableOpacity>
                ) : null}
              </View>
              <Text style={styles.titleInfo}>Thông tin chi tiết</Text>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Loại tin rao</Text>
                <Text style={styles.newRight}>
                  {detailData?.categories_type}
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Loại bất động sản</Text>
                <Text style={styles.newRight}>
                  {detailData?.categoriesInfo?.name}
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Diện tích</Text>
                <Text style={styles.newRight}>{detailData?.area || 0} m2</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Địa chỉ</Text>
                <Text style={styles.newRight}>{detailData?.address}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Mức giá</Text>
                <Text style={styles.newRight}>{detailData?.price}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Phòng ngủ</Text>
                <Text style={styles.newRight}>
                  {detailData?.bedroom_numbers
                    ? detailData?.bedroom_numbers + ' PN'
                    : ''}
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Hướng nhà</Text>
                <Text style={styles.newRight}>
                  {detailData?.home_direction}
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Hướng ban công</Text>
                <Text style={styles.newRight}>
                  {detailData?.baconly_direction}
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Số tầng</Text>
                <Text style={styles.newRight}>{detailData?.floor_numbers}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Nội thất</Text>
                <Text style={styles.newRight}>{detailData?.furniture}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Pháp lý</Text>
                <Text style={styles.newRight}>{detailData?.legality}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Số toilet</Text>
                <Text style={styles.newRight}>
                  {detailData?.toilet_numbers}
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Mã tin đăng</Text>
                <Text style={styles.newRight}>{detailData?.id}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Loại tin đăng</Text>
                <Text style={styles.newRight}>{detailData.product_type}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Ngày đăng tin</Text>
                <Text style={styles.newRight}>{detailData?.start_date}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Ngày hết hạn</Text>
                <Text style={styles.newRight}>{detailData?.end_date}</Text>
              </View>
            </Selection>
            {/*<MapView*/}
            {/*  style={styles.ggMap}*/}
            {/*  initialRegion={{*/}
            {/*    latitude: 21.0277644,*/}
            {/*    longitude: 105.8341598,*/}
            {/*    latitudeDelta: 0.0922,*/}
            {/*    longitudeDelta: 0.0421,*/}
            {/*  }}*/}
            {/*  showsUserLocation*/}
            {/*  minZoomLevel={11}*/}
            {/*/>*/}
            <Selection size={16}>
              <View style={styles.blockLH}>
                <Text style={styles.titleLh}>Liên hệ người đăng tin</Text>
                <View style={styles.blockUserLH}>
                  {detailData?.userInfo?.avatar ? (
                    <Image
                      source={{
                        uri: detailData?.userInfo?.avatar,
                      }}
                      style={{width: 44, height: 44, borderRadius: 50}}
                    />
                  ) : (
                    <Image
                      source={require('../../assets/dashboard/Post/MaskGroup295.png')}
                    />
                  )}
                  <View style={styles.NamePhone}>
                    <Text style={styles.NameLh}>
                      {detailData?.userInfo?.name}
                    </Text>
                    <Text style={styles.PhoneLh}>
                      {detailData?.userInfo?.phone}
                    </Text>
                  </View>
                </View>
              </View>
            </Selection>
            <Selection size={16}>
              <View>
                <Text style={styles.titleKV}>Tin đăng cùng khu vực</Text>
                <LoadingSkeleton loading={loadingRelease} count={1}>
                  {listRelease.length > 0 ? (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('DetailPostDashboard', {
                          infor: listRelease.length > 0 ? listRelease[0] : {},
                        });
                      }}
                      style={styles.blockImage_header}>
                      <View>
                        {listRelease[0]?.image ? (
                          <Image
                            source={{
                              uri: listRelease[0]?.image,
                            }}
                            style={styles.w100}
                          />
                        ) : (
                          <Image
                            source={require('../../assets/dashboard/no_image.png')}
                            style={styles.w100}
                          />
                        )}
                        {listRelease[0]?.is_hot === 1 ? (
                          <View style={styles.hotnew}>
                            <Text style={styles.vluenew}>Nổi bật</Text>
                          </View>
                        ) : null}
                        <View style={styles.totalImage}>
                          <Text style={styles.totalIm}>
                            {listRelease[0]?.total_images || 0}
                          </Text>
                          <Image
                            source={require('../../assets/dashboard/bx-image.png')}
                          />
                        </View>
                        <View style={styles.totalImage_phone}>
                          <Image
                            source={require('../../assets/dashboard/bx-phone-call.png')}
                          />
                          <Text style={styles.totalIm_phone}>
                            {listRelease[0]?.userInfo?.phone}
                          </Text>
                        </View>
                      </View>
                      <View style={styles.blockItemNew}>
                        {/*<Image*/}
                        {/*  source={require('../../assets/dashboard/Path616.png')}*/}
                        {/*  style={styles.star5}*/}
                        {/*/>*/}
                        <Text style={styles.titleNew}>
                          {listRelease[0]?.title}
                        </Text>
                        <View style={styles.price_time}>
                          <Text style={styles.price_m}>
                            {listRelease[0]?.price} ·{' '}
                            {listRelease[0]?.area || 0} m²
                          </Text>
                          <Text style={styles.time_m}>
                            {listRelease[0]?.start_date}
                          </Text>
                        </View>
                        <Text style={styles.price_m}>
                          {listRelease[0]?.address}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  ) : null}
                  <FlatList
                    data={listRelease.length > 0 ? listRelease.slice(1) : []}
                    renderItem={renderItem}
                  />
                </LoadingSkeleton>
              </View>
            </Selection>
          </LoadingSkeleton>
        </ScrollView>
      </SafeAreaView>
      <TouchableOpacity style={styles.callIcon}>
        <Image source={require('../../assets/dashboard/Post/Path1272.png')} />
      </TouchableOpacity>
    </Container>
  );
}
