import React, {useState} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  View,
  TouchableOpacity,
} from 'react-native';
import {styles} from '../../../core/dashboard/TabRentalNews/styles';
import {Text} from 'react-native-paper';
import Selection from '../../../components/Selection';
import {useFocusEffect} from '@react-navigation/native';
import LoadingSkeleton from '../../../components/LoadingSkeleton';

export default function TabRentalNews({loadingTab, navigation, ...props}) {
  const [listData, setListData] = useState(props?.data || []);
  useFocusEffect(
    React.useCallback(() => {
      setListData(props.data || []);
    }, [props?.data]),
  );
  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('DetailPostDashboard', {infor: item});
      }}
      style={styles.blockImage_header}>
      <View style={styles.blockItemNew}>
        {/*{item?.is_hot == 0 ? (*/}
        {/*  <Image*/}
        {/*    source={require('../../../assets/dashboard/Path616.png')}*/}
        {/*    style={styles.star5}*/}
        {/*  />*/}
        {/*) : (*/}
        {/*  <View />*/}
        {/*)}*/}
        <Text style={styles.titleNew}>
          {/*{item?.is_hot == 0 ? '      ' : null}*/}
          {item?.title}
        </Text>
        <View style={styles.blockItemW}>
          <View>
            {item?.avatar || item?.avatar === '' || item?.avatar === null ? (
              <Image
                source={{
                  uri: item?.avatar,
                }}
                style={styles.wblock}
              />
            ) : (
              <Image
                source={require('../../../assets/dashboard/no_image.png')}
                style={styles.wblock}
              />
            )}
            <View style={styles.totalImage}>
              <Text style={styles.totalIm}>{item?.total_images || 0}</Text>
              <Image
                source={require('../../../assets/dashboard/bx-image.png')}
              />
            </View>
          </View>
          <View style={styles.blockInfor}>
            <Text style={styles.price_m_w}>
              {item?.price} · {item?.area || 0} m² ·{' '}
              {item?.bedroom_numbers || 0} PN · {item?.toilet_numbers || 0} WC
            </Text>
            <Text style={[styles.price_m, {marginBottom: 12}]}>
              {item?.address}
            </Text>
            <Text style={styles.time_m}>{item?.start_date}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
  return (
    <Selection size={16}>
      <LoadingSkeleton loading={loadingTab} count={1}>
        {listData.length > 0 ? (
          <ScrollView>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('DetailPostDashboard', {
                  infor: listData.length > 0 ? listData[0] : {},
                });
              }}
              style={styles.blockImage_header}>
              <View>
                {listData[0]?.avatar ? (
                  <Image
                    source={{
                      uri: listData[0]?.avatar,
                    }}
                    style={styles.w100}
                  />
                ) : (
                  <Image
                    source={require('../../../assets/dashboard/no_image.png')}
                    style={styles.w100}
                  />
                )}
                {listData[0]?.is_hot === 1 ? (
                  <View style={styles.hotnew}>
                    <Text style={styles.vluenew}>Nổi bật</Text>
                  </View>
                ) : null}
                <View style={styles.totalImage}>
                  <Text style={styles.totalIm}>
                    {listData[0]?.total_images || 0}
                  </Text>
                  <Image
                    source={require('../../../assets/dashboard/bx-image.png')}
                  />
                </View>
                <View style={styles.totalImage_phone}>
                  <Image
                    source={require('../../../assets/dashboard/bx-phone-call.png')}
                  />
                  <Text style={styles.totalIm_phone}>
                    {listData[0]?.seller_phone_number}
                  </Text>
                </View>
              </View>
              <View style={styles.blockItemNew}>
                {/*<Image*/}
                {/*  source={require('../../../assets/dashboard/Path616.png')}*/}
                {/*  style={styles.star5}*/}
                {/*/>*/}
                <Text style={styles.titleNew}>{listData[0]?.title}</Text>
                <View style={styles.price_time}>
                  <Text style={styles.price_m}>
                    {listData[0]?.price} · {listData[0]?.area || 0} m²
                  </Text>
                  <Text style={styles.time_m}>{listData[0]?.start_date}</Text>
                </View>
                <Text style={styles.price_m}>
                  {listData[0]?.seller_address}
                </Text>
              </View>
            </TouchableOpacity>
            <FlatList data={listData.slice(1)} renderItem={renderItem} />
          </ScrollView>
        ) : (
          <View />
        )}
      </LoadingSkeleton>
    </Selection>
  );
}
