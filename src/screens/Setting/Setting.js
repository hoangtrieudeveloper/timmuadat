import React, {useState} from 'react';
import {Image, SafeAreaView, View, TouchableOpacity} from 'react-native';
import {styles} from '../../core/setting/styles';
import {MainBottomTabComponent} from '../../components/BottomTab';
import {Body, Container, Text} from 'native-base';

import Header from '../../components/Header';

export default function Setting({navigation}) {
  return (
    <Container style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <View>
          <Header type={'Noback'} keyHeader="Thêm" navigation={navigation} />
          <View style={styles.blockWrapper}>
            <View style={styles.blockAll}>
              <TouchableOpacity style={styles.blockText}>
                <Text style={styles.textName}>Giới thiệu chung</Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.blockText}>
                <Text style={styles.textName}>Quy định đăng tin</Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.blockText}>
                <Text style={styles.textName}>Chính sách bảo mật</Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.blockText}>
                <Text style={styles.textName}>Giải quyết khiếu nại</Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.blockText}>
                <Text style={styles.textName}>Góp ý về ứng dụng</Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
      <MainBottomTabComponent navigation={navigation} activeTab="setting" />
    </Container>
  );
}
