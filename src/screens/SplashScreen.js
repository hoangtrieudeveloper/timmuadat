import React from 'react';
import {SafeAreaView, AsyncStorage} from 'react-native';
import Background from '../components/Background';
import Logo from '../components/Logo';
import {Users} from '../models/users';
import {theme} from '../core/theme';

export default function Splash({navigation}) {
  getUserInfo();

  function getUserInfo() {
    AsyncStorage.getItem('user_token').then(token => {
      if (!token) {
        token = '';
      }
      token = '' + token;
      Users.getUserInfoByToken(token)
        .then(data => {
          if (data.status == 1) {
            global.token = token;
            global.userinfo = data.data || {};
            navigation.navigate('Dashboard', {object: {}});
          } else {
            global.userinfo = {};
            global.token = null;
            navigation.navigate('Dashboard', {object: {}});
          }
        })
        .catch(e => {
          navigation.navigate('Dashboard', {object: {}});
        })
        .finally(() => {
          navigation.navigate('Dashboard', {object: {}});
        });
    });
  }

  return (
    <SafeAreaView style={theme.container}>
      <Background>
        <Logo />
      </Background>
    </SafeAreaView>
  );
}
