import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
} from 'react-native';

import Const from '../../../components/Const';
import Selection from '../../../components/Selection';
import TextInput from '../../../components/TextInput';
import {LocationModel} from '../../../models/location';
import SanFrancisco from '../../../components/SanFrancisco';
import Option from '../../../components/Option';
import ModalProcess from '../../../components/ListModal/ModalProcess';
import ImagePicker from 'react-native-image-picker';
import {ProductModel} from '../../../models/product';

const {width, height} = Dimensions.get('screen');

function CustomizePhotos({visible, onChange, data, header, ...props}) {
  //modal
  const [modalProcess, setModalProcess] = useState(false);
  const [titleModal, setTitleModal] = useState('');
  const [contentModal, setContentModal] = useState('');
  const setCloseModal = item => {
    if (item === 1) {
      listDataImage.splice(indexImage - 1, 1);
      setImageShow(listDataImage[0]);
    }
    setModalProcess(false);
  };
  const [type, setType] = useState(1);
  const [imageShow, setImageShow] = useState(data[0] || '');
  const [indexImage, setIndexImage] = useState(1);
  const [listDataImage, setListDataImage] = useState(data || []);
  const renderItem = (item, index) => (
    <TouchableOpacity
      onPress={() => {
        setImageShow(item);
        setIndexImage(index === 0 ? 1 : index + 1);
      }}
      style={{marginRight: 12}}>
      <Image
        source={{
          uri: item,
        }}
        style={{
          borderRadius: 8,
          width: 66,
          height: 66,
        }}
      />
    </TouchableOpacity>
  );
  useEffect(() => {
    if (!visible) {
      return;
    }
    setListDataImage(data);
    setImageShow(data[0]);
  }, [visible]);

  const setChange = () => {
    console.log('listDataImage', listDataImage);
    onChange(listDataImage);
  };

  if (!visible) {
    return <View />;
  }
  const removeItemImage = () => {
    setModalProcess(true);
    setTitleModal('Xác nhận xóa ảnh');
    setContentModal('Bạn có chắc chắn muốn xóa ảnh này hay không!');
  };

  function chooseFileLast() {
    const options = {
      title: 'Thêm hình ảnh',
      cancelButtonTitle: 'Đóng',
      didCancel: true,
      chooseFromLibraryButtonTitle: 'Chọn từ thư viện',
      takePhotoButtonTitle: 'Ảnh mới',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        privateDirectory: true,
      },
      permissionDenied: {
        title: 'Quyền bị từ chối',
        text:
          'Bạn không có quyền truy cập vào máy ảnh, thư viện ảnh và bộ nhớ máy.',
        reTryTitle: 'Thử lại',
        okTitle: 'Đồng ý',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        ProductModel.uploadFile(response)
          .then(data => {
            if (data.status == 1) {
              setListDataImage([...listDataImage, data.data.full_path]);
            }
          })
          .catch(e => {
            console.log('e', e);
          });
      }
    });
  }

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <ModalProcess
        typeButton={2}
        visible={modalProcess}
        title={titleModal}
        content={contentModal}
        onChange={item => setCloseModal(item)}
      />
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#F9F9F9F0',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
          }}>
          <TouchableOpacity onPress={() => setChange()}>
            <Image source={require('../../../assets/back.png')} />
          </TouchableOpacity>
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.BLACK_ROOT,
            }}>
            {header}
          </Text>
          <TouchableOpacity onPress={() => setChange()}>
            <Text
              style={{
                ...SanFrancisco.regular,
                fontSize: 17,
                lineHeight: 20,
                color: Const.BLUE,
              }}>
              Xong
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{backgroundColor: '#00000066', flex: 1}}>
          <View
            style={{
              zIndex: 99999,
              position: 'absolute',
              top: 16,
              right: 16,
              backgroundColor: Const.BLACK_ROOT,
              paddingTop: 5,
              paddingBottom: 4,
              paddingRight: 6,
              paddingLeft: 6,
              borderRadius: 4,
            }}>
            <Text
              style={{
                ...SanFrancisco.regular,
                fontSize: 13,
                lineHeight: 15,
                color: Const.WHITE,
              }}>
              {indexImage}/{listDataImage.length}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              justifySelf: 'center',
              justifyItems: 'center',
              justifyContent: 'center',
              alignContent: 'center',
            }}
          />
          <Image
            source={{
              uri: imageShow,
            }}
            style={{
              width: width,
              height: 210,
              marginBottom: 44,
            }}
          />
          {/*<TextInput*/}
          {/*  style={[*/}
          {/*    {*/}
          {/*      backgroundColor: '#FFFFFF',*/}
          {/*      height: 44,*/}
          {/*      fontSize: 17,*/}
          {/*      letterSpacing: -0.3,*/}
          {/*      ...SanFrancisco.regular,*/}
          {/*      color: '#3C3C4399',*/}
          {/*      textAlign: 'left',*/}
          {/*      marginTop: 45,*/}
          {/*      borderTopRightRadius: 0,*/}
          {/*      borderTopLeftRadius: 0,*/}
          {/*    },*/}
          {/*  ]}*/}
          {/*  underlineColor="transparent"*/}
          {/*  placeholder="Nhập chú thích cho ảnh"*/}
          {/*  placeholderTextColor="#999999"*/}
          {/*  returnKeyType="next"*/}
          {/*  // value={filterObject.value}*/}
          {/*  onChangeText={text => setFilterObject(text)}*/}
          {/*  // error={!!filterObject.error}*/}
          {/*  // errorText={filterObject.error}*/}
          {/*  theme={{colors: {primary: '#999999', text: '#000000'}}}*/}
          {/*  autoCapitalize="none"*/}
          {/*  autoCompleteType="email"*/}
          {/*  textContentType="emailAddress"*/}
          {/*  keyboardType="email-address"*/}
          {/*  marginVerNone={0}*/}
          {/*/>*/}
          <Selection
            size={16}
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 26,
              backgroundColor: '#F9F9F9F0',
              paddingTop: 25,
              paddingBottom: 25,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Option
                label={'Ảnh đại diện'}
                value={1}
                checked={type}
                onChange={() => {
                  setType(1);
                }}
                style={{marginRight: 27}}
              />
              <Option
                label={'Ảnh 360'}
                value={2}
                checked={type}
                onChange={() => {
                  setType(2);
                }}
              />
            </View>
            <TouchableOpacity onPress={() => removeItemImage()}>
              <Image source={require('../../../assets/post/bx-trash.png')} />
            </TouchableOpacity>
          </Selection>
          <Selection
            size={16}
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              backgroundColor: Const.WHITE,
              paddingTop: 16,
              paddingBottom: 19,
              width: width,
            }}>
            <FlatList
              horizontal={true}
              data={listDataImage}
              renderItem={({item, index}) => renderItem(item, index)}
            />
            <TouchableOpacity
              style={{
                backgroundColor: Const.GRAY_v7,
                borderWidth: 1,
                borderColor: Const.GRAY_v6,
                borderRadius: 8,
                textAlign: 'center',
                justifySelf: 'center',
                justifyContent: 'center',
                justifyItems: 'center',
                alignItems: 'center',
                paddingRight: 3,
                paddingLeft: 4,
                marginLeft: 12,
                width: 66,
                height: 66,
              }}>
              <Image
                source={require('../../../assets/post/bx-image-add.png')}
              />
              <TouchableOpacity
                onPress={() => {
                  chooseFileLast();
                }}>
                <Text
                  style={{
                    ...SanFrancisco.medium,
                    fontSize: 11,
                    lineHeight: 13,
                    color: Const.BLUE,
                    marginTop: 8,
                  }}>
                  Thêm ảnh
                </Text>
              </TouchableOpacity>
            </TouchableOpacity>
          </Selection>
        </View>
      </View>
    </View>
  );
}

export default CustomizePhotos;
