import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  Dimensions,
} from 'react-native';
import {styles} from '../../core/post/poststep_1/styles';
import {Body, Container, Text} from 'native-base';
const {width, height} = Dimensions.get('screen');
import Button from '../../components/Button';
import Header from '../../components/Header';

export default function PostStep_1({navigation}) {
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Đăng tin rao" navigation={navigation} />
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <View style={[styles.blockPost, {height: height / 2}]}>
            <Image source={require('../../assets/post/Group2148.png')} />
            <Text style={styles.textBBSN}>Bán bất động sản</Text>
            <Button
              labelStyle={styles.buttonTitle}
              style={styles.buttonLogin}
              type="blue"
              mode="contained"
              onPress={() => {
                navigation.navigate('PostStep_1');
              }}>
              Đăng tin bán
            </Button>
          </View>
          <View style={[styles.blockPostBottom, {height: height / 2}]}>
            <Image source={require('../../assets/post/Group2153.png')} />
            <Text style={styles.textBBSN}>Cho thuê bất động sản</Text>
            <Button
              labelStyle={styles.buttonTitle}
              style={[styles.buttonLogin, {width: 230}]}
              type="blue"
              mode="contained"
              onPress={() => {
                navigation.navigate('PostStep_1');
              }}>
              Đăng tin cho thuê
            </Button>
          </View>
        </SafeAreaView>
      </ScrollView>
    </Container>
  );
}
