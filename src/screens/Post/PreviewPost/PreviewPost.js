import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  ScrollView,
} from 'react-native';
import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';
import Selection from '../../../components/Selection';
import {styles} from '../../../core/post/Preview/styles';
import {SliderBox} from 'react-native-image-slider-box';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Button from '../../../components/Button';
import ModalProcessV2 from '../../../components/ListModal/ModalProcessV2';
import {ProductModel} from '../../../models/product';
import moment from 'moment';

function PreviewPost({visible, onChange, data, header, ...props}) {
  //modal
  const [typeButtonPost, setTypeButtonPost] = useState(1);
  const [titleButtonPostOk, setTitleButtonPostOk] = useState('');
  const [modalProcess, setModalProcess] = useState(false);
  const [titleModal, setTitleModal] = useState('');
  const [contentModal, setContentModal] = useState('');
  const [listMarker, setListMarker] = useState([
    data.maps || {
      latitude: 21.0277644,
      longitude: 105.8341598,
    },
  ]);
  const setCloseModal = item => {
    if (item === 1) {
      props.navigation.navigation.reset({
        index: 0,
        routes: [{name: 'Post'}],
      });
    } else if (item === 2) {
      props.navigation.navigate('Dashboard', {object: {}});
    }
    setModalProcess(false);
  };
  const [object, setObject] = useState(data || '');
  const [slider, setSlider] = useState([]);
  const [indexTotal, setIndexTotal] = useState(1);
  const [idPost, setIdPost] = useState('');
  useEffect(() => {
    if (!visible) {
      return;
    }
    setObject(data);
    setSlider(
      data?.listAvatar.length > 0
        ? data?.listAvatar
        : [require('../../../assets/dashboard/no_image.png')],
    );
    setListMarker([
      data?.maps || {
        latitude: 21.0277644,
        longitude: 105.8341598,
      },
    ]);
  }, [visible]);

  const setWallet = () => {
    onChange(1);
  };
  const onSubmitPost = () => {
    ProductModel.createProductTypeBuy(
      data?.title?.value,
      data?.objectForm?.value,
      data?.objectCategories?.id,
      data?.objectCity?.id,
      data?.objectDistricts?.id,
      data?.objectWards?.id,
      data?.objectStreets?.id,
      data?.objectProject?.id,
      data?.address?.value,
      data?.maps.latitude,
      data?.maps.longitude,
      data?.area?.value,
      data?.price?.value,
      data?.objectPriceType?.value,
      data?.description?.value,
      data?.seller_name?.value,
      data?.seller_address?.value,
      data?.seller_phone_number?.value,
      data?.seller_email?.value,
      data?.startDate,
      data?.endDate,
      data?.listAvatar,
      data?.floor_numbers?.value,
      data?.bedroom_numbers?.value,
      data?.toilet_numbers?.value,
      data?.width_numbers?.value,
      data?.land_width?.value,
      data?.objectDirection?.value,
      data?.objectBaconly?.value,
      data?.furniture?.value,
      data?.legality?.value,
      data?.objectAdver?.id,
    )
      .then(data => {
        if (data.status == 1) {
          setTypeButtonPost(2);
          setModalProcess(true);
          setTitleModal('Đăng tin thành công');
          setContentModal(data.message);
          setIdPost(data.data.id);
          setTitleButtonPostOk('Đăng tiếp');
        } else {
          setTitleButtonPostOk('Ok');
          setIdPost('');
          setTypeButtonPost(1);
          setModalProcess(true);
          setTitleModal('Thông báo');
          setContentModal(data.message);
        }
      })
      .catch(e => {
        props.navigation.navigate('Dashboard', {object: {}});
      });
  };

  if (!visible) {
    return <View />;
  }

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <ModalProcessV2
        typeButton={typeButtonPost}
        visible={modalProcess}
        title={titleModal}
        content={contentModal}
        id={idPost}
        onChange={item => setCloseModal(item)}
        titleOk={titleButtonPostOk}
        titleBack={'kết thúc'}
      />
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#F9F9F9F0',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
          }}>
          <TouchableOpacity onPress={() => setWallet()}>
            <Image source={require('../../../assets/back.png')} />
          </TouchableOpacity>
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.BLACK_ROOT,
            }}>
            {header}
          </Text>
          <View>
            <Text>&nbsp;</Text>
          </View>
        </View>
        <View style={{marginBottom: 50}}>
          <ScrollView>
            <View style={styles.blockCount}>
              <Text style={styles.totalCount}>
                {indexTotal}/{slider.length > 0 ? slider.length : 0}
              </Text>
            </View>
            <SliderBox
              images={slider}
              resizeMethod={'resize'}
              resizeMode={'cover'}
              paginationBoxVerticalPadding={5}
              currentImageEmitter={index => {
                setTimeout(() => {
                  setIndexTotal(index + 1);
                }, 400);
              }}
              sliderBoxHeight={210}
            />
            <Selection size={16}>
              <Text style={styles.time}>Xem trước</Text>
              <Text style={styles.title}>{object?.title?.value}</Text>
              <Text style={styles.price}>
                {object?.price?.value}&nbsp;
                {object?.objectPriceType?.name}
              </Text>
              <Text style={styles.price_dt}>{object?.address?.value}</Text>
              {/*<TouchableOpacity style={styles.blockMap}>*/}
              {/*  <Image*/}
              {/*    source={require('../../../assets/dashboard/Post/Path1270.png')}*/}
              {/*  />*/}
              {/*  <Text style={styles.textMap}>Xem bản đồ</Text>*/}
              {/*</TouchableOpacity>*/}
              <View>
                <Text style={styles.textMT}>Mô tả</Text>
                <Text style={styles.contentMT}>
                  {object?.description?.value}
                </Text>
                <Text style={styles.ShowHidden}>Hiển thị thêm</Text>
              </View>
              <Text style={styles.titleInfo}>Thông tin chi tiết</Text>
              {/*<View style={styles.blockNews}>*/}
              {/*  <Text style={styles.newLeft}>Loại tin rao</Text>*/}
              {/*  <Text style={styles.newRight}>Nhà đất bán</Text>*/}
              {/*</View>*/}
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Loại bất động sản</Text>
                <Text style={styles.newRight}>
                  {object?.objectCategories?.name}
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Diện tích</Text>
                <Text style={styles.newRight}>
                  {object?.area?.value || 0} m2
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Địa chỉ</Text>
                <Text style={styles.newRight}>{object?.address?.value}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Mức giá</Text>
                <Text style={styles.newRight}>
                  ~{object?.price?.value}&nbsp;
                  {object?.objectPriceType?.name}
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Phòng ngủ</Text>
                <Text style={styles.newRight}>
                  {object?.bedroom_numbers?.name || 0} PN
                </Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Hướng nhà</Text>
                <Text style={styles.newRight}>
                  {object?.objectDirection?.name}
                </Text>
              </View>
              {/*<View style={styles.blockNews}>*/}
              {/*  <Text style={styles.newLeft}>Mã tin đăng</Text>*/}
              {/*  <Text style={styles.newRight}>330561</Text>*/}
              {/*</View>*/}
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Loại tin đăng</Text>
                <Text style={styles.newRight}>{object?.objectAdver?.name}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Ngày đăng tin</Text>
                <Text style={styles.newRight}>{object?.startDate}</Text>
              </View>
              <View style={styles.blockNews}>
                <Text style={styles.newLeft}>Ngày hết hạn</Text>
                <Text style={styles.newRight}>{object?.endDate}</Text>
              </View>
            </Selection>
            <MapView
              style={styles.ggMap}
              initialRegion={{
                latitude: 21.0277644,
                longitude: 105.8341598,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
              showsUserLocation
              minZoomLevel={11}
              provider={PROVIDER_GOOGLE}>
              {listMarker.map(marker => (
                <Marker coordinate={marker} />
              ))}
            </MapView>
            <Selection
              size={16}
              style={{
                borderTopWidth: 1,
                borderTopColor: '#E6E6E6',
                paddingTop: 12,
              }}>
              <Button
                titleStyle={styles.buttonTitle}
                style={styles.buttonLogin}
                type="blue"
                title="Đăng tin"
                mode="contained"
                onPress={() => onSubmitPost()}
              />
            </Selection>
          </ScrollView>
        </View>
      </View>
    </View>
  );
}

export default PreviewPost;
