import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  Dimensions,
} from 'react-native';
import {styles} from '../../core/post/styles';
import {Body, Container, Text} from 'native-base';

const {width, height} = Dimensions.get('screen');
import Button from '../../components/Button';
import StepIndicator from 'react-native-step-indicator';
import Selection from '../../components/Selection';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import PositionMaps from '../../components/PositionMaps/PositionMaps';
import TextInput from '../../components/TextInput';
import CustomizePhotos from './CustomizePhotos/CustomizePhotos';
import PreviewPost from './PreviewPost/PreviewPost';
import {useFocusEffect} from '@react-navigation/native';
import Categories from '../../components/Categories/Categories';
import ListCity from '../../components/ListCity/ListCity';
import ListDistricts from '../../components/ListDistricts/ListDistricts';
import ListWards from '../../components/ListWards/ListWards';
import ListStreets from '../../components/ListStreets/ListStreets';
import ListProject from '../../components/ListProject/ListProject';
import ListDirection from '../../components/ListDirection/ListDirection';
import ModalProcessV2 from '../../components/ListModal/ModalProcessV2';
import {ProductModel} from '../../models/product';
import {categoriesValidator} from '../../helpers/categoriesValidator';
import {cityValidator} from '../../helpers/cityValidator';
import {districtsValidator} from '../../helpers/districtsValidator';
import {titleValidator} from '../../helpers/titleValidator';
import {areaValidator} from '../../helpers/areaValidator';
import ListForm from '../../components/ListForm/ListForm';
import {formValidator} from '../../helpers/formValidator';
import ListPriceType from '../../components/ListPriceType/ListPriceType';
import ImagePicker from 'react-native-image-picker';
import ListBaconly from '../../components/ListBaconly/ListBaconly';
import CustomDatePicker from '../../components/DatePicker';
import moment from 'moment';
import TypePostType from '../../components/TypePostType/TypePostType';
import FormatNumber from '../../components/FormatNumber';
import {addressValidator} from '../../helpers/addressValidator';
import {descriptionValidator} from '../../helpers/descriptionValidator';
import {phoneValidator} from '../../helpers/phone';
import ListPriceConditions from '../../components/ListPriceConditions/ListPriceConditions';
import ListAreaConditions from '../../components/ListAreaConditions/ListAreaConditions';

const CDatePicker = ({label, dateE = new Date(), ...props}) => {
  const [visible, setVisible] = useState(false);

  const setDate = date => {
    props.onChange(date);
  };

  return (
    <View>
      <CustomDatePicker
        onDateChange={e => {
          setDate(e);
        }}
        date={dateE}
        visible={visible}
        onChangeVisible={() => {
          setVisible(false);
        }}
        onChangeDate={d => setDate(d)}
      />
      <TouchableOpacity
        style={[
          styles.blockInfoPost,
          label == 'Ngày bắt đầu' ? {marginTop: 0} : null,
        ]}>
        <View>
          <Text style={styles.textRightInfo}>{label}</Text>
        </View>
        <View style={styles.blockAllIcon}>
          <Text style={[styles.textRightInfoDate]}>
            {moment(dateE).format('YYYY/MM/DD')}
          </Text>
          <TouchableOpacity onPress={() => setVisible(true)}>
            <Image
              source={require('../../assets/Acount/filterpost/bx-calendar.png')}
            />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </View>
  );
};
export default function Post({navigation}) {
  const [loading, setLoading] = useState(false);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(moment(new Date()).day(+30));
  const [totalDays, setTotalDays] = useState(0);
  const [typePost, setTypePost] = useState(1);
  const [typeButtonPost, setTypeButtonPost] = useState(1);
  const [titleButtonPostOk, setTitleButtonPostOk] = useState('');
  const [listAvatar, setListAvatar] = useState([]);
  const [currentPosition, setCurrentPosition] = useState('');
  const [address, setAddress] = useState({value: '', error: ''});
  const [title, setTitle] = useState({value: '', error: ''});
  const [area, setArea] = useState({value: '', error: ''});
  const [price, setPrice] = useState({value: '', error: ''});
  const [description, setDescription] = useState({value: '', error: ''});
  const [floor_numbers, setFloor_numbers] = useState({value: '', error: ''});
  const [furniture, setFurniture] = useState({value: '', error: ''});
  const [legality, setLegality] = useState({value: '', error: ''});
  const [bedroom_numbers, setBedroom_numbers] = useState({
    value: '',
    error: '',
  });
  const [toilet_numbers, setToilet_numbers] = useState({value: '', error: ''});
  const [width_numbers, setWidth_numbers] = useState({value: '', error: ''});
  const [land_width, setLand_width] = useState({value: '', error: ''});
  const [seller_name, setSeller_name] = useState({
    value: global?.userinfo?.fullname || '',
    error: '',
  });
  const [seller_address, setSeller_address] = useState({
    value: global?.userinfo?.address || '',
    error: '',
  });
  const [seller_phone_number, setSeller_phone_number] = useState({
    value: global?.userinfo?.phone || '',
    error: '',
  });
  const [seller_email, setSeller_email] = useState({
    value: global?.userinfo?.email || '',
    error: '',
  });
  const [titleHeader, setTitleHeader] = useState('Đăng tin rao');
  const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 1,
    currentStepStrokeWidth: 1,
    stepStrokeCurrentColor: '#004182',
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: '#004182',
    stepStrokeUnFinishedColor: '#E6E6E6',
    separatorFinishedColor: '#004182',
    separatorUnFinishedColor: '#E6E6E6',
    stepIndicatorFinishedColor: '#004182',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#004182',
    stepIndicatorLabelFontSize: 14,
    currentStepIndicatorLabelFontSize: 14,
    stepIndicatorLabelCurrentColor: '#ffffff',
    stepIndicatorLabelCurrentBackground: '#004182',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#777777',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#fe7013',
  };
  //Preview
  const [popupPreview, setPopupPreview] = useState(false);
  const [objectPreview, setObjectPreview] = useState('');
  const setCloseSelectPreview = item => {
    // setObjectPreview(item);
    setPopupPreview(false);
    setCurrentPosition(2);
  };
  //Vitri
  const [popupListAdver, setPopupListAdver] = useState(false);
  const [objectAdver, setObjectAdver] = useState({
    name: 'Tất cả',
    id: '',
    error: '',
  });
  const setCloseSelect = item => {
    setObjectAdver(item);
    setPopupListAdver(false);
  };
  //hinhthuc
  const [popupListForm, setPopupListForm] = useState(false);
  const [objectForm, setObjectForm] = useState({
    name: 'Tất cả',
    value: '',
    error: '',
  });
  const setCloseSelectForm = item => {
    setObjectForm({...item, error: ''});
    setPopupListForm(false);
  };
  //maps
  const [popupPositionMaps, setPopupPositionMaps] = useState(false);
  const [objectPositionMap, setObjectPositionMap] = useState([
    {
      latitude: 21.0277644,
      longitude: 105.8341598,
    },
  ]);
  const [regonObject, setRegonObject] = useState({
    latitude: 21.0277644,
    longitude: 105.8341598,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const setCloseMaps = item => {
    setObjectPositionMap(item);
    setPopupPositionMaps(false);
  };
  //tùy chỉnh ảnh
  const [popupCustomizePhoto, setPopupCustomizePhoto] = useState(false);
  const setCloseSelectCustomizePhoto = item => {
    setListAvatar(item);
    setPopupCustomizePhoto(false);
  };
  //loaibatdongsan
  const [popupCategories, setPopupCategories] = useState(false);
  const [objectCategories, setObjectCategories] = useState({
    name: 'Tất cả',
    id: '',
    error: '',
  });
  const setCloseSelectCategories = item => {
    setObjectCategories({...item, error: ''});
    setPopupCategories(false);
  };
  //thanhPho
  const [popupCity, setPopupCity] = useState(false);
  const [objectCity, setObjectCity] = useState({
    name: 'Trên toàn quốc',
    id: '',
    error: '',
  });
  const setCloseSelectCity = item => {
    setObjectCity({...item, error: ''});
    setPopupCity(false);
  };
  //quan/huyen
  const [popupDistricts, setPopupDistricts] = useState(false);
  const [objectDistricts, setObjectDistricts] = useState({
    name: 'Tất cả',
    id: '',
    error: '',
  });
  const setCloseSelectDistricts = item => {
    setObjectDistricts({...item, error: ''});
    setPopupDistricts(false);
  };
  //phuong/xa
  const [popupWards, setPopupWards] = useState(false);
  const [objectWards, setObjectWards] = useState({
    name: 'Tất cả',
    id: '',
    error: '',
  });
  const setCloseSelectWards = item => {
    setObjectWards({...item, error: ''});
    setPopupWards(false);
  };
  //duong/pho
  const [popupStreets, setPopupStreets] = useState(false);
  const [objectStreets, setObjectStreets] = useState({
    name: 'Tất cả',
    id: '',
    error: '',
  });
  const setCloseSelectStreets = item => {
    setObjectStreets({...item, error: ''});
    setPopupStreets(false);
  };
  //duAn
  const [popupProject, setPopupProject] = useState(false);
  const [objectProject, setObjectProject] = useState({
    name: 'Tất cả',
    id: '',
    error: '',
  });
  const setCloseSelectProject = item => {
    setObjectProject({...item, error: ''});
    setPopupProject(false);
  };
  //donvigia
  const [popupPriceType, setPopupPriceType] = useState(false);
  const [objectPriceType, setObjectPriceType] = useState({
    name: 'Đơn vị giá',
    value: '',
    error: '',
  });
  const setCloseSelectPriceType = item => {
    setObjectPriceType({...item, error: ''});
    setPopupPriceType(false);
  };
  //select gia
  const [popupPriceConditions, setPopupPriceConditions] = useState(false);
  const [objectPriceConditions, setObjectPriceConditions] = useState({
    name: 'Chọn mức giá',
    id: '',
    error: '',
  });
  const setCloseSelectPriceConditions = item => {
    setObjectPriceConditions({...item, error: ''});
    setPopupPriceConditions(false);
  };
  //select dien tich
  const [popupAreaConditions, setPopupAreaConditions] = useState(false);
  const [objectAreaConditions, setObjectAreaConditions] = useState({
    name: 'Chọn diện tích',
    id: '',
    error: '',
  });
  const setCloseSelectAreaConditions = item => {
    setObjectAreaConditions({...item, error: ''});
    setPopupAreaConditions(false);
  };
  //huongbancong
  const [popupListBaconly, setPopupListBaconly] = useState(false);
  const [objectBaconly, setObjectBaconly] = useState({
    name: 'Tất cả',
    value: '',
  });
  const setCloseSelectBaconly = item => {
    setObjectBaconly(item);
    setPopupListBaconly(false);
  };
  //huongnha
  const [popupListDirection, setPopupListDirection] = useState(false);
  const [objectDirection, setObjectDirection] = useState({
    name: 'Tất cả',
    value: '',
  });
  const setCloseSelectDirection = item => {
    setObjectDirection(item);
    setPopupListDirection(false);
  };
  //modal
  const [modalProcess, setModalProcess] = useState(false);
  const [titleModal, setTitleModal] = useState('');
  const [contentModal, setContentModal] = useState('');
  const [idPost, setIdPost] = useState('');
  const setCloseModal = item => {
    if (item === 1) {
      navigation.reset({
        index: 0,
        routes: [{name: 'Post'}],
      });
    } else if (item === 2) {
      navigation.navigate('Dashboard', {object: {}});
    }
    setModalProcess(false);
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        setPopupCustomizePhoto(true);
      }}>
      <Image
        source={{
          uri: item,
        }}
        style={styles.image}
      />
    </TouchableOpacity>
  );
  const renderItemNotImage = ({item}) => (
    <View>
      <Image
        source={require('../../assets/dashboard/no_image.png')}
        style={styles.image}
      />
    </View>
  );
  const onValidate = () => {
    switch (currentPosition) {
      case 0:
        return validateCaseOne();
      case 1:
        return validateCasetwo();
      case 2:
        return validateCaseThree();
      default:
        return null;
    }
  };
  const validateCaseOne = () => {
    const objectFormError = formValidator(objectForm.value);
    setObjectForm({...objectForm, error: objectFormError});
    const objectCategoriesError = categoriesValidator(objectCategories.id);
    setObjectCategories({...objectCategories, error: objectCategoriesError});
    const objectCityError = cityValidator(objectCity.id);
    setObjectCity({...objectCity, error: objectCityError});
    const objectDistrictsError = districtsValidator(objectDistricts.id);
    setObjectDistricts({...objectDistricts, error: objectDistrictsError});
    const addressError = addressValidator(address.value);
    setAddress({...address, error: addressError});
    if (
      !objectFormError &&
      !objectCategoriesError &&
      !objectCityError &&
      !objectDistrictsError &&
      !addressError
    ) {
      NextStep(
        currentPosition === 2
          ? 2
          : currentPosition === 0
          ? 1
          : currentPosition + 1,
      );
    }
  };
  const validateCasetwo = () => {
    const titleError = titleValidator(title.value);
    setTitle({...title, error: titleError});
    // const areaError = areaValidator(area.value);
    // setArea({...area, error: areaError});
    const descriptionError = descriptionValidator(description.value);
    setDescription({...description, error: descriptionError});
    if (!titleError && !descriptionError) {
      NextStep(
        currentPosition === 2
          ? 2
          : currentPosition === 0
          ? 1
          : currentPosition + 1,
      );
    }
  };
  const validateCaseThree = () => {
    const seller_phone_numberError = phoneValidator(seller_phone_number.value);
    setSeller_phone_number({
      ...seller_phone_number,
      error: seller_phone_numberError,
    });
    if (!seller_phone_numberError) {
      if (typePost === 1) {
        onSubmitPostTypeSell();
      } else {
        onSubmitPostTypeBuy();
      }
    }
  };
  const onSubmitPostTypeBuy = () => {
    setLoading(true);
    ProductModel.createProductTypeBuy(
      title.value,
      objectForm.value,
      objectCategories.id,
      objectCity.id,
      objectDistricts.id,
      objectWards.id,
      objectStreets.id,
      objectProject.id,
      address.value,
      objectPositionMap[0].latitude,
      objectPositionMap[0].longitude,
      objectAreaConditions.id,
      objectPriceConditions.id,
      description.value,
      seller_name.value,
      seller_address.value,
      seller_phone_number.value,
      seller_email.value,
      moment(startDate).format('DD-MM-YYYY'),
      moment(endDate).format('DD-MM-YYYY'),
      listAvatar,
    )
      .then(data => {
        setLoading(false);
        if (data.status == 1) {
          setTypeButtonPost(2);
          setModalProcess(true);
          setTitleModal('Đăng tin thành công');
          setContentModal(data.message);
          setIdPost(data.data.id);
          setTitleButtonPostOk('Đăng tiếp');
        } else {
          setTitleButtonPostOk('Ok');
          setIdPost('');
          setTypeButtonPost(1);
          setModalProcess(true);
          setTitleModal('Thông báo');
          setContentModal(data.message);
        }
      })
      .catch(e => {
        navigation.navigate('Dashboard', {object: {}});
      });
  };
  const onSubmitPostTypeSell = () => {
    setLoading(true);
    ProductModel.createProductTypeSell(
      title.value,
      objectForm.value,
      objectCategories.id,
      objectCity.id,
      objectDistricts.id,
      objectWards.id,
      objectStreets.id,
      objectProject.id,
      address.value,
      objectPositionMap[0].latitude,
      objectPositionMap[0].longitude,
      area.value,
      price.value,
      objectPriceType.value,
      description.value,
      seller_name.value,
      seller_address.value,
      seller_phone_number.value,
      seller_email.value,
      moment(startDate).format('DD-MM-YYYY'),
      moment(endDate).format('DD-MM-YYYY'),
      listAvatar,
      floor_numbers.value,
      bedroom_numbers.value,
      toilet_numbers.value,
      width_numbers.value,
      land_width.value,
      objectDirection.value,
      objectBaconly.value,
      furniture.value,
      legality.value,
      objectAdver.id,
    )
      .then(data => {
        setLoading(false);
        if (data.status == 1) {
          setTypeButtonPost(2);
          setModalProcess(true);
          setTitleModal('Đăng tin thành công');
          setContentModal(data.message);
          setIdPost(data.data.id);
          setTitleButtonPostOk('Đăng tiếp');
        } else {
          setTitleButtonPostOk('Ok');
          setIdPost('');
          setTypeButtonPost(1);
          setModalProcess(true);
          setTitleModal('Thông báo');
          setContentModal(data.message);
        }
      })
      .catch(e => {
        navigation.navigate('Dashboard', {object: {}});
      });
  };

  function chooseFileLast() {
    const options = {
      title: 'Thêm hình ảnh',
      cancelButtonTitle: 'Đóng',
      didCancel: true,
      chooseFromLibraryButtonTitle: 'Chọn từ thư viện',
      takePhotoButtonTitle: 'Ảnh mới',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        privateDirectory: true,
      },
      permissionDenied: {
        title: 'Quyền bị từ chối',
        text:
          'Bạn không có quyền truy cập vào máy ảnh, thư viện ảnh và bộ nhớ máy.',
        reTryTitle: 'Thử lại',
        okTitle: 'Đồng ý',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        ProductModel.uploadFile(response)
          .then(data => {
            if (data.status == 1) {
              setListAvatar([...listAvatar, data.data.full_path]);
            }
          })
          .catch(e => {
            console.log('e', e);
          });
      }
    });
  }

  const getListProductType = () => {
    ProductModel.listProductTypes()
      .then(data => {
        if (data.status == 1) {
          setObjectAdver(data.data[0]);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  const handleTotalDate = (end, start) => {
    setTotalDays(moment(end).diff(moment(start), 'days'));
  };
  const handleDataPreview = () => {
    setObjectPreview({
      title: title,
      objectForm: objectForm,
      objectCategories: objectCategories,
      objectCity: objectCity,
      objectDistricts: objectDistricts,
      objectWards: objectWards,
      objectStreets: objectStreets,
      objectProject: objectProject,
      address: address,
      maps: {
        latitude: objectPositionMap[0].latitude,
        longitude: objectPositionMap[0].longitude,
      },
      area: area,
      price: price,
      objectPriceType: objectPriceType,
      description: description,
      seller_name: seller_name,
      seller_address: seller_address,
      seller_phone_number: seller_phone_number,
      seller_email: seller_email,
      startDate: moment(startDate).format('DD-MM-YYYY'),
      endDate: moment(endDate).format('DD-MM-YYYY'),
      listAvatar: listAvatar,
      floor_numbers: floor_numbers,
      bedroom_numbers: bedroom_numbers,
      toilet_numbers: toilet_numbers,
      width_numbers: width_numbers,
      land_width: land_width,
      objectDirection: objectDirection,
      objectBaconly: objectBaconly,
      furniture: furniture,
      legality: legality,
      objectAdver: objectAdver,
    });
    setPopupPreview(true);
  };

  useFocusEffect(
    React.useCallback(() => {
      setCurrentPosition('');
      getListProductType();
      handleTotalDate(endDate, startDate);
    }, []),
  );
  const TabOne = () => {
    return (
      <View style={styles.blockWrap}>
        <TouchableOpacity
          onPress={() => setPopupListForm(true)}
          style={[
            styles.itemQlTKTP,
            objectForm.error !== ''
              ? {borderBottomColor: '#FF3B30', marginBottom: 8}
              : {},
          ]}>
          <View>
            <Text style={styles.titleText}>Chọn hình thức*</Text>
            <Text
              style={[
                objectForm.error === ''
                  ? styles.textLeftInfo
                  : styles.textLeftInfoNotcontent,
              ]}>
              {objectForm.name}
            </Text>
          </View>
          <View style={styles.blockIconRightInput}>
            <Image
              source={require('../../assets/Vector.png')}
              style={[
                objectForm.error === ''
                  ? styles.vecterNext
                  : styles.vecterNextNotContent,
              ]}
            />
          </View>
        </TouchableOpacity>
        {objectForm.error !== '' ? (
          <Text style={styles.ErrorItem}>{objectForm.error}</Text>
        ) : (
          <View />
        )}
        <TouchableOpacity
          onPress={() => setPopupCategories(true)}
          style={[
            styles.itemQlTKTP,
            objectCategories.error !== ''
              ? {borderBottomColor: '#FF3B30', marginBottom: 8}
              : {},
          ]}>
          <View>
            <Text style={styles.titleText}>Chọn loại bất động sản*</Text>
            <Text
              style={[
                objectCategories.error === ''
                  ? styles.textLeftInfo
                  : styles.textLeftInfoNotcontent,
              ]}>
              {objectCategories.name}
            </Text>
          </View>
          <View style={styles.blockIconRightInput}>
            <Image
              source={require('../../assets/Vector.png')}
              style={[
                objectCategories.error === ''
                  ? styles.vecterNext
                  : styles.vecterNextNotContent,
              ]}
            />
          </View>
        </TouchableOpacity>
        {objectCategories.error !== '' ? (
          <Text style={styles.ErrorItem}>{objectCategories.error}</Text>
        ) : (
          <View />
        )}
        <TouchableOpacity
          onPress={() => setPopupCity(true)}
          style={[
            styles.itemQlTKTP,
            objectCity.error !== ''
              ? {borderBottomColor: '#FF3B30', marginBottom: 8}
              : {},
          ]}>
          <View>
            <Text style={styles.titleText}>Chọn Tỉnh/Thành phố*</Text>
            <Text
              style={[
                objectCity.error === ''
                  ? styles.textLeftInfo
                  : styles.textLeftInfoNotcontent,
              ]}>
              {objectCity.name}
            </Text>
          </View>
          <View style={styles.blockIconRightInput}>
            <Image
              source={require('../../assets/Vector.png')}
              style={[
                objectCity.error === ''
                  ? styles.vecterNext
                  : styles.vecterNextNotContent,
              ]}
            />
          </View>
        </TouchableOpacity>
        {objectCity.error !== '' ? (
          <Text style={styles.ErrorItem}>{objectCity.error}</Text>
        ) : (
          <View />
        )}
        <TouchableOpacity
          onPress={() => setPopupDistricts(true)}
          style={[
            styles.itemQlTKTP,
            objectDistricts.error !== ''
              ? {borderBottomColor: '#FF3B30', marginBottom: 8}
              : {},
          ]}>
          <View>
            <Text style={styles.titleText}>Chọn Quận/Huyện*</Text>
            <Text
              style={[
                objectDistricts.error === ''
                  ? styles.textLeftInfo
                  : styles.textLeftInfoNotcontent,
              ]}>
              {objectDistricts.name}
            </Text>
          </View>
          <View style={styles.blockIconRightInput}>
            <Image
              source={require('../../assets/Vector.png')}
              style={[
                objectDistricts.error === ''
                  ? styles.vecterNext
                  : styles.vecterNextNotContent,
              ]}
            />
          </View>
        </TouchableOpacity>
        {objectDistricts.error !== '' ? (
          <Text style={styles.ErrorItem}>{objectDistricts.error}</Text>
        ) : (
          <View />
        )}
        <TouchableOpacity
          onPress={() => setPopupWards(true)}
          style={[styles.itemQlTKTP]}>
          <View>
            <Text style={styles.titleText}>Chọn Phường/Xã</Text>
            <Text
              style={[
                objectWards
                  ? styles.textLeftInfo
                  : styles.textLeftInfoNotcontent,
              ]}>
              {objectWards.name}
            </Text>
          </View>
          <View style={styles.blockIconRightInput}>
            <Image
              source={require('../../assets/Vector.png')}
              style={styles.vecterNextNotContent}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setPopupStreets(true)}
          style={[styles.itemQlTKTP]}>
          <View>
            <Text style={styles.titleText}>Chọn Đường/Phố</Text>
            <Text
              style={[
                objectStreets
                  ? styles.textLeftInfo
                  : styles.textLeftInfoNotcontent,
              ]}>
              {objectStreets.name}
            </Text>
          </View>
          <View style={styles.blockIconRightInput}>
            <Image
              source={require('../../assets/Vector.png')}
              style={styles.vecterNextNotContent}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setPopupProject(true)}
          style={[styles.itemQlTKTP]}>
          <View>
            <Text style={styles.titleText}>Chọn Dự án</Text>
            <Text
              style={[
                objectProject
                  ? styles.textLeftInfo
                  : styles.textLeftInfoNotcontent,
              ]}>
              {objectProject.name}
            </Text>
          </View>
          <View style={styles.blockIconRightInput}>
            <Image
              source={require('../../assets/Vector.png')}
              style={styles.vecterNextNotContent}
            />
          </View>
        </TouchableOpacity>
        <View style={[styles.itemQlTKTP, {borderBottomWidth: 0}]}>
          <View>
            <Text style={styles.titleText}>Địa chỉ chi tiết*</Text>
            <TextInput
              style={[
                styles.inputDes,
                {
                  width: width - 32,
                  borderBottomColor: '#E6E6E6',
                },
              ]}
              placeholder=""
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={address.value}
              onChangeText={text => setAddress({value: text, error: ''})}
              error={!!address.error}
              errorText={address.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
          </View>
          <View style={styles.blockIconRightInput}>
            <Image
              source={require('../../assets/post/LocationIcon.png')}
              style={styles.vecterNextNotContent}
            />
          </View>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => setPopupPositionMaps(true)}
            style={[styles.itemQlTKTP]}>
            <View>
              <Text style={styles.titleText}>Vị trí trên bản đồ</Text>
              <Text style={styles.textLeftInfoNotcontent} />
            </View>
            <View style={styles.blockIconRightInput}>
              <Image
                source={require('../../assets/Vector.png')}
                style={styles.vecterNextNotContent}
              />
            </View>
          </TouchableOpacity>
          <View style={styles.blockMaps}>
            <MapView
              style={styles.ggMap}
              initialRegion={regonObject}
              region={regonObject}
              showsUserLocation
              provider={PROVIDER_GOOGLE}
              minZoomLevel={11}>
              {objectPositionMap.map(marker => (
                <Marker coordinate={marker} />
              ))}
            </MapView>
          </View>
        </View>
      </View>
    );
  };
  const TabTwo = () => {
    return (
      <Selection size={16}>
        <Text style={styles.textMtcb}>Mô tả cơ bản</Text>
        <View style={styles.itemQlTKMXH}>
          <View>
            <TextInput
              style={[styles.inputLogin]}
              placeholder="Tiêu đề tin*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={title.value}
              onChangeText={text => setTitle({value: text, error: ''})}
              error={!!title.error}
              errorText={title.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
            <Text style={styles.textTT}>
              {title.value !== '' ? title.value.length : 0}/99
            </Text>
          </View>
        </View>
        {typePost === 1 ? (
          <View style={styles.itemQlTKMXH}>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    paddingRight: 15,
                    height: 60,
                  },
                ]}
                placeholder="Diện tích"
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={area.value}
                onChangeText={text => setArea({value: text, error: ''})}
                error={!!area.error}
                errorText={area.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                marginVerNone={0}
                keyboardType="numeric"
              />
              <Text style={styles.m2DT}>m2</Text>
            </View>
          </View>
        ) : null}
        {typePost === 1 ? (
          <View style={[styles.itemQlTKMXH, {flexDirection: 'row'}]}>
            <View>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    height: 60,
                    marginTop: 16,
                    width: width / 2 - 26,
                    marginRight: 20,
                  },
                ]}
                placeholder="Giá"
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={price.value}
                onChangeText={text => setPrice({value: text, error: ''})}
                error={!!price.error}
                errorText={price.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                marginVerNone={0}
                keyboardType="numeric"
              />
            </View>
            <TouchableOpacity
              onPress={() => setPopupPriceType(true)}
              style={[styles.blockPriceType, {width: width / 2 - 26}]}>
              <Text style={styles.priceType}>{objectPriceType.name}</Text>
              <Image
                source={require('../../assets/Vector.png')}
                style={styles.vecterNextNotContent}
              />
            </TouchableOpacity>
          </View>
        ) : (
          <View>
            <TouchableOpacity
              onPress={() => setPopupAreaConditions(true)}
              style={[
                styles.blockPriceType,
                {
                  width: width - 26,
                  marginTop: 0,
                  marginBottom: 26,
                  paddingBottom: 12,
                },
              ]}>
              <Text style={styles.priceType}>{objectAreaConditions.name}</Text>
              <Image
                source={require('../../assets/Vector.png')}
                style={styles.vecterNextNotContent}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupPriceConditions(true)}
              style={[
                styles.blockPriceType,
                {width: width - 26, marginTop: 0, paddingBottom: 12},
              ]}>
              <Text style={styles.priceType}>{objectPriceConditions.name}</Text>
              <Image
                source={require('../../assets/Vector.png')}
                style={styles.vecterNextNotContent}
              />
            </TouchableOpacity>
          </View>
        )}
        <View style={styles.blockPriceImage}>
          {typePost === 1 ? (
            <View style={styles.flex}>
              <Text style={styles.totalPrice}>Tổng tiền:</Text>
              <Text style={styles.price}>
                <FormatNumber
                  value={price.value}
                  currency={
                    objectPriceType.value !== ''
                      ? ' ' + objectPriceType.name
                      : ' Thỏa thuận'
                  }
                />
              </Text>
            </View>
          ) : null}
          <View style={styles.flex}>
            <Text style={[styles.totalPrice, {marginBottom: 28}]}>
              Hình ảnh
            </Text>
            <Image
              source={require('../../assets/post/bxs-info-circle.png')}
              style={{marginLeft: 12}}
            />
          </View>
          <View style={styles.blockImage}>
            {listAvatar.length > 0 ? (
              <FlatList
                horizontal={true}
                data={listAvatar}
                renderItem={renderItem}
              />
            ) : (
              <FlatList
                horizontal={true}
                data={[{}, {}, {}]}
                renderItem={renderItemNotImage}
              />
            )}
            <TouchableOpacity
              onPress={() => {
                chooseFileLast();
              }}
              style={styles.blockAddImage}>
              <Image source={require('../../assets/post/bx-image-add.png')} />
              <Text style={styles.imageAdd}>Thêm ảnh</Text>
            </TouchableOpacity>
          </View>
          {/*<View>*/}
          {/*  <UploadImage*/}
          {/*    width="100%"*/}
          {/*    onChange={setListImage}*/}
          {/*    value={console.log('listImage', listImage)}*/}
          {/*  />*/}
          {/*</View>*/}
          <View style={styles.itemQlTKMXH}>
            <View>
              <TextInput
                style={[styles.inputLogin, {height: 60}]}
                placeholder="Nội dung mô tả*"
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={description.value}
                onChangeText={text => setDescription({value: text, error: ''})}
                error={!!description.error}
                errorText={description.error}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                marginVerNone={0}
              />
              <Text style={styles.textTT}>0/3000</Text>
            </View>
          </View>
          {typePost === 1 ? (
            <View>
              <Text style={styles.textMtcb}>Thông tin khác</Text>
              <View style={styles.blockInfo}>
                <Text style={styles.textRightInfo}>Số tầng</Text>
                <View style={styles.blockNumber}>
                  <TouchableOpacity
                    style={[styles.blockNumberTr, {marginRight: 12}]}>
                    <Text style={styles.numberTr}>-</Text>
                  </TouchableOpacity>
                  <View>
                    <TextInput
                      style={[
                        styles.inputLogin,
                        {
                          height: 29,
                          width: 50,
                          borderWidth: 1,
                          borderBottomWidth: 0,
                          borderColor: '#CECED0',
                          borderRadius: 4,
                          textAlign: 'center',
                        },
                      ]}
                      placeholder=""
                      placeholderTextColor="#999999"
                      returnKeyType="next"
                      value={floor_numbers.value}
                      onChangeText={text =>
                        setFloor_numbers({value: text, error: ''})
                      }
                      error={!!floor_numbers.error}
                      errorText={floor_numbers.error}
                      theme={{colors: {primary: '#999999', text: '#000000'}}}
                      autoCapitalize="none"
                      marginVerNone={0}
                      keyboardType="numeric"
                    />
                  </View>
                  <TouchableOpacity
                    style={[styles.blockNumberTr, {marginLeft: 12}]}>
                    <Text style={styles.numberTr}>+</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[styles.blockInfo, {marginTop: 31}]}>
                <Text style={styles.textRightInfo}>Số phòng ngủ</Text>
                <View style={styles.blockNumber}>
                  <TouchableOpacity
                    // onPress={() => {
                    //   console.log('soTang', soTang);
                    //   setSoTang(soTang > 0 ? soTang - 1 : 0);
                    // }}
                    style={[styles.blockNumberTr, {marginRight: 12}]}>
                    <Text style={styles.numberTr}>-</Text>
                  </TouchableOpacity>
                  <View>
                    <TextInput
                      style={[
                        styles.inputLogin,
                        {
                          height: 29,
                          width: 50,
                          borderWidth: 1,
                          borderColor: '#CECED0',
                          borderBottomWidth: 0,
                          borderRadius: 4,
                          textAlign: 'center',
                        },
                      ]}
                      placeholder=""
                      placeholderTextColor="#999999"
                      returnKeyType="next"
                      value={bedroom_numbers.value}
                      onChangeText={text =>
                        setBedroom_numbers({value: text, error: ''})
                      }
                      error={!!bedroom_numbers.error}
                      errorText={bedroom_numbers.error}
                      theme={{colors: {primary: '#999999', text: '#000000'}}}
                      autoCapitalize="none"
                      marginVerNone={0}
                      keyboardType="numeric"
                    />
                  </View>
                  <TouchableOpacity
                    // onPress={() => {
                    //   console.log('soTang', soTang);
                    //   setSoTang(soTang === 0 ? 1 : Math.fround(soTang + 1));
                    // }}
                    style={[styles.blockNumberTr, {marginLeft: 12}]}>
                    <Text style={styles.numberTr}>+</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[styles.blockInfo, {marginTop: 31}]}>
                <Text style={styles.textRightInfo}>Số toilet</Text>
                <View style={styles.blockNumber}>
                  <TouchableOpacity
                    // onPress={() => {
                    //   console.log('soTang', soTang);
                    //   setSoTang(soTang > 0 ? soTang - 1 : 0);
                    // }}
                    style={[styles.blockNumberTr, {marginRight: 12}]}>
                    <Text style={styles.numberTr}>-</Text>
                  </TouchableOpacity>
                  <View>
                    <TextInput
                      style={[
                        styles.inputLogin,
                        {
                          height: 29,
                          width: 50,
                          borderWidth: 1,
                          borderColor: '#CECED0',
                          borderBottomWidth: 0,
                          borderRadius: 4,
                          textAlign: 'center',
                        },
                      ]}
                      placeholder=""
                      placeholderTextColor="#999999"
                      returnKeyType="next"
                      value={toilet_numbers.value}
                      onChangeText={text =>
                        setToilet_numbers({value: text, error: ''})
                      }
                      error={!!toilet_numbers.error}
                      errorText={toilet_numbers.error}
                      theme={{colors: {primary: '#999999', text: '#000000'}}}
                      autoCapitalize="none"
                      marginVerNone={0}
                      keyboardType="numeric"
                    />
                  </View>
                  <TouchableOpacity
                    // onPress={() => {
                    //   console.log('soTang', soTang);
                    //   setSoTang(soTang === 0 ? 1 : Math.fround(soTang + 1));
                    // }}
                    style={[styles.blockNumberTr, {marginLeft: 12}]}>
                    <Text style={styles.numberTr}>+</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[styles.blockInfo, {marginTop: 31}]}>
                <Text style={styles.textRightInfo}>Mặt tiền (m)</Text>
                <View style={styles.blockNumber}>
                  <TouchableOpacity
                    // onPress={() => {
                    //   console.log('soTang', soTang);
                    //   setSoTang(soTang > 0 ? soTang - 1 : 0);
                    // }}
                    style={[styles.blockNumberTr, {marginRight: 12}]}>
                    <Text style={styles.numberTr}>-</Text>
                  </TouchableOpacity>
                  <View>
                    <TextInput
                      style={[
                        styles.inputLogin,
                        {
                          height: 29,
                          width: 50,
                          borderWidth: 1,
                          borderColor: '#CECED0',
                          borderBottomWidth: 0,
                          borderRadius: 4,
                          textAlign: 'center',
                        },
                      ]}
                      placeholder=""
                      placeholderTextColor="#999999"
                      returnKeyType="next"
                      value={width_numbers.value}
                      onChangeText={text =>
                        setWidth_numbers({value: text, error: ''})
                      }
                      error={!!width_numbers.error}
                      errorText={width_numbers.error}
                      theme={{colors: {primary: '#999999', text: '#000000'}}}
                      autoCapitalize="none"
                      marginVerNone={0}
                      keyboardType="numeric"
                    />
                  </View>
                  <TouchableOpacity
                    // onPress={() => {
                    //   console.log('soTang', soTang);
                    //   setSoTang(soTang === 0 ? 1 : Math.fround(soTang + 1));
                    // }}
                    style={[styles.blockNumberTr, {marginLeft: 12}]}>
                    <Text style={styles.numberTr}>+</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[styles.blockInfo, {marginTop: 31}]}>
                <Text style={styles.textRightInfo}>
                  Chiều rộng đường vào (m)
                </Text>
                <View style={styles.blockNumber}>
                  <TouchableOpacity
                    // onPress={() => {
                    //   console.log('soTang', soTang);
                    //   setSoTang(soTang > 0 ? soTang - 1 : 0);
                    // }}
                    style={[styles.blockNumberTr, {marginRight: 12}]}>
                    <Text style={styles.numberTr}>-</Text>
                  </TouchableOpacity>
                  <View>
                    <TextInput
                      style={[
                        styles.inputLogin,
                        {
                          height: 29,
                          width: 50,
                          borderWidth: 1,
                          borderColor: '#CECED0',
                          borderBottomWidth: 0,
                          borderRadius: 4,
                          textAlign: 'center',
                        },
                      ]}
                      placeholder=""
                      placeholderTextColor="#999999"
                      returnKeyType="next"
                      value={land_width.value}
                      onChangeText={text =>
                        setLand_width({value: text, error: ''})
                      }
                      error={!!land_width.error}
                      errorText={land_width.error}
                      theme={{colors: {primary: '#999999', text: '#000000'}}}
                      autoCapitalize="none"
                      marginVerNone={0}
                      keyboardType="numeric"
                    />
                  </View>
                  <TouchableOpacity
                    // onPress={() => {
                    //   console.log('soTang', soTang);
                    //   setSoTang(soTang === 0 ? 1 : Math.fround(soTang + 1));
                    // }}
                    style={[styles.blockNumberTr, {marginLeft: 12}]}>
                    <Text style={styles.numberTr}>+</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity
                onPress={() => setPopupListDirection(true)}
                style={[styles.itemQlTKTPTab2, {marginTop: 23}]}>
                <View>
                  <Text style={styles.textRightInfo}>Hướng nhà</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text
                    style={[
                      styles.textRightInfo,
                      {color: '#999999', marginRight: 11},
                    ]}>
                    {objectDirection.name}
                  </Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupListBaconly(true)}
                style={[
                  styles.itemQlTKTPTab2,
                  {marginTop: 16, marginBottom: 16},
                ]}>
                <View>
                  <Text style={styles.textRightInfo}>Hướng ban công</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text
                    style={[
                      styles.textRightInfo,
                      {color: '#999999', marginRight: 11},
                    ]}>
                    {objectBaconly.name}
                  </Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <View style={[styles.itemQlTKMXH, {marginBottom: 10}]}>
                <View>
                  <TextInput
                    style={[styles.inputDes]}
                    placeholder="Mô tả nội thất"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    value={furniture.value}
                    onChangeText={text =>
                      setFurniture({value: text, error: ''})
                    }
                    error={!!furniture.error}
                    errorText={furniture.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                  <Text style={styles.textTT}>0/200</Text>
                </View>
              </View>
              <View style={[styles.itemQlTKMXH, {marginBottom: 12}]}>
                <View>
                  <Text style={styles.textRightInfo}>Thông tin pháp lý</Text>
                  <TextInput
                    style={[styles.inputDes]}
                    placeholder="VD:Đã có sổ đỏ,sổ hồng,đã được phê duyệt..."
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    value={legality.value}
                    onChangeText={text => setLegality({value: text, error: ''})}
                    error={!!legality.error}
                    errorText={legality.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                </View>
              </View>
            </View>
          ) : null}
        </View>
      </Selection>
    );
  };
  const TabThree = () => {
    return (
      <Selection size={16}>
        <Text style={styles.textMtcb}>Thông tin liên hệ</Text>
        <View style={styles.itemQlTK}>
          <View>
            <Text style={styles.textLeftInfo_tab3}>Tên liên hệ</Text>
            <TextInput
              style={styles.inputLoginTab3}
              placeholder=""
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={seller_name.value}
              onChangeText={text => setSeller_name({value: text, error: ''})}
              error={!!seller_name.error}
              errorText={seller_name.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
          </View>
        </View>
        <View style={styles.itemQlTK}>
          <View>
            <Text style={[styles.textLeftInfo_tab3, {marginTop: 20}]}>
              Địa chỉ
            </Text>
            <TextInput
              style={styles.inputLoginTab3}
              placeholder=""
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={seller_address.value}
              onChangeText={text => setSeller_address({value: text, error: ''})}
              error={!!seller_address.error}
              errorText={seller_address.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
          </View>
        </View>
        <View style={styles.itemQlTK}>
          <View>
            <Text style={[styles.textLeftInfo_tab3, {marginTop: 20}]}>
              Số điện thoại*
            </Text>
            <TextInput
              style={styles.inputLoginTab3}
              placeholder=""
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={seller_phone_number.value}
              onChangeText={text =>
                setSeller_phone_number({value: text, error: ''})
              }
              error={!!seller_phone_number.error}
              errorText={seller_phone_number.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
              keyboardType="numeric"
            />
          </View>
        </View>
        <View style={styles.itemQlTK}>
          <View>
            <Text style={[styles.textLeftInfo_tab3, {marginTop: 20}]}>
              Email
            </Text>
            <TextInput
              style={styles.inputLoginTab3}
              placeholder=""
              placeholderTextColor="#999999"
              returnKeyType="next"
              value={seller_email.value}
              onChangeText={text => setSeller_email({value: text, error: ''})}
              error={!!seller_email.error}
              errorText={seller_email.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
          </View>
        </View>
        {typePost === 1 ? (
          <View>
            <Text style={[styles.textMtcb, {marginBottom: 12}]}>
              THÔNG TIN THANH TOÁN
            </Text>
            <View style={styles.itemBlockInput}>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTPTab2]}>
                <View>
                  <Text style={styles.textRightInfo}>Loại đăng tin</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text
                    style={[
                      styles.textRightInfo,
                      {color: '#999999', marginRight: 10},
                    ]}>
                    {objectAdver.name}
                  </Text>
                  <Image
                    source={require('../../assets/post/bxs-info-circle.png')}
                    style={{marginRight: 10, marginTop: 2}}
                  />
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
            </View>
            {/*<View style={styles.blockWn}>*/}
            {/*  <Text style={styles.textBlockWn}>*/}
            {/*    Đăng tin hiệu quả hơn với tính năng nổi bật cho gói tin.*/}
            {/*  </Text>*/}
            {/*  <View style={styles.blockChecked}>*/}
            {/*    <Checked*/}
            {/*      value={true}*/}
            {/*      checked={type}*/}
            {/*      onChange={() => {*/}
            {/*        setType(!type);*/}
            {/*      }}*/}
            {/*    />*/}
            {/*    <View>*/}
            {/*      <View style={styles.fleRow}>*/}
            {/*        <Text style={styles.textBuyChecked}>*/}
            {/*          Mua thêm Tính Năng Nổi Bật*/}
            {/*        </Text>*/}
            {/*        <Image*/}
            {/*          source={require('../../assets/post/bxs-info-circle.png')}*/}
            {/*          style={{marginTop: 2}}*/}
            {/*        />*/}
            {/*      </View>*/}
            {/*      <Text style={styles.bonusVND}>(+2,727 VNĐ/ngày)</Text>*/}
            {/*    </View>*/}
            {/*  </View>*/}
            {/*</View>*/}
            <View style={styles.blockInfoPost}>
              <Text style={styles.textRightInfo}>Đơn giá</Text>
              <Text style={styles.textRightInfo}>
                <FormatNumber value={objectAdver.price} currency=" VND/ngày" />
              </Text>
            </View>
            <CDatePicker
              label="Ngày bắt đầu"
              dateE={startDate}
              onChange={date => {
                setStartDate(date);
                handleTotalDate(endDate, date);
              }}
            />
            <CDatePicker
              label="Ngày kết thúc"
              dateE={endDate}
              onChange={date => {
                setEndDate(date);
                handleTotalDate(date, startDate);
              }}
            />
            <View style={styles.blockInfoPost}>
              <Text style={styles.textRightInfo}>Thời gian đăng tin</Text>
              <Text style={styles.textRightInfo}>{totalDays} ngày</Text>
            </View>
            <View style={styles.borderBottom} />
            <View>
              <Text style={styles.textHeaderBottom}>Thành tiền</Text>
              <View style={styles.blockInfoPost}>
                <Text style={styles.textRightInfo}>Phí đăng tin</Text>
                <Text style={styles.textRightInfo}>
                  <FormatNumber
                    value={objectAdver.price * totalDays}
                    currency={' VND'}
                  />
                </Text>
              </View>
              <View style={styles.blockInfoPost}>
                <Text style={styles.textRightInfo}>VAT(10%)</Text>
                <Text style={styles.textRightInfo}>
                  <FormatNumber
                    value={objectAdver.price * totalDays * 0.1}
                    currency={' VND'}
                  />
                </Text>
              </View>
              <View style={styles.blockInfoPost}>
                <Text style={styles.textRightInfoTC}>Tổng cộng</Text>
                <Text style={[styles.textRightInfoTC, {color: '#FF3B30'}]}>
                  <FormatNumber
                    value={
                      objectAdver.price * totalDays +
                      objectAdver.price * totalDays * 0.1
                    }
                    currency={' VND'}
                  />
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <View>
            <Text style={[styles.textMtcb, {marginBottom: 12}]}>
              Lịch đăng tin
            </Text>
            <CDatePicker
              label="Ngày bắt đầu"
              dateE={startDate}
              onChange={date => {
                setStartDate(date);
                handleTotalDate(endDate, date);
              }}
            />
            <CDatePicker
              label="Ngày kết thúc"
              dateE={endDate}
              onChange={date => {
                setEndDate(date);
                handleTotalDate(date, startDate);
              }}
            />
          </View>
        )}
      </Selection>
    );
  };
  const TabPost = stack => {
    switch (stack) {
      case 0:
        return TabOne();
      case 1:
        return TabTwo();
      case 2:
        return TabThree();
      default:
        return TabOne();
    }
  };

  const NextStep = item => {
    setCurrentPosition(item);
    if (item === 1) {
      setTitleHeader('Mô tả');
    } else if (item === 2) {
      setTitleHeader('Liên hệ và thanh toán');
    } else {
      setTitleHeader('Đăng tin rao');
    }
  };
  const backStep = item => {
    setCurrentPosition(
      item === '' || item === undefined
        ? navigation.navigate('Dashboard', {object: {}})
        : item === 0
        ? ''
        : item - 1,
    );
    if (item === 2) {
      setTitleHeader('Mô tả');
    } else {
      setTitleHeader('Đăng tin rao');
    }
    //reset data select hinh thuc
    if (item === 0) {
      resetData();
    }
  };
  const resetData = () => {
    setTypePost(1);
    setObjectForm({
      name: 'Tất cả',
      value: '',
      error: '',
    });
    setObjectCategories({
      name: 'Tất cả',
      id: '',
      error: '',
    });
    setObjectCity({
      name: 'Trên toàn quốc',
      id: '',
      error: '',
    });
    setObjectDistricts({
      name: 'Tất cả',
      id: '',
      error: '',
    });
    setObjectWards({
      name: 'Tất cả',
      id: '',
      error: '',
    });
    setObjectStreets({
      name: 'Tất cả',
      id: '',
      error: '',
    });
    setObjectProject({
      name: 'Tất cả',
      id: '',
      error: '',
    });
    setAddress({value: '', error: ''});
    setObjectPositionMap([
      {
        latitude: 21.0277644,
        longitude: 105.8341598,
      },
    ]);
    setTitle({value: '', error: ''});
    setArea({value: '', error: ''});
    setPrice({value: '', error: ''});
    setObjectPriceType({
      name: 'Đơn vị giá',
      value: '',
      error: '',
    });
    setListAvatar([]);
    setDescription({value: '', error: ''});
    setFloor_numbers({value: '', error: ''});
    setBedroom_numbers({value: '', error: ''});
    setToilet_numbers({value: '', error: ''});
    setWidth_numbers({value: '', error: ''});
    setLand_width({value: '', error: ''});
    setObjectDirection({
      name: 'Tất cả',
      value: '',
    });
    setObjectBaconly({
      name: 'Tất cả',
      value: '',
    });
    setFurniture({value: '', error: ''});
    setLegality({value: '', error: ''});
    setSeller_name({value: '', error: ''});
    setSeller_address({value: '', error: ''});
    setSeller_phone_number({value: '', error: ''});
    setSeller_email({value: '', error: ''});
    setObjectAdver({
      name: 'Tất cả',
      id: '',
      error: '',
    });
    setStartDate(new Date());
    setEndDate(moment(new Date()).day(+30));
    setTotalDays(0);
    setObjectPriceConditions({
      name: 'Chọn mức giá',
      id: '',
      error: '',
    });
    setObjectAreaConditions({
      name: 'Chọn diện tích',
      id: '',
      error: '',
    });
  };

  return (
    <Container style={{flex: 1}}>
      <ModalProcessV2
        typeButton={typeButtonPost}
        visible={modalProcess}
        title={titleModal}
        content={contentModal}
        id={idPost}
        onChange={item => setCloseModal(item)}
        titleOk={titleButtonPostOk}
        titleBack={'kết thúc'}
      />
      <View style={styles.wrapHeader}>
        <View style={styles.titleHeaderCenter}>
          <View style={styles.blockContentHeader}>
            <TouchableOpacity
              onPress={() => {
                backStep(currentPosition);
              }}>
              <Image source={require('../../assets/back.png')} />
            </TouchableOpacity>
            <Text style={styles.titleHeader}>{titleHeader}</Text>
            <View />
          </View>
        </View>
      </View>
      {currentPosition === '' || currentPosition == undefined ? (
        <ScrollView>
          <SafeAreaView style={styles.container}>
            <View style={[styles.blockPostSelect, {height: height / 2}]}>
              <Image source={require('../../assets/post/Group2148.png')} />
              <Text style={styles.textBBSNSelect}>Bán bất động sản</Text>
              <Button
                titleStyle={styles.buttonTitleSelect}
                style={styles.buttonLoginSelect}
                type="blue"
                title="Cần bán/Cho thuê"
                mode="contained"
                onPress={() => {
                  setTypePost(1);
                  NextStep(0);
                }}
              />
            </View>
            <View style={[styles.blockPostBottomSelect, {height: height / 2}]}>
              <Image source={require('../../assets/post/Group2153.png')} />
              <Text style={styles.textBBSNSelect}>Cho thuê bất động sản</Text>
              <Button
                titleStyle={styles.buttonTitleSelect}
                style={styles.buttonLoginSelect}
                type="blue"
                title="Cần mua/Cần thuê"
                mode="contained"
                onPress={() => {
                  setTypePost(2);
                  NextStep(0);
                }}
              />
            </View>
          </SafeAreaView>
        </ScrollView>
      ) : (
        <View style={{flex: 1}}>
          <ScrollView>
            <SafeAreaView style={styles.container}>
              <View style={[styles.blockPost, {width: width / 2}]}>
                <StepIndicator
                  stepCount={3}
                  customStyles={customStyles}
                  currentPosition={currentPosition}
                />
              </View>
              {TabPost(currentPosition)}
            </SafeAreaView>
          </ScrollView>
          {currentPosition !== 2 ? (
            <Selection
              size={16}
              style={{
                borderTopWidth: 1,
                borderTopColor: '#E6E6E6',
                paddingTop: 12,
              }}>
              <Button
                titleStyle={styles.buttonTitle}
                style={styles.buttonLogin}
                type="blue"
                title="Tiếp tục"
                mode="contained"
                onPress={() => {
                  onValidate();
                }}
              />
            </Selection>
          ) : (
            // <View style={styles.buttonBottom}>
            //   <TouchableOpacity
            //     onPress={() => {
            //       handleDataPreview();
            //     }}
            //     style={[
            //       styles.pdBottom,
            //       {borderRightWidth: 1, borderRightColor: '#ffffff'},
            //     ]}>
            //     <Text style={styles.textBottom}>Xem trước tin đăng</Text>
            //   </TouchableOpacity>
            //   <TouchableOpacity
            //     onPress={() => onValidate()}
            //     style={styles.pdBottom}>
            //     <Text style={styles.textBottom}>Đăng tin</Text>
            //   </TouchableOpacity>
            // </View>
            <Selection size={16} style={{marginTop: 16}}>
              <Button
                titleStyle={styles.buttonTitle}
                style={styles.buttonLogin}
                type="blue"
                title="Đăng tin"
                mode="contained"
                loading={loading}
                disabled={loading}
                onPress={() => onValidate()}
              />
            </Selection>
          )}
        </View>
      )}
      <PreviewPost
        visible={popupPreview}
        data={objectPreview}
        onChange={item => setCloseSelectPreview(item)}
        header={'Xem trước tin đăng'}
        navigation={navigation}
      />
      <Categories
        visible={popupCategories}
        data={objectCategories}
        onChange={item => setCloseSelectCategories(item)}
        header={'Loại bất động sản'}
      />
      <ListCity
        visible={popupCity}
        data={objectCity}
        onChange={item => setCloseSelectCity(item)}
        header={'Tỉnh/Thành phố'}
      />
      <ListDistricts
        visible={popupDistricts}
        data={objectDistricts}
        keyFilter={objectCity.id}
        onChange={item => setCloseSelectDistricts(item)}
        header={'Quận/Huyện'}
      />
      <ListWards
        visible={popupWards}
        data={objectWards}
        keyFilterCity={objectCity.id}
        keyFilterDisctrict={objectDistricts.id}
        onChange={item => setCloseSelectWards(item)}
        header={'Phường/Xã'}
      />
      <ListStreets
        visible={popupStreets}
        data={objectStreets}
        keyFilterCity={objectCity.id}
        keyFilterDisctrict={objectDistricts.id}
        onChange={item => setCloseSelectStreets(item)}
        header={'Đường/Phố'}
      />
      <ListProject
        visible={popupProject}
        data={objectProject}
        onChange={item => setCloseSelectProject(item)}
        keyFilterProject={objectCity.id}
        header={'Dự án'}
      />
      <ListPriceType
        visible={popupPriceType}
        data={objectPriceType}
        onChange={item => setCloseSelectPriceType(item)}
        keyFilterProject={objectForm.value}
        header={'Đơn vị giá'}
      />
      <ListPriceConditions
        visible={popupPriceConditions}
        data={objectPriceConditions}
        onChange={item => setCloseSelectPriceConditions(item)}
        keyFilterProject={objectForm.value}
        header={'Giá'}
      />
      <ListAreaConditions
        visible={popupAreaConditions}
        data={objectAreaConditions}
        onChange={item => setCloseSelectAreaConditions(item)}
        header={'Diện tích'}
      />
      <TypePostType
        visible={popupListAdver}
        data={objectAdver}
        onChange={item => setCloseSelect(item)}
        header={'Loại đăng tin'}
      />
      <ListForm
        visible={popupListForm}
        data={objectForm}
        onChange={item => setCloseSelectForm(item)}
        keyFilterProject={typePost}
        header={'Hình thức'}
      />
      <ListDirection
        visible={popupListDirection}
        data={objectDirection}
        onChange={item => setCloseSelectDirection(item)}
        header={'Hướng nhà'}
      />
      <ListBaconly
        visible={popupListBaconly}
        data={objectBaconly}
        onChange={item => setCloseSelectBaconly(item)}
        header={'Hướng ban công'}
      />
      <PositionMaps
        visible={popupPositionMaps}
        data={objectPositionMap}
        onChange={item => setCloseMaps(item)}
      />
      <CustomizePhotos
        visible={popupCustomizePhoto}
        data={listAvatar}
        onChange={item => setCloseSelectCustomizePhoto(item)}
        header={'Tùy chỉnh ảnh'}
      />
    </Container>
  );
}
