import React, {useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/DetailDeposit/styles';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import Header from '../../../components/Header';
import Button from '../../../components/Button';
import TextInput from '../../../components/TextInput';
import ListBank from '../../../components/ListBank/ListBank';
import {WalletModel} from '../../../models/wallet';
import {amountValidator} from '../../../helpers/amountValidator';
import ModalProcess from '../../../components/ListModal/ModalProcess';
import {useFocusEffect} from '@react-navigation/native';
import {DashboardModel} from '../../../models/dashboard';

export default function DetailDeposit({route, navigation}) {
  const {objectMethod} = route?.params;
  const [amount, setAmount] = useState({value: '', error: ''});
  //Danh sach ngan hang
  const [loading, setLoading] = useState(false);
  const [popupBank, setPopupBank] = useState(false);
  const [objectBank, setObjectBank] = useState('');
  const [dataDeposit, setDataDeposit] = useState('');
  const setCloseSelectBank = item => {
    setObjectBank(item);
    setPopupBank(false);
  };
  //modal
  const [modalProcess, setModalProcess] = useState(false);
  const [titleModal, setTitleModal] = useState('');
  const [infoPayHome, setInfoPayHome] = useState('');
  const [contentModal, setContentModal] = useState('');
  const setCloseModal = item => {
    setModalProcess(false);
  };
  const onSubmitDeposit = () => {
    const amountError = amountValidator(amount.value);
    setAmount({...amount, error: amountError});
    if (objectBank === '') {
      setModalProcess(true);
      setTitleModal('Thông báo');
      setContentModal('Chọn ngân hàng thanh toán');
    }
    if (!amountError && objectBank !== '') {
      setLoading(true);
      WalletModel.deposit_by_transfer_bank(objectBank.id, amount.value)
        .then(data => {
          setLoading(false);
          if (data.status == 1) {
            setDataDeposit(data.data);
            setAmount({value: '', error: ''});
            setObjectBank('');
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          } else {
            setModalProcess(true);
            setTitleModal('Thông báo');
            setContentModal(data.message);
          }
        })
        .catch(e => {
          console.log(e);
        });
    }
  };
  const TabOne = () => {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Selection size={16}>
            <View style={styles.blockText}>
              <Text style={styles.textLeftInfo}>Chọn ngân hàng thanh toán</Text>
            </View>
            <View>
              <Text style={styles.textDes}>
                Lưu ý: Để thực hiện thanh toán, thẻ ATM nội địa của Quý khách
                cần được đăng ký sử dụng dịch vụ Internet Banking. Quý khách sẽ
                được chuyển hướng về cổng thanh toán để hoàn tất giao dịch.
              </Text>
              <TouchableOpacity
                onPress={() => {
                  setPopupBank(true);
                }}
                style={styles.blockSelectNh}>
                <Text style={styles.textSelectNh}>
                  {objectBank?.bank_name || 'Chọn ngân hàng*'}
                </Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vectorNh}
                />
              </TouchableOpacity>
              <View style={styles.itemQlTK}>
                <View>
                  <TextInput
                    style={styles.inputLogin}
                    placeholder="Số tiền muốn nạp (gồm VAT)*"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                </View>
                <Text style={styles.textTt}>(Tối thiểu nạp 10.000 VNĐ)</Text>
              </View>
              <View style={styles.itemQlTKN}>
                <View>
                  <TextInput
                    style={styles.inputLogin}
                    placeholder="Nội dung giao dịch*"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                </View>
              </View>
              <View style={[styles.itemQlTKMXH, {marginBottom: 12}]}>
                <View>
                  <Text style={styles.textLeftInfoInput}>Tên khách hàng*</Text>
                  <TextInput
                    style={[styles.inputDes]}
                    placeholder="Username"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                </View>
              </View>
              <View style={[styles.itemQlTKMXH, {marginBottom: 12}]}>
                <View>
                  <Text style={styles.textLeftInfoInput}>Email*</Text>
                  <TextInput
                    style={[styles.inputDes]}
                    placeholder="username01@gmail.com"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                </View>
              </View>
              <View style={[styles.itemQlTKMXH, {marginBottom: 12}]}>
                <View>
                  <Text style={styles.textLeftInfoInput}>Số điện thoại*</Text>
                  <TextInput
                    style={[styles.inputDes]}
                    placeholder="0912310691"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                </View>
              </View>
            </View>
          </Selection>
        </ScrollView>
        <Selection size={16} style={{marginTop: 16}}>
          <Button
            titleStyle={styles.buttonTitle}
            style={styles.buttonLogin}
            type="blue"
            title="Tiếp tục"
            mode="contained"
            // loading={loading}
            // disabled={loading}
            // onPress={() => {
            //   onSubmitDeposit();
            // }}
          />
        </Selection>
      </SafeAreaView>
    );
  };
  const TabBank_transfer = () => {
    return (
      <SafeAreaView style={styles.container}>
        <ModalProcess
          visible={modalProcess}
          title={titleModal}
          content={contentModal}
          onChange={item => setCloseModal(item)}
        />
        <ScrollView>
          <Selection size={16} style={{marginBottom: 20}}>
            <View style={styles.blockTextTransfer}>
              <Text style={styles.textLeftInfo}>
                1. Chọn ngân hàng thanh toán
              </Text>
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  setPopupBank(true);
                }}
                style={styles.blockSelectNh}>
                <Text style={styles.textSelectNh}>
                  {objectBank?.bank_name || 'Chọn ngân hàng*'}
                </Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vectorNh}
                />
              </TouchableOpacity>
              <View style={styles.itemQlTKTransfer}>
                <View>
                  <TextInput
                    style={styles.inputLogin}
                    placeholder="Số tiền muốn nạp (gồm VAT)*"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    value={amount.value}
                    onChangeText={text => setAmount({value: text, error: ''})}
                    error={!!amount.error}
                    errorText={amount.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                </View>
                <Text style={styles.textTt}>(Tối thiểu nạp 10.000 VNĐ)</Text>
                <Button
                  titleStyle={styles.buttonTitle}
                  style={styles.buttonLogin}
                  type="blue"
                  title="Chuyển khoản"
                  mode="contained"
                  loading={loading}
                  disabled={loading}
                  onPress={() => {
                    onSubmitDeposit();
                  }}
                />
              </View>
              {dataDeposit !== '' ? (
                <View>
                  <View style={styles.blockTextTransfer2}>
                    <Text style={styles.textLeftInfo}>
                      2.Thông tin chuyển khoản
                    </Text>
                  </View>
                  <View style={styles.blockInfoTab2}>
                    <View style={styles.itemLeftInfo}>
                      <Text style={styles.textNameNh}>Tên ngân hàng</Text>
                    </View>
                    <View style={styles.itemRightInfo}>
                      <Text style={styles.nameNhRight}>
                        {dataDeposit.bank_name}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.blockInfoTab2}>
                    <View style={styles.itemLeftInfo}>
                      <Text style={styles.textNameNh}>Chi nhánh</Text>
                    </View>
                    <View style={styles.itemRightInfo}>
                      <Text style={styles.nameNhRight}>
                        {dataDeposit.bank_address}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.blockInfoTab2}>
                    <View style={styles.itemLeftInfo}>
                      <Text style={styles.textNameNh}>Người nhận</Text>
                    </View>
                    <View style={styles.itemRightInfo}>
                      <Text style={styles.nameNhRight}>
                        {dataDeposit.bank_owner}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.blockInfoTab2}>
                    <View style={styles.itemLeftInfo}>
                      <Text style={styles.textNameNh}>Số tài khoản:</Text>
                    </View>
                    <View style={styles.itemRightInfo}>
                      <Text style={styles.nameNhRightSTk}>
                        {dataDeposit.bank_number}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.blockInfoTab2}>
                    <View style={[styles.itemLeftInfo, {borderBottomWidth: 1}]}>
                      <Text style={styles.textNameNh}>
                        Nội dung chuyển khoản:
                      </Text>
                    </View>
                    <View
                      style={[styles.itemRightInfo, {borderBottomWidth: 1}]}>
                      <Text style={styles.nameNhRight}>
                        {dataDeposit.content_transfer}
                      </Text>
                    </View>
                  </View>
                  <Text style={styles.desInfo}>
                    Lưu ý:{' '}
                    <Text style={styles.contentTrf}>
                      {dataDeposit.content_transfer}
                    </Text>{' '}
                    là mã chuyển khoản của riêng bạn. Hệ thống của chúng tôi sẽ
                    căn cứ trên mã này để tự động xử lý giao dịch.{' '}
                    <Text style={styles.textNtf}>
                      Bạn vui lòng nhập đúng mã {dataDeposit.content_transfer}
                    </Text>{' '}
                    ở đầu nội dung chuyển khoản để việc xác nhận giao dịch được
                    nhanh chóng và chính xác.
                  </Text>
                </View>
              ) : null}
            </View>
          </Selection>
        </ScrollView>
      </SafeAreaView>
    );
  };
  const TabBank_at_home = () => {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Selection size={16} style={{marginBottom: 20}}>
            <View style={styles.blockTextAtHome}>
              <Text style={styles.textLeftInfo}>Trụ sở chính tại Hà Nội</Text>
            </View>
            <Text style={styles.textAdAtHome}>
              <Text style={styles.addAt}>Địa chỉ:</Text> {infoPayHome.address}
            </Text>
            <Text style={styles.textAdAtHome}>
              <Text style={styles.addAt}>Điện thoại:</Text> {infoPayHome.phone}
            </Text>
          </Selection>
        </ScrollView>
      </SafeAreaView>
    );
  };
  const TabPost = stack => {
    switch (stack) {
      case 'bank_transfer':
        return TabBank_transfer();
      case 'pay_at_home':
        return TabBank_at_home();
      case 1:
        return TabOne();
      default:
        return TabOne();
    }
  };
  const getPaydirectHomeInfo = () => {
    WalletModel.payDirectAtHomeInfo()
      .then(data => {
        if (data.status == 1) {
          setInfoPayHome(data.data);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  useFocusEffect(
    React.useCallback(() => {
      if (objectMethod.key === 'pay_at_home') {
        getPaydirectHomeInfo();
      }
    }, [objectMethod]),
  );
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader={objectMethod.name} navigation={navigation} />
      {TabPost(objectMethod.key)}
      <ListBank
        visible={popupBank}
        data={objectBank}
        onChange={item => setCloseSelectBank(item)}
        header={'Chọn ngân hàng'}
      />
    </Container>
  );
}
