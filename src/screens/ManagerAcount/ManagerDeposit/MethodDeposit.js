import React, {useState, useEffect} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/MethodDeposit/styles';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import Header from '../../../components/Header';
import Button from '../../../components/Button';
import {WalletModel} from '../../../models/wallet';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import LoadingSkeleton from '../../../components/LoadingSkeleton';
const {width, height} = Dimensions.get('screen');

export default function MethodDeposit({navigation}) {
  const [method, setMethod] = useState('');
  const [listData, setListData] = useState('');
  const [loading, setLoading] = useState(true);
  const getListData = () => {
    WalletModel.list_deposit_method()
      .then(data => {
        if (data.status == 1) {
          setListData(data.data);
          setLoading(false);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  useEffect(() => {
    getListData();
    setLoading(true);
  }, []);
  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        setMethod(item);
      }}>
      <View
        style={[
          styles.listItemHistory,
          item.key === method.key
            ? {borderColor: '#004182'}
            : {borderColor: '#E6E6E6'},
        ]}>
        <Image
          source={
            item.key === 'bank_transfer'
              ? require('../../../assets/Acount/MethodDeposit/Group2336.png')
              : require('../../../assets/Acount/MethodDeposit/flaticon_1052814.png')
          }
        />
        <View style={styles.blockInfo}>
          <Text style={styles.textItemBLock}>{item.name}</Text>
          <Text style={styles.textAcount}>{item.des}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Nạp tiền" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <Selection size={16}>
          <View style={styles.blockText}>
            <Text style={styles.textLeftInfo}>Chọn phương thức thanh toán</Text>
          </View>
          <LoadingSkeleton
            loading={loading}
            count={6}
            skeleton={
              <SkeletonPlaceholder>
                <View
                  style={{
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginBottom: 10,
                  }}>
                  <View
                    style={{width: width - 36, height: 87, borderRadius: 8}}
                  />
                </View>
              </SkeletonPlaceholder>
            }>
            <FlatList
              style={{marginBottom: 100}}
              data={listData}
              renderItem={renderItem}
            />
          </LoadingSkeleton>
        </Selection>
      </SafeAreaView>
      <Selection size={16}>
        <Button
          titleStyle={styles.buttonTitle}
          style={styles.buttonLogin}
          type="blue"
          title="Tiếp tục"
          mode="contained"
          onPress={() => {
            navigation.navigate('DetailDeposit', {objectMethod: method});
          }}
        />
      </Selection>
    </Container>
  );
}
