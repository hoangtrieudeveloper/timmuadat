import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Alert,
  AsyncStorage,
  FlatList,
} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../../components/Button';
import {styles} from '../../../core/Acount/updateinformation/styles';
import Header from '../../../components/Header';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import TextInput from '../../../components/TextInput';
import Option from '../../../components/Option';
import PopupListCountry from '../../../components/ListPopup/ListCountry';
import ModalProcess from '../../../components/ListModal/ModalProcess';
import AddPhoneNumber from './AddPhoneNumber';

export default function UpdateInformation({navigation}) {
  const [phone, setPhone] = useState({value: '', error: ''});
  const [listPhone, setListPhone] = useState([{}]);
  const [type, setType] = useState(2);

  const [popupListCountry, setPopupListCountry] = useState(false);
  const [modalProcess, setModalProcess] = useState(false);
  const [from_country, setFrom_country] = useState('');
  const [from_country_name, setFrom_country_name] = useState('');
  const setCloseCountry = item => {
    setFrom_country(item.value);
    setFrom_country_name(item.name);
    setPopupListCountry(false);
  };
  const setCloseModal = item => {
    setModalProcess(false);
  };

  const renderItem = ({item}) => (
    <View style={styles.itemQlTK}>
      <Text style={styles.textLeftInfo}>Số điện thoại chính*</Text>
      <View>
        <TextInput
          style={[
            styles.inputLogin,
            {
              paddingRight: 20,
            },
          ]}
          placeholder="Nhập số điện thoại chính"
          placeholderTextColor="#999999"
          returnKeyType="next"
          value={phone.value}
          onChangeText={text => setPhone({value: text, error: ''})}
          error={!!phone.error}
          errorText={phone.error}
          theme={{colors: {primary: '#999999', text: '#000000'}}}
          autoCapitalize="none"
          marginVerNone={0}
        />
        {phone.value !== '' ? (
          <TouchableOpacity
            onPress={() => {
              setModalProcess(true);
            }}
            style={styles.clearPhone}>
            <Image
              source={require('../../../assets/Acount/updateinformation/ClearGlyph.png')}
            />
          </TouchableOpacity>
        ) : (
          <View />
        )}
      </View>
    </View>
  );

  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Sửa thông tin tài khoản" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ModalProcess
          visible={modalProcess}
          title={'Xác nhận xóa số điện thoại'}
          content={'bạn có chắc chắn muốn xóa số diện thoại này không?'}
          typeButton={2}
          onChange={item => setCloseModal(item)}
        />
        <ScrollView>
          <Selection size={16} style={styles.blockTitle}>
            <View style={styles.infoUser}>
              <Image
                source={require('../../../assets/ic_authorize/icon_login/MaskGroup309.png')}
              />
              <TouchableOpacity style={styles.blockIconUpdate}>
                <Image
                  source={require('../../../assets/Acount/updateinformation/bx-camera.png')}
                />
              </TouchableOpacity>
            </View>
          </Selection>
          <Selection size={16}>
            <View style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Thông tin cá nhân</Text>
            </View>
            <View style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Họ và tên*</Text>
              <View>
                <TextInput
                  style={styles.inputLogin}
                  placeholder="Nhập họ và tên"
                  placeholderTextColor="#999999"
                  returnKeyType="next"
                  // value={fullname.value}
                  // onChangeText={text => setFullname({value: text, error: ''})}
                  // error={!!fullname.error}
                  // errorText={fullname.error}
                  theme={{colors: {primary: '#999999', text: '#000000'}}}
                  autoCapitalize="none"
                  marginVerNone={0}
                />
              </View>
            </View>
            <View style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Ngày sinh</Text>
              <View>
                <TextInput
                  style={styles.inputLogin}
                  placeholder="Nhập ngày sinh"
                  placeholderTextColor="#999999"
                  returnKeyType="next"
                  // value={fullname.value}
                  // onChangeText={text => setFullname({value: text, error: ''})}
                  // error={!!fullname.error}
                  // errorText={fullname.error}
                  theme={{colors: {primary: '#999999', text: '#000000'}}}
                  autoCapitalize="none"
                  marginVerNone={0}
                />
              </View>
            </View>
            <View style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Giới tính</Text>
              <View
                style={{flexDirection: 'row', marginTop: 13, marginBottom: 3}}>
                <Option
                  label={'Nam'}
                  value={1}
                  checked={type}
                  onChange={() => {
                    setType(1);
                  }}
                  style={{marginRight: 24}}
                />
                <Option
                  label={'Nữ'}
                  value={2}
                  checked={type}
                  onChange={() => {
                    setType(2);
                  }}
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[
                styles.itemQlTKTP,
                from_country_name !== '' ? {paddingBottom: 10} : {},
              ]}>
              <View>
                <Text style={styles.textLeftInfo}>Tỉnh/Thành Phố*</Text>
                <Text>{from_country_name}</Text>
              </View>
              <Image
                source={require('../../../assets/Vector.png')}
                style={styles.vecterNext}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[
                styles.itemQlTKTP,
                from_country_name !== '' ? {paddingBottom: 10} : {},
              ]}>
              <View>
                <Text style={styles.textLeftInfo}>Quận/Huyện*</Text>
                <Text>{from_country_name}</Text>
              </View>
              <Image
                source={require('../../../assets/Vector.png')}
                style={styles.vecterNext}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[
                styles.itemQlTKTP,
                from_country_name !== '' ? {paddingBottom: 10} : {},
              ]}>
              <View>
                <Text style={styles.textLeftInfo}>Phường/Xã</Text>
                <Text>{from_country_name}</Text>
              </View>
              <Image
                source={require('../../../assets/Vector.png')}
                style={styles.vecterNext}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[
                styles.itemQlTKTP,
                from_country_name !== '' ? {paddingBottom: 10} : {},
              ]}>
              <View>
                <Text style={styles.textLeftInfo}>Đường/Phố</Text>
                <Text>{from_country_name}</Text>
              </View>
              <Image
                source={require('../../../assets/Vector.png')}
                style={styles.vecterNext}
              />
            </TouchableOpacity>
            <View style={[styles.blockTitleQLTK, {borderTopWidth: 0}]}>
              <Text style={styles.titleQLTK}>Thông tin liên hệ</Text>
            </View>
            <FlatList data={listPhone} renderItem={renderItem} />
            <View style={styles.blockAddPhone}>
              <Image
                source={require('../../../assets/Acount/updateinformation/Symbol.png')}
              />
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('AddPhoneNumber');
                }}>
                <Text style={styles.textAddPhone}>
                  Thêm số điện thoại đăng tin
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Email*</Text>
              <View>
                <TextInput
                  style={styles.inputLogin}
                  placeholder="Nhập Email"
                  placeholderTextColor="#999999"
                  returnKeyType="next"
                  // value={fullname.value}
                  // onChangeText={text => setFullname({value: text, error: ''})}
                  // error={!!fullname.error}
                  // errorText={fullname.error}
                  theme={{colors: {primary: '#999999', text: '#000000'}}}
                  autoCapitalize="none"
                  marginVerNone={0}
                />
              </View>
            </View>
            <View style={styles.itemQlTKMXH}>
              <View>
                <TextInput
                  style={[styles.inputLogin, {paddingBottom: 10}]}
                  placeholder="Mã số thuế/CMND"
                  placeholderTextColor="#999999"
                  returnKeyType="next"
                  // value={fullname.value}
                  // onChangeText={text => setFullname({value: text, error: ''})}
                  // error={!!fullname.error}
                  // errorText={fullname.error}
                  theme={{colors: {primary: '#999999', text: '#000000'}}}
                  autoCapitalize="none"
                  marginVerNone={0}
                />
              </View>
            </View>
            <View style={styles.itemQlTKMXH}>
              <View>
                <TextInput
                  style={[
                    styles.inputLogin,
                    {paddingTop: 10, paddingBottom: 10},
                  ]}
                  placeholder="Facebook"
                  placeholderTextColor="#999999"
                  returnKeyType="next"
                  // value={fullname.value}
                  // onChangeText={text => setFullname({value: text, error: ''})}
                  // error={!!fullname.error}
                  // errorText={fullname.error}
                  theme={{colors: {primary: '#999999', text: '#000000'}}}
                  autoCapitalize="none"
                  marginVerNone={0}
                />
              </View>
            </View>
            <View style={styles.itemQlTKMXH}>
              <View>
                <TextInput
                  style={[
                    styles.inputLogin,
                    {paddingTop: 10, paddingBottom: 10},
                  ]}
                  placeholder="Twitter"
                  placeholderTextColor="#999999"
                  returnKeyType="next"
                  // value={fullname.value}
                  // onChangeText={text => setFullname({value: text, error: ''})}
                  // error={!!fullname.error}
                  // errorText={fullname.error}
                  theme={{colors: {primary: '#999999', text: '#000000'}}}
                  autoCapitalize="none"
                  marginVerNone={0}
                />
              </View>
            </View>
            <View style={styles.itemQlTKMXH}>
              <View>
                <TextInput
                  style={[
                    styles.inputLogin,
                    {paddingTop: 10, paddingBottom: 10},
                  ]}
                  placeholder="Zalo"
                  placeholderTextColor="#999999"
                  returnKeyType="next"
                  // value={fullname.value}
                  // onChangeText={text => setFullname({value: text, error: ''})}
                  // error={!!fullname.error}
                  // errorText={fullname.error}
                  theme={{colors: {primary: '#999999', text: '#000000'}}}
                  autoCapitalize="none"
                  marginVerNone={0}
                />
              </View>
            </View>
          </Selection>
        </ScrollView>
      </SafeAreaView>
      <Selection size={16} style={styles.mrt16}>
        <Button
          titleStyle={styles.buttonTitle}
          style={styles.buttonLogin}
          type="blue"
          title="Lưu"
          mode="contained"
          // onPress={() => {
          //   onSubmitDeposit();
          // }}
        />
      </Selection>
      <PopupListCountry
        visible={popupListCountry}
        data={from_country}
        onChange={item => setCloseCountry(item)}
      />
    </Container>
  );
}
