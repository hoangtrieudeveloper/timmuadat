import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
} from 'react-native';
import {styles} from '../../../core/Acount/DetailPromotion/styles';
import {Container} from 'native-base';
import Search from '../../Dashboard/Search';
import Selection from '../../../components/Selection';
import Button from '../../../components/Button';
const {width, height} = Dimensions.get('screen');
export default function DetailPromotion({navigation}) {
  return (
    <Container style={{flex: 1}}>
      <View style={styles.wrapHeader}>
        <View style={styles.titleHeaderCenter}>
          <View style={styles.blockContentHeader}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <Image source={require('../../../assets/back.png')} />
            </TouchableOpacity>
            <Text style={styles.titleHeader}>Khuyến mãi</Text>
            <TouchableOpacity>
              <Image
                source={require('../../../assets/Acount/draftNews/trash.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Selection size={16}>
            <View style={styles.blockContent}>
              <Text style={styles.timeToFrom}>Từ 06/10/2015 - 31/10/2027</Text>
              <Text style={styles.titleSort}>Khuyến mãi 2</Text>
              <Text style={styles.titleContent}>
                Tặng 10% tiền tin đăng cho tin VIP đăng tối thiểu 1 tháng vào
                tài khoả...
              </Text>
              <Text style={styles.textContent}>
                Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào
                việc trình bày và dàn trang phục vụ cho in ấn. Lorem Ipsum đã
                được sử dụng như một văn bản chuẩn cho ngành công nghiệp in ấn
                từ những năm 1500, khi một họa sĩ vô danh ghép nhiều đoạn văn
                bản với nhau để tạo thành một bản mẫu văn bản. Đoạn văn bản này
                không những đã tồn tại năm thế kỉ, mà khi được áp dụng vào tin
                học văn phòng, nội dung của nó vẫn không hề bị thay đổi. Nó đã
                được phổ biến trong những năm 1960 nhờ việc bán những bản giấy
                Letraset in những đoạn Lorem Ipsum, và gần đây hơn, được sử dụng
                trong các ứng dụng dàn trang, như Aldus PageMaker.
              </Text>
            </View>
          </Selection>
        </ScrollView>
      </SafeAreaView>
      <Selection size={16} style={styles.mrt16}>
        <Button
          titleStyle={styles.buttonTitle}
          style={styles.buttonLogin}
          type="blue"
          title="Đăng tin ngay"
          mode="contained"
        />
      </Selection>
    </Container>
  );
}
