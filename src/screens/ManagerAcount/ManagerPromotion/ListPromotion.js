import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import {styles} from '../../../core/Acount/ListPromotion/styles';
import Header from '../../../components/Header';
import {Container} from 'native-base';
import {TabView} from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import DraftNews from './../ManagerPost/DraftNews';
import AllPromotion from './AllPromotion';

export default function ListPromotion({navigation}) {
  const _handleIndexChange = i => setIndex(i);
  const [index, setIndex] = React.useState(0);
  const renderScene = ({route}) => {
    switch (route.key) {
      case '1':
        return <AllPromotion navigation={navigation} />;
      case '2':
        return <AllPromotion navigation={navigation} />;
      case '3':
        return <AllPromotion navigation={navigation} />;
      default:
        return null;
    }
  };
  const [routes] = React.useState([
    {key: '1', title: 'Tất cả'},
    {key: '2', title: 'Còn hiệu lực'},
    {key: '3', title: 'Đã hết hạn'},
  ]);
  const _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={[styles.tabBar]}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={[
                styles.tabItem,
                index === i ? {backgroundColor: '#004182'} : {},
              ]}
              onPress={() => setIndex(i)}>
              <Animated.Text
                style={[
                  styles.titleTab,
                  index === i ? {color: '#ffffff'} : {color: '#000000'},
                ]}>
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Khuyến mãi" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <TabView
          swipeEnabled={false}
          navigationState={{index, routes}}
          renderScene={renderScene}
          renderTabBar={_renderTabBar}
          onIndexChange={_handleIndexChange}
        />
      </SafeAreaView>
    </Container>
  );
}
