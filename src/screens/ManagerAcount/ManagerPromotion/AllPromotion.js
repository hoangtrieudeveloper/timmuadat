import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/AllPromotion/styles';
import Selection from '../../../components/Selection';
import Swipeable from 'react-native-swipeable';
import Const from '../../../components/Const';

export default function AllPromotion({navigation}) {
  const [listData, setListData] = useState([
    {status: 1},
    {status: 1},
    {status: 1},
    {status: 2},
    {status: 2},
  ]);
  const textStatus = item => {
    if (item === 2) {
      return <Text style={{color: Const.GREEN}}>Đã duyệt</Text>;
    } else if (item === 3) {
      return <Text style={{color: Const.RED_v2}}>Không duyệt</Text>;
    } else {
      return <Text>Chưa duyệt</Text>;
    }
  };
  const rightButtons = [
    <TouchableOpacity style={styles.StatusSwipeableRight}>
      <Image
        source={require('../../../assets/Acount/rentalNews/bx-trash.png')}
      />
      <Text style={styles.textStatus}>Xóa</Text>
    </TouchableOpacity>,
  ];
  const renderItem = ({item}) => (
    <Swipeable rightButtons={rightButtons}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('DetailPromotion');
        }}
        style={styles.blockItem}>
        <View style={styles.imageItem}>
          <Image
            source={require('../../../assets/Acount/ManagerPromotion/bx-purchase-tag-alt.png')}
          />
        </View>
        <View style={styles.rightItem}>
          <View style={styles.titleImage}>
            <Text
              style={
                item.status === 1
                  ? styles.titleRightItemSort
                  : styles.titleRightItemSortSee
              }>
              Khuyến mại 1
            </Text>
            {item.status === 1 ? <View style={styles.border50} /> : <View />}
          </View>
          <Text
            style={
              item.status === 1
                ? styles.titleRightItem
                : styles.titleRightItemSee
            }>
            Tổng hợp các căn hộ cần chuyển nhượng giá gốc Vinhomes Ocean Park,
            liên...
          </Text>
          <Text style={styles.PriceInfo}>Từ 06/10/2015 - 31/10/2027</Text>
        </View>
      </TouchableOpacity>
    </Swipeable>
  );

  return (
    <SafeAreaView style={styles.container}>
      <Selection size={16}>
        <View style={styles.totalNews}>
          <Text style={styles.textTotal}>Có 5 khuyến mại</Text>
        </View>
      </Selection>
      <FlatList
        style={{marginBottom: 10}}
        data={listData}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
}
