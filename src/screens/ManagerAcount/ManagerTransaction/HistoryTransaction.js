import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/HistoryTransaction/styles';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import FilterHistoryTransaction from './FilterHistoryTransaction';
import {WalletModel} from '../../../models/wallet';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import LoadingSkeleton from '../../../components/LoadingSkeleton';
import moment from 'moment';
const {width, height} = Dimensions.get('screen');
export default function HistoryTransaction({navigation}) {
  const [startDate, setStartDate] = useState(
    moment(new Date()).day(-7).format('YYYY/MM/DD'),
  );
  const [endDate, setEndDate] = useState(
    moment(new Date()).format('YYYY/MM/DD'),
  );
  const [loading, setLoading] = useState(true);
  const [listData, setListData] = useState([]);
  const [listDataStatus, setListDataStatus] = useState([]);
  //filter
  const [popupListFilter, setPopupListFilter] = useState(false);
  const [status, setStatus] = useState({
    name: 'Tất cả',
    value: '',
  });
  const setCloseSelect = item => {
    setStartDate(moment(item.startDateData).format('YYYY/MM/DD'));
    setEndDate(moment(item.endDateData).format('YYYY/MM/DD'));
    setStatus(item.objectData);
    getListData(
      moment(item.startDateData).format('YYYY/MM/DD'),
      moment(item.endDateData).format('YYYY/MM/DD'),
      item.objectData.value,
    );
    setPopupListFilter(false);
  };
  const renderItem = ({item}) => (
    <View style={styles.listItemHistory}>
      <View style={styles.blockAcount}>
        {/*<Text style={styles.textAcount}>Tài khoản chinh</Text>*/}
        <Text style={styles.textItemBLock}>Mã nạp tiền: {item.code}</Text>
        <Text style={styles.textAcount}>{item.created_at}</Text>
      </View>
      <View style={styles.blockAcountPrice}>
        <Text style={styles.textAcount}>Nộp tiền: {item.amount_text || 0}</Text>
        <Text
          style={[
            item.status_class == 'warning'
              ? styles.textAcountCxl
              : styles.textAcountht,
          ]}>
          {item.status_text}
        </Text>
      </View>
    </View>
  );
  const getListData = (startDatePr, endDatePr, statusPr) => {
    WalletModel.transferBankHistory(startDatePr, endDatePr, statusPr)
      .then(data => {
        if (data.status == 1) {
          setListData(data.data);
          setListDataStatus(data.optional.list_status);
          setLoading(false);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  useEffect(() => {
    setLoading(true);
    getListData(startDate, endDate, status.value);
  }, []);
  return (
    <Container style={{flex: 1}}>
      <View style={styles.wrapHeader}>
        <View style={styles.titleHeaderCenter}>
          <View style={styles.blockContentHeader}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <Image source={require('../../../assets/back.png')} />
            </TouchableOpacity>
            <Text style={styles.titleHeader}>Lịch sử giao dịch</Text>
            <TouchableOpacity
              onPress={() => {
                setPopupListFilter(true);
              }}>
              <Image
                source={require('../../../assets/dashboard/Group1755.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <SafeAreaView style={styles.container}>
        <Selection size={16}>
          <View style={styles.blockText}>
            <Text style={styles.textLeftInfo}>
              Có {listData.length > 0 ? listData.length : 0} giao dịch từ ngày
              &nbsp;{startDate} đến ngày {endDate}
            </Text>
          </View>
          <LoadingSkeleton
            loading={loading}
            count={6}
            skeleton={
              <SkeletonPlaceholder>
                <View
                  style={{
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginBottom: 10,
                  }}>
                  <View
                    style={{width: width - 36, height: 87, borderRadius: 8}}
                  />
                </View>
              </SkeletonPlaceholder>
            }>
            <FlatList
              style={{marginBottom: 100}}
              data={listData}
              renderItem={renderItem}
            />
          </LoadingSkeleton>
        </Selection>
      </SafeAreaView>
      <FilterHistoryTransaction
        listStatus={listDataStatus}
        visible={popupListFilter}
        data={status}
        startDateObject={startDate}
        endDateObject={endDate}
        onChange={item => setCloseSelect(item)}
        header={'Lọc lịch sử giao dịch'}
      />
    </Container>
  );
}
