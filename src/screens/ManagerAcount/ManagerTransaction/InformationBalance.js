import React, {useState} from 'react';
import {SafeAreaView, ScrollView, View} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/InformationBalance/styles';
import Header from '../../../components/Header';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';

export default function InformationBalance({navigation}) {
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Thông tin số dư" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Selection size={16}>
            <View style={styles.itemQlTKTPTop}>
              <View>
                <Text style={styles.textLeftInfo}>Tài khoản</Text>
              </View>
              <Text style={styles.textLeftIcon}>Số dư</Text>
            </View>
            <View style={styles.borderBottom}>
              <View style={styles.itemQlTKTP}>
                <View>
                  <Text style={styles.textLeftInfoBottom}>Tài khoản chính</Text>
                </View>
                <Text style={styles.textLeftIconBottom}>40.000 VNĐ</Text>
              </View>
              <View style={styles.itemQlTKTP}>
                <View>
                  <Text style={styles.textLeftInfoBottom}>
                    Tài khoản bên ngoài
                  </Text>
                </View>
                <Text style={styles.textLeftIconBottom}>0 VNĐ</Text>
              </View>
              <View style={styles.itemQlTKTP}>
                <View>
                  <Text style={styles.textLeftInfoBottom}>
                    Tài khoản khuyến mãi 1
                  </Text>
                </View>
                <Text style={styles.textLeftIconBottom}>8,000 VNĐ</Text>
              </View>
              <View style={styles.itemQlTKTP}>
                <View>
                  <Text style={styles.textLeftInfoBottom}>
                    Tài khoản khuyến mãi 2
                  </Text>
                </View>
                <Text style={styles.textLeftIconBottom}>0 VNĐ</Text>
              </View>
            </View>
            <View style={styles.itemQlTKTP}>
              <View>
                <Text style={styles.textLeftTotal}>Tổng số dư</Text>
              </View>
              <Text style={styles.textLeftIconPriceTotal}>48,000 VNĐ</Text>
            </View>
            <View style={styles.blockNote}>
              <Text style={styles.textNote}>
                *Chú thích: {'\n'}- Tài khoản chính là loại tài khoản mà số tiền
                bạn có thể dùng để thanh toán bất kì loại tin nào.{'\n'} - Tài
                khoản khuyến mại 1 là số tiền bạn được khuyến mại thêm vào khi
                nộp tiền. Số tiền này có thể được sử dụng để thanh toán bất kì
                loại tin nào.{'\n'} - Tài khoản khuyến mại 2 là số tiền bạn được
                khuyến mãi thêm vào. Số tiền này chỉ sử dụng khi thanh toán tin
                thường. - Khi thanh toán các loại tin VIP sẽ trừ trong tài khoản
                khuyến khuyến mãi 1 trước sau đó đến tài khoản chính.{'\n'} - Số
                dư cuối cùng = Tài khoản chính + Tài khoản khuyến mãi 1 + Tài
                khoản khuyến mãi 2 - Số nợ hiện tại của bạn (Áp dụng với các
                giao dịch từ ngày 25/09/2013).
              </Text>
            </View>
          </Selection>
        </ScrollView>
      </SafeAreaView>
    </Container>
  );
}
