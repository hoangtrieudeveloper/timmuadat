import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/HistoryTransaction/styles';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import FilterHistoryTransaction from './FilterHistoryTransaction';
import {WalletModel} from '../../../models/wallet';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import LoadingSkeleton from '../../../components/LoadingSkeleton';
import moment from 'moment';
import FilterHistoryBalance from './FilterHistoryBalance';
const {width, height} = Dimensions.get('screen');
export default function HistoryBalance({navigation}) {
  const [startDate, setStartDate] = useState(
    moment(new Date()).day(-7).format('YYYY/MM/DD'),
  );
  const [endDate, setEndDate] = useState(
    moment(new Date()).format('YYYY/MM/DD'),
  );
  const [loading, setLoading] = useState(true);
  const [listData, setListData] = useState([]);
  const [listDataType, setListDataType] = useState([]);
  //filter
  const [popupListFilter, setPopupListFilter] = useState(false);
  const [status, setStatus] = useState({
    name: 'Tất cả',
    value: '',
  });
  const setCloseSelect = item => {
    setStartDate(moment(item.startDateData).format('YYYY/MM/DD'));
    setEndDate(moment(item.endDateData).format('YYYY/MM/DD'));
    setStatus(item.objectData);
    getListData(
      moment(item.startDateData).format('YYYY/MM/DD'),
      moment(item.endDateData).format('YYYY/MM/DD'),
      item.objectData.value,
    );
    setPopupListFilter(false);
  };
  const renderItem = ({item}) => (
    <View style={styles.listItemHistory}>
      <Text style={[styles.textAcount, {marginBottom: 12}]}>
        Loại: <Text style={styles.textItemBLock}>{item.type_text}</Text>
      </Text>
      <View style={styles.blockAcount}>
        <Text style={styles.textItemBLock}>Số tiền: {item.amount}</Text>
        <Text style={styles.textAcount}>{item.created_at}</Text>
      </View>
      <View style={styles.blockAcountPrice}>
        <Text style={styles.textAcount}>
          Số tiền trước: {item.before_amount || 0}
        </Text>
        <Text style={styles.textAcount}>
          Số tiền sau: {item.last_amount || 0}
        </Text>
      </View>
      <View style={[styles.blockAcountPrice, {marginTop: 12}]}>
        <Text style={styles.textAcount}>Nội dung: {item.message}</Text>
      </View>
    </View>
  );
  const getListData = (startDatePr, endDatePr, statusPr) => {
    WalletModel.BalanceHistory(startDatePr, endDatePr, statusPr)
      .then(data => {
        if (data.status == 1) {
          setListData(data.data);
          setLoading(false);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  const getListType = () => {
    WalletModel.balanceHistoryInfo()
      .then(data => {
        if (data.status == 1) {
          setListDataType(data.data.list_type);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  useEffect(() => {
    setLoading(true);
    getListData(startDate, endDate, status.value);
    getListType();
  }, []);
  return (
    <Container style={{flex: 1}}>
      <View style={styles.wrapHeader}>
        <View style={styles.titleHeaderCenter}>
          <View style={styles.blockContentHeader}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <Image source={require('../../../assets/back.png')} />
            </TouchableOpacity>
            <Text style={styles.titleHeader}>Lịch sử số dư</Text>
            <TouchableOpacity
              onPress={() => {
                setPopupListFilter(true);
              }}>
              <Image
                source={require('../../../assets/dashboard/Group1755.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <SafeAreaView style={styles.container}>
        <Selection size={16}>
          <View style={styles.blockText}>
            <Text style={styles.textLeftInfo}>
              Có {listData.length > 0 ? listData.length : 0} giao dịch từ ngày
              &nbsp;{startDate} đến ngày {endDate}
            </Text>
          </View>
          <LoadingSkeleton
            loading={loading}
            count={6}
            skeleton={
              <SkeletonPlaceholder>
                <View
                  style={{
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginBottom: 10,
                  }}>
                  <View
                    style={{width: width - 36, height: 87, borderRadius: 8}}
                  />
                </View>
              </SkeletonPlaceholder>
            }>
            <FlatList
              style={{marginBottom: 100}}
              data={listData}
              renderItem={renderItem}
            />
          </LoadingSkeleton>
        </Selection>
      </SafeAreaView>
      <FilterHistoryBalance
        listStatus={listDataType}
        visible={popupListFilter}
        data={status}
        startDateObject={startDate}
        endDateObject={endDate}
        onChange={item => setCloseSelect(item)}
        header={'Lọc lịch sử số dư'}
      />
    </Container>
  );
}
