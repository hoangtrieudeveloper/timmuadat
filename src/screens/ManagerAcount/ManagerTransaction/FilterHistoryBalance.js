import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  TouchableOpacity,
  View,
} from 'react-native';
import moment from 'moment';
import {Text} from 'react-native-paper';
import Button from '../../../components/Button';
import {styles} from '../../../core/Acount/FilterHistoryBalance/styles';
import Selection from '../../../components/Selection';
import Const from '../../../components/Const';
import SanFrancisco from '../../../components/SanFrancisco';
import CustomDatePicker from '../../../components/DatePicker';
import ListFilterHistoryBalance from '../../../components/ListFilterHistoryBalance/ListFilterHistoryBalance';

const CDatePicker = ({label, date = new Date(), ...props}) => {
  const [visible, setVisible] = useState(false);

  const setDate = date => {
    props.onChange(date);
  };

  return (
    <View>
      <CustomDatePicker
        onDateChange={date => {
          setDate(date);
        }}
        date={date}
        visible={visible}
        onChangeVisible={() => {
          setVisible(false);
        }}
        onChangeDate={date => setDate(date)}
      />

      <TouchableOpacity
        style={[styles.itemQlTKTP, label == 'Từ ngày' ? {marginTop: 0} : null]}>
        <View>
          <Text style={styles.textLeftInfo}>{label}</Text>
        </View>
        <View style={styles.blockAllIcon}>
          <Text style={[styles.textLeftIcon, {color: Const.GRAY_v5}]}>
            {moment(date).format('YYYY/MM/DD')}
          </Text>
          <TouchableOpacity onPress={() => setVisible(true)}>
            <Image
              source={require('../../../assets/Acount/filterpost/bx-calendar.png')}
            />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </View>
  );
};
function FilterHistoryBalance({
  listStatus,
  visible,
  onChange,
  startDateObject,
  endDateObject,
  data,
  header,
  ...props
}) {
  const [startDate, setStartDate] = useState(
    startDateObject || moment(new Date()).day(-7),
  );
  const [endDate, setEndDate] = useState(endDateObject || new Date());
  const [listDataStatus, setListDataStatus] = useState(listStatus || '');
  //trangthai
  const [popupStatus, setPopupStatus] = useState(false);
  const [objectStatus, setObjectStatus] = useState(
    data || {
      value: '',
      name: 'Tất cả',
    },
  );
  const setCloseSelectStatus = item => {
    setObjectStatus(item);
    setPopupStatus(false);
  };
  useEffect(() => {
    if (!visible) {
      return;
    }
    setObjectStatus(data);
    setStartDate(new Date(startDateObject));
    setEndDate(new Date(endDateObject));
    setListDataStatus(listStatus);
  }, [visible, listStatus]);

  const setSubmit = () => {
    console.log('objectStatus', objectStatus);
    onChange({
      objectData: objectStatus,
      startDateData: startDate,
      endDateData: endDate,
    });
  };

  if (!visible) {
    return <View />;
  }

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#F9F9F9F0',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
          }}>
          <View />
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.BLACK_ROOT,
            }}>
            {header}
          </Text>
          <TouchableOpacity onPress={() => setSubmit()}>
            <Image
              source={require('../../../assets/Acount/filterpost/close.png')}
              style={{width: 17, height: 17}}
            />
          </TouchableOpacity>
        </View>
        <SafeAreaView style={styles.container}>
          <ScrollView>
            <Selection size={16}>
              <TouchableOpacity
                onPress={() => {
                  setObjectStatus({
                    value: '',
                    name: 'Tất cả',
                  });
                  setStartDate(moment(new Date()).day(-7));
                  setEndDate(new Date());
                }}
                style={styles.blockTitleQLTK}>
                <Text style={styles.titleQLTK}>Xóa bộ lọc</Text>
              </TouchableOpacity>
              <CDatePicker
                label="Từ ngày"
                date={startDate}
                onChange={date => setStartDate(date)}
              />
              <CDatePicker
                label="Đến ngày"
                date={endDate}
                onChange={date => setEndDate(date)}
              />
              <TouchableOpacity
                onPress={() => setPopupStatus(true)}
                style={[styles.itemQlTKTP]}>
                <View style={styles.mr10}>
                  <Text style={styles.textLeftInfo}>Lọc lịch sử</Text>
                </View>
                <View style={{flexShrink: 1}}>
                  <Text style={[styles.textLeftIcon, {marginRight: 16}]}>
                    {objectStatus.name}
                  </Text>
                  <View style={styles.iconRight}>
                    <Image
                      source={require('../../../assets/Vector.png')}
                      style={styles.vecterNext}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </Selection>
          </ScrollView>
        </SafeAreaView>
        <Selection size={16} style={styles.mrt16}>
          <Button
            titleStyle={styles.buttonTitle}
            style={styles.buttonLogin}
            type="blue"
            title="Áp dụng"
            mode="contained"
            onPress={() => {
              setSubmit();
            }}
          />
        </Selection>
      </View>
      <ListFilterHistoryBalance
        listDtStatus={listDataStatus}
        visible={popupStatus}
        data={objectStatus}
        onChange={item => setCloseSelectStatus(item)}
        header={'Lịch sử'}
      />
    </View>
  );
}

export default FilterHistoryBalance;
