import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import {styles} from '../../../core/Acount/detailpost/styles';
import Header from '../../../components/Header';
import {Container} from 'native-base';
import Selection from '../../../components/Selection';
import ModalProcess from '../../../components/ListModal/ModalProcess';

export default function DetailPost({navigation}) {
  const [modalProcess, setModalProcess] = useState(false);
  const setCloseModal = item => {
    setModalProcess(false);
  };
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Chi tiết đăng tin" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ModalProcess
          visible={modalProcess}
          title={'Xác nhận xoá tin đăng'}
          content={
            'Nếu xoá tin rao này bạn sẽ không thể khôi phục lại được. Bạn cũng sẽ không được hoàn lại tiền hoặc lần thanh toán thêm. Bạn có chắc chắn muốn xoá tin rao này?'
          }
          typeButton={2}
          onChange={item => setCloseModal(item)}
        />
        <ScrollView>
          <Image
            source={require('../../../assets/dashboard/Group2391.png')}
            style={styles.imageItem}
          />
          <Selection size={16}>
            <Text style={styles.title}>
              Chính chủ bán nhanh căn biệt thự song lập Ngọc Trai 151.3m2,
              Vinhomes Ocean Park. LH 0915972886
            </Text>
            <Text style={styles.price_dt}>1.65 tỷ · 75 m² · 2 PN · 2 WC</Text>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Loại tin rao</Text>
              <Text style={styles.newRight}>Nhà đất bán</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Loại bất động sản</Text>
              <Text style={styles.newRight}>Bán căn hộ chung cư</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Địa chỉ</Text>
              <Text style={styles.newRight}>
                S1-06 Vinhomes Ocean Park, Đa Tốn, Gia lâm, Hà Nội
              </Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Mã tin đăng</Text>
              <Text style={styles.newRight}>330561</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Loại tin đăng</Text>
              <Text style={styles.newRight}>Tin Vip 3</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Thời gian</Text>
              <Text style={styles.newRight}>06/10/2021 đến 13/10/2021</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Trạng thái</Text>
              <Text style={[styles.newRight, {color: '#429134'}]}>
                Đã duyệt
              </Text>
            </View>
          </Selection>
          <View style={styles.blockEditTrash}>
            <TouchableOpacity
              onPress={() => {
                setModalProcess(true);
              }}
              style={styles.blockItem}>
              <Image
                source={require('../../../assets/Acount/detailPost/bxs-edit.png')}
              />
              <Text style={styles.textItem}>Sửa</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setModalProcess(true);
              }}
              style={styles.blockItem}>
              <Image
                source={require('../../../assets/Acount/draftNews/trash.png')}
                style={{width: 19, height: 19}}
              />
              <Text style={styles.textItem}>Xóa</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </Container>
  );
}
