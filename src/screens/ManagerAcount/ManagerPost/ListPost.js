import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import {styles} from '../../../core/Acount/managerpost/styles';
import Header from '../../../components/Header';
import {Container} from 'native-base';
import {TabView} from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import RentalNews from './RentalNews';
import DraftNews from './DraftNews';
import FilterPost from './FilterPost';
import {ProductModel} from '../../../models/product';
import moment from 'moment';

export default function ListPost({navigation}) {
  const [loadingTabListData, setLoadingTabListData] = useState(true);
  const [loadingTabListTWO, setLoadingTabListTWO] = useState(true);
  const [listData, setListData] = useState([]);
  const [listDataTypeTwo, setListDataTypeTwo] = useState([]);
  //filter
  const [popupListFilter, setPopupListFilter] = useState(false);
  const [objectFilter, setObjectFilter] = useState('');
  const setCloseSelect = item => {
    setObjectFilter(item);
    setPopupListFilter(false);
    if (index === 1) {
      getListDataBySeller(
        moment(item?.objectStartdate).format('DD/MM/YYYY'),
        moment(item?.objectEnddate).format('DD/MM/YYYY'),
        item?.objectAdver.value,
        item?.objectCategories.value,
        item?.objectCity.id,
        item?.objectProductType.id,
      );
    } else {
      getListDataBySellerTypeTwo(
        moment(item?.objectStartdate).format('DD/MM/YYYY'),
        moment(item?.objectEnddate).format('DD/MM/YYYY'),
        item?.objectAdver.value,
        item?.objectCategories.value,
        item?.objectCity.id,
        item?.objectProductType.id,
      );
    }
  };
  const setCloseSelectTab = item => {
    setPopupListFilter(true);
  };
  const _handleIndexChange = i => setIndex(i);
  const [index, setIndex] = React.useState(0);
  const renderScene = ({route}) => {
    switch (route.key) {
      case '1':
        return (
          <RentalNews
            Onchange={setCloseSelectTab}
            loadingTab={loadingTabListData}
            dataList={listDataTypeTwo}
            navigation={navigation}
          />
        );
      case '2':
        return (
          <DraftNews
            Onchange={setCloseSelectTab}
            loadingTab={loadingTabListTWO}
            dataList={listData}
            navigation={navigation}
          />
        );
      default:
        return null;
    }
  };
  const [routes] = React.useState([
    {key: '1', title: 'Tin cần mua/cần thuê'},
    {key: '2', title: 'Tin rao bán/cho thuê'},
  ]);
  const _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={[styles.tabBar]}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={[
                styles.tabItem,
                index === i ? {backgroundColor: '#004182'} : {},
              ]}
              onPress={() => {
                setIndex(i);
                if (i === 1) {
                  getListDataBySeller();
                } else {
                  getListDataBySellerTypeTwo();
                }
              }}>
              <Animated.Text
                style={[
                  styles.titleTab,
                  index === i ? {color: '#ffffff'} : {color: '#000000'},
                ]}>
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  const getListDataBySeller = (
    start_date,
    end_date,
    categories_type,
    categories_id,
    province_id,
    product_type,
  ) => {
    setLoadingTabListTWO(true);
    ProductModel.getListBySeller(
      1,
      start_date,
      end_date,
      categories_type,
      categories_id,
      province_id,
      product_type,
    )
      .then(data => {
        if (data.status == 1) {
          setTimeout(() => {
            setLoadingTabListTWO(false);
          }, 1000);
          setListData(data.data.data);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  const getListDataBySellerTypeTwo = (
    start_date,
    end_date,
    categories_type,
    categories_id,
    province_id,
    product_type,
  ) => {
    setLoadingTabListData(true);
    ProductModel.getListBySeller(
      2,
      start_date,
      end_date,
      categories_type,
      categories_id,
      province_id,
      product_type,
    )
      .then(data => {
        if (data.status == 1) {
          setTimeout(() => {
            setLoadingTabListData(false);
          }, 1000);
          setListDataTypeTwo(data.data.data);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  useEffect(() => {
    if (index === 0) {
      getListDataBySellerTypeTwo();
    } else {
      getListDataBySeller();
    }
    setLoadingTabListData(true);
    setLoadingTabListTWO(true);
  }, []);
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Quản lý đăng tin" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <TabView
          swipeEnabled={false}
          navigationState={{index, routes}}
          renderScene={renderScene}
          renderTabBar={_renderTabBar}
          onIndexChange={_handleIndexChange}
        />
      </SafeAreaView>
      <FilterPost
        visible={popupListFilter}
        data={objectFilter}
        onChange={item => setCloseSelect(item)}
        header={'Lọc đăng tin'}
      />
    </Container>
  );
}
