import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Alert,
  AsyncStorage,
  FlatList,
} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../../components/Button';
import {styles} from '../../../core/Acount/FilterPost/styles';
import Header from '../../../components/Header';
import Selection from '../../../components/Selection';
import PopupListCountry from '../../../components/ListPopup/ListCountry';
import Const from '../../../components/Const';
import SanFrancisco from '../../../components/SanFrancisco';
import CustomDatePicker from '../../../components/DatePicker';
import moment from 'moment';
import TypeAdvertisement from '../../../components/TypeAdvertisement/TypeAdvertisement';
import Categories from '../../../components/Categories/Categories';
import ListCity from '../../../components/ListCity/ListCity';
import TypePostType from '../../../components/TypePostType/TypePostType';

const CDatePicker = ({label, dateE = new Date(), ...props}) => {
  const [visible, setVisible] = useState(false);

  const setDate = date => {
    props.onChange(date);
  };

  return (
    <View>
      <CustomDatePicker
        onDateChange={e => {
          setDate(e);
        }}
        date={dateE}
        visible={visible}
        onChangeVisible={() => {
          setVisible(false);
        }}
        onChangeDate={d => setDate(d)}
      />
      <TouchableOpacity
        style={[
          styles.itemQlTKTP,
          label == 'Ngày bắt đầu' ? {marginTop: 0} : null,
        ]}>
        <View>
          <Text style={styles.textLeftInfo}>{label}</Text>
        </View>
        <View style={styles.blockAllIcon}>
          <Text style={[styles.textLeftIcon, {color: Const.GRAY_v5}]}>
            {moment(dateE).format('YYYY/MM/DD')}
          </Text>
          <TouchableOpacity onPress={() => setVisible(true)}>
            <Image
              source={require('../../../assets/Acount/filterpost/bx-calendar.png')}
            />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </View>
  );
};

function FilterPost({visible, onChange, data, header, ...props}) {
  const [startDate, setStartDate] = useState(
    object?.objectEnddate || moment(new Date()).day(-30),
  );
  const [endDate, setEndDate] = useState(object?.objectStartdate || new Date());
  const [object, setObject] = useState(data || '');

  //loaitinrao
  const [popupListAdver, setPopupListAdver] = useState(false);
  const [objectAdver, setObjectAdver] = useState({
    name: object?.objectAdver?.name || 'Tất cả',
    value: object?.objectAdver?.value || '',
  });
  const setCloseSelect = item => {
    setObjectAdver(item);
    setPopupListAdver(false);
  };
  //loaibatdongsan
  const [popupCategories, setPopupCategories] = useState(false);
  const [objectCategories, setObjectCategories] = useState({
    name: object?.objectCategories?.name || 'Tất cả',
    value: object?.objectCategories?.value || '',
  });
  const setCloseSelectCategories = item => {
    setObjectCategories(item);
    setPopupCategories(false);
  };
  //thanhPho
  const [popupCity, setPopupCity] = useState(false);
  const [objectCity, setObjectCity] = useState({
    name: object?.objectCity?.name || 'Trên toàn quốc',
    id: object?.objectCity?.value || '',
  });
  const setCloseSelectCity = item => {
    setObjectCity(item);
    setPopupCity(false);
  };
  //Vitri
  const [popupListProductType, setPopupListProductType] = useState(false);
  const [objectProductType, setObjectProductType] = useState({
    name: object?.objectProductType?.name || 'Tất cả',
    id: object?.objectProductType?.id || '',
    error: '',
  });

  const setCloseSelectProductType = item => {
    setObjectProductType(item);
    setPopupListProductType(false);
  };
  useEffect(() => {
    if (!visible) {
      return;
    }
    setObject(data);
  }, [visible, data]);

  const setSubmit = () => {
    onChange({
      objectEnddate: endDate,
      objectStartdate: startDate,
      objectAdver: objectAdver,
      objectCategories: objectCategories,
      objectCity: objectCity,
      objectProductType: objectProductType,
    });
  };

  if (!visible) {
    return <View />;
  }
  const ClearData = () => {
    setStartDate(moment(new Date()).day(-30));
    setEndDate(new Date());
    setObject('');
    setObjectProductType({
      name: 'Tất cả',
      id: '',
      error: '',
    });
    setObjectCity({
      name: 'Trên toàn quốc',
      id: '',
    });
    setObjectCategories({
      name: 'Tất cả',
      value: '',
    });
    setObjectAdver({
      name: 'Tất cả',
      value: '',
    });
  };

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#F9F9F9F0',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
          }}>
          <TouchableOpacity onPress={() => setSubmit()}>
            <Image source={require('../../../assets/back.png')} />
          </TouchableOpacity>
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.BLACK_ROOT,
            }}>
            {header}
          </Text>
          <TouchableOpacity onPress={() => setSubmit()}>
            <Text
              style={{
                ...SanFrancisco.regular,
                fontSize: 17,
                lineHeight: 20,
                color: Const.BLUE,
              }}>
              Xong
            </Text>
          </TouchableOpacity>
        </View>
        <SafeAreaView style={styles.container}>
          <ScrollView>
            <Selection size={16}>
              <TouchableOpacity
                onPress={() => {
                  ClearData();
                }}
                style={styles.blockTitleQLTK}>
                <Text style={styles.titleQLTK}>Xóa bộ lọc</Text>
              </TouchableOpacity>
              <CDatePicker
                label="Từ ngày"
                dateE={startDate}
                onChange={date => {
                  setStartDate(date);
                }}
              />
              <CDatePicker
                label="Đến ngày"
                dateE={endDate}
                onChange={date => {
                  setEndDate(date);
                }}
              />
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.textLeftInfo}>Loại tin rao</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>{objectAdver.name}</Text>
                  <Image
                    source={require('../../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupCategories(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.textLeftInfo}>Loại bất động sản</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>
                    {objectCategories.name}
                  </Text>
                  <Image
                    source={require('../../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupCity(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.textLeftInfo}>Tỉnh/Thành phố</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>{objectCity.name}</Text>
                  <Image
                    source={require('../../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupListProductType(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.textLeftInfo}>Loại tin đăng</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>
                    {objectProductType.name}
                  </Text>
                  <Image
                    source={require('../../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
            </Selection>
          </ScrollView>
        </SafeAreaView>
        <Selection size={16} style={styles.mrt16}>
          <Button
            titleStyle={styles.buttonTitle}
            style={styles.buttonLogin}
            type="blue"
            title="Áp dụng"
            mode="contained"
            onPress={() => {
              setSubmit();
            }}
          />
        </Selection>
      </View>
      <TypeAdvertisement
        visible={popupListAdver}
        data={objectAdver}
        onChange={item => setCloseSelect(item)}
        header={'Loại tin rao'}
      />
      <Categories
        visible={popupCategories}
        data={objectCategories}
        onChange={item => setCloseSelectCategories(item)}
        keyFilter={objectAdver.value}
        header={'Loại bất động sản'}
      />
      <ListCity
        visible={popupCity}
        data={objectCity}
        onChange={item => setCloseSelectCity(item)}
        header={'Tỉnh/Thành phố'}
      />
      <TypePostType
        visible={popupListProductType}
        data={objectProductType}
        onChange={item => setCloseSelectProductType(item)}
        header={'Loại tin đăng'}
      />
    </View>
  );
}

export default FilterPost;
