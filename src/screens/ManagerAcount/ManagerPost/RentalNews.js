import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  View,
  TouchableOpacity,
  FlatList,
  Dimensions,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/retalnews/styles';
import Selection from '../../../components/Selection';
import Swipeable from 'react-native-swipeable';
import Const from '../../../components/Const';
import {useFocusEffect} from '@react-navigation/native';
import LoadingSkeleton from '../../../components/LoadingSkeleton';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
const {width, height} = Dimensions.get('screen');

export default function RentalNews({
  Onchange,
  loadingTab,
  dataList,
  navigation,
}) {
  const setWallet = () => {
    Onchange(1);
  };
  const [listData, setListData] = useState(dataList || []);
  useFocusEffect(
    React.useCallback(() => {
      setListData(dataList || []);
    }, [dataList]),
  );
  const textStatus = item => {
    if (item === 2) {
      return <Text style={{color: Const.RED_v2}}>Từ chối</Text>;
    } else if (item === 3) {
      return <Text style={{color: Const.GREEN}}>Đã duyệt</Text>;
    } else {
      return <Text style={{color: Const.GRAY_v4}}>Chờ duyệt</Text>;
    }
  };
  const rightButtons = [
    <TouchableOpacity style={styles.StatusSwipeableLeft}>
      <Image
        source={require('../../../assets/Acount/rentalNews/bxs-edit.png')}
      />
      <Text style={styles.textStatus}>Sửa</Text>
    </TouchableOpacity>,
    <TouchableOpacity style={styles.StatusSwipeableRight}>
      <Image
        source={require('../../../assets/Acount/rentalNews/bx-trash.png')}
      />
      <Text style={styles.textStatus}>Xóa</Text>
    </TouchableOpacity>,
  ];
  const renderItem = ({item}) => (
    <Swipeable rightButtons={rightButtons}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('DetailPostDashboard', {infor: item});
        }}
        style={styles.blockItem}>
        <View style={styles.rightItem}>
          {item?.avatar && item?.avatar === '' && item?.avatar === null ? (
            <Image
              source={{
                uri: item?.avatar,
              }}
              style={styles.imageItem}
            />
          ) : (
            <Image
              source={require('../../../assets/dashboard/no_image.png')}
              style={styles.imageItem}
            />
          )}
          <View style={styles.titleImage}>
            <Text style={styles.titleRightItem}>{item?.title}</Text>
            <View>
              <Text style={styles.PriceInfo}>
                {item?.price} · {item?.area || 0} m² ·{' '}
                {item?.bedroom_numbers || 0} PN · {item?.toilet_numbers || 0} WC
              </Text>
            </View>
            <View>
              <Text style={styles.timeRightItem}>
                {item?.start_date} · {item?.product_type_text} ·
                {textStatus(item.status)}
              </Text>
            </View>
          </View>
        </View>
        <Image
          source={require('../../../assets/Vector.png')}
          style={styles.imageRightTitle}
        />
      </TouchableOpacity>
    </Swipeable>
  );

  return (
    <SafeAreaView style={styles.container}>
      <LoadingSkeleton
        loading={loadingTab}
        count={6}
        skeleton={
          <SkeletonPlaceholder>
            <View
              style={{
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                marginBottom: 10,
              }}>
              <View style={{width: width - 36, height: 87, borderRadius: 8}} />
            </View>
          </SkeletonPlaceholder>
        }>
        <Selection size={16}>
          <View style={styles.totalNews}>
            <Text style={styles.textTotal}>
              Có {listData.length > 0 ? listData.length : 0} tin rao tìm thấy
            </Text>
            <TouchableOpacity
              onPress={() => {
                setWallet(1);
              }}
              style={styles.iconFind}>
              <Image
                source={require('../../../assets/dashboard/Group1755.png')}
              />
            </TouchableOpacity>
          </View>
        </Selection>
        <FlatList data={listData} renderItem={renderItem} />
      </LoadingSkeleton>
    </SafeAreaView>
  );
}
