export function titleValidator(name) {
  if (!name) {
    return 'Nhập tiêu đề tin';
  }
  if (name.length > 99) {
    return 'Tiêu đề tin không được quá 99 ký tự';
  }
  return '';
}
