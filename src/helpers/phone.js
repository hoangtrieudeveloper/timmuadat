export function phoneValidator(phone) {
  if (!phone) {
    return 'Nhập số điện thoại';
  }
  if (phone.length < 9 || phone.length > 10) {
    return 'Số điện thoại phải từ 9 đến 10 số';
  }
  return null;
}
