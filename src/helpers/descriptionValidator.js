export function descriptionValidator(name) {
  if (!name) {
    return 'Nhập nội dung mô tả';
  }
  if (name.length > 3000) {
    return 'Nội dung mô tả không được quá 3000 ký tự';
  }
  return '';
}
