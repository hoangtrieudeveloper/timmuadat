export function old_passwordValidator(password) {
  if (!password) {
    return 'Mật khẩu không khớp';
  }
  if (password.length < 5) {
    return 'Mật khẩu cũ không được nhỏ hơn 5 kí tự';
  }
  return null;
}
