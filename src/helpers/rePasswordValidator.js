export function rePasswordValidator(password) {
  if (!password) {
    return 'Nhập lại mật khẩu';
  }
  if (password.length < 5) {
    return 'Mật khẩu không được nhỏ hơn 5 kí tự';
  }
  return null;
}
