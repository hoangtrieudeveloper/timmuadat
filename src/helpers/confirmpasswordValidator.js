export function confirmpasswordValidator(password) {
  if (!password) {
    return 'Nhập mật khẩu';
  }
  if (password.length < 5) {
    return 'Mật khẩu không được nhỏ hơn 5 kí tự';
  }
  return null;
}
