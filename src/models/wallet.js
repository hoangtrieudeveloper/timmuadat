import {config} from '../config/config';

export const WalletModel = {
  bank_template: () => {
    return fetch(config.endpointUrl + 'api/wallet/bank_template', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  list_deposit_method: () => {
    return fetch(config.endpointUrl + 'api/wallet/deposit_method', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  deposit_by_transfer_bank: (bank_id, amount) => {
    return fetch(config.endpointUrl + 'api/wallet/deposit_by_transfer_bank', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        bank_id: bank_id,
        amount: amount,
      }),
    }).then(data => data.json());
  },
  payDirectAtHomeInfo: () => {
    return fetch(config.endpointUrl + 'api/wallet/pay_direct_at_home_info', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  transferBankHistory: (date_from, date_to, status) => {
    let param = 'api/wallet/transfer_bank_history';
    if (date_from !== undefined) {
      param += `?date_from=${date_from}`;
    }
    if (date_to !== undefined) {
      param += `&date_to=${date_to}`;
    }
    if (status !== undefined && status !== '') {
      param += `&status=${status}`;
    }
    console.log('param', config.endpointUrl + param);
    return fetch(config.endpointUrl + param, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  BalanceHistory: (date_from, date_to, type) => {
    let param = 'api/transaction/balance_history';
    if (date_from !== undefined) {
      param += `?date_from=${date_from}`;
    }
    if (date_to !== undefined) {
      param += `&date_to=${date_to}`;
    }
    if (type !== undefined && type !== '') {
      param += `&type=${type}`;
    }
    console.log('param', config.endpointUrl + param);
    return fetch(config.endpointUrl + param, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  balanceHistoryInfo: () => {
    return fetch(config.endpointUrl + 'api/wallet/balance_history_info', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
};
