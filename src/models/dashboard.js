import {config} from '../config/config';

export const DashboardModel = {
  indexAction: () => {
    return fetch(config.endpointUrl + 'api/dashboard/index', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        // Authorization: global.token,
      },
    }).then(data => data.json());
  },
  filterDataAction: () => {
    return fetch(config.endpointUrl + 'api/dashboard/filterData', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        // Authorization: global.token,
      },
    }).then(data => data.json());
  },
  getPriceConditions: filter => {
    return fetch(
      config.endpointUrl + 'api/dashboard/priceConditions/' + filter,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          // Authorization: global.token,
        },
      },
    ).then(data => data.json());
  },
  getListInterestProducts: (
    province_id,
    district_id,
    ward_id,
    home_direction,
    bedroom_numbers,
    category_type,
    categories_id,
    price_id,
    area_id,
    project_id,
    keyword,
    sort,
    limit,
  ) => {
    let param = 'api/dashboard/listInterestProducts';
    if (province_id !== '' && province_id !== undefined) {
      param += `?province_id=${province_id}`;
    }
    if (district_id !== '' && province_id !== undefined) {
      param += `&district_id=${district_id}`;
    }
    if (ward_id !== '' && ward_id !== undefined) {
      param += `&ward_id=${ward_id}`;
    }
    if (home_direction !== '' && home_direction !== undefined) {
      param += `&home_direction=${home_direction}`;
    }
    if (bedroom_numbers !== '' && bedroom_numbers !== undefined) {
      param += `&bedroom_numbers=${bedroom_numbers}`;
    }
    if (category_type !== '' && category_type !== undefined) {
      param += `&category_type=${category_type}`;
    }
    if (categories_id !== '' && categories_id !== undefined) {
      param += `&categories_id=${categories_id}`;
    }
    if (price_id !== '' && price_id !== undefined) {
      param += `&price_id=${price_id}`;
    }
    if (area_id !== '' && area_id !== undefined) {
      param += `&area_id=${area_id}`;
    }
    if (project_id !== '' && project_id !== undefined) {
      param += `&project_id=${project_id}`;
    }
    if (keyword !== '' && keyword !== undefined) {
      param += `&keyword=${keyword}`;
    }
    if (sort !== '' && sort !== undefined) {
      param += `&sort=${sort}`;
    }
    if (limit !== '' && limit !== undefined) {
      param += `&limit=${limit}`;
    }
    return fetch(config.endpointUrl + param, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        // Authorization: global.token,
      },
    }).then(data => data.json());
  },
};
