import {config} from '../config/config';

export const ProductModel = {
  detailProduct: id => {
    return fetch(config.endpointUrl + 'api/products/detail/' + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  listRelease: id => {
    return fetch(config.endpointUrl + 'api/products/release/' + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  listPriceType: id => {
    return fetch(config.endpointUrl + 'api/products/priceType/' + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  listPriceConditions: id => {
    return fetch(config.endpointUrl + 'api/dashboard/priceConditions/' + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  listAreaConditions: id => {
    return fetch(config.endpointUrl + 'api/dashboard/areaConditions', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  listProductTypes: () => {
    return fetch(config.endpointUrl + 'api/products/productTypes', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  createProductTypeSell: (
    title,
    categories_type,
    categories_id,
    province_id,
    district_id,
    ward_id,
    street_id,
    project_id,
    address,
    lat,
    lng,
    area_condition,
    price,
    price_type,
    description,
    seller_name,
    seller_address,
    seller_phone_number,
    seller_email,
    start_date,
    end_date,
    images,
    floor_numbers,
    bedroom_numbers,
    toilet_numbers,
    width,
    land_width,
    home_direction,
    baconly_direction,
    furniture,
    legality,
    product_type,
  ) => {
    return fetch(config.endpointUrl + 'api/products/createProductTypeSell', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        title: title,
        categories_type: categories_type,
        categories_id: categories_id,
        province_id: province_id,
        district_id: district_id,
        ward_id: ward_id,
        street_id: street_id,
        project_id: project_id,
        address: address,
        lat: lat,
        lng: lng,
        area_condition: area_condition,
        price: price,
        price_type: price_type,
        description: description,
        seller_name: seller_name,
        seller_address: seller_address,
        seller_phone_number: seller_phone_number,
        seller_email: seller_email,
        start_date: start_date,
        end_date: end_date,
        images: images,
        floor_numbers: floor_numbers,
        bedroom_numbers: bedroom_numbers,
        toilet_numbers: toilet_numbers,
        width: width,
        land_width: land_width,
        home_direction: home_direction,
        baconly_direction: baconly_direction,
        furniture: furniture,
        legality: legality,
        product_type: product_type,
      }),
    }).then(data => data.json());
  },
  createProductTypeBuy: (
    title,
    categories_type,
    categories_id,
    province_id,
    district_id,
    ward_id,
    street_id,
    project_id,
    address,
    lat,
    lng,
    area_condition,
    price_condition,
    description,
    seller_name,
    seller_address,
    seller_phone_number,
    seller_email,
    start_date,
    end_date,
    images,
  ) => {
    return fetch(config.endpointUrl + 'api/products/createProductTypeBuy', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        title: title,
        categories_type: categories_type,
        categories_id: categories_id,
        province_id: province_id,
        district_id: district_id,
        ward_id: ward_id,
        street_id: street_id,
        project_id: project_id,
        address: address,
        lat: lat,
        lng: lng,
        area_condition: area_condition,
        price_condition: price_condition,
        description: description,
        seller_name: seller_name,
        seller_address: seller_address,
        seller_phone_number: seller_phone_number,
        seller_email: seller_email,
        start_date: start_date,
        end_date: end_date,
        images: images,
      }),
    }).then(data => data.json());
  },
  uploadFile: responseImg => {
    const formData = new FormData();
    formData.append('avatar', {
      uri: responseImg.uri.replace('file://', ''),
      name: responseImg.fileName || responseImg.uri.split('/').pop(),
      type: responseImg.type,
    });
    return fetch(config.endpointUrl + 'api/images/upload', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: global.token,
      },
      body: formData,
    }).then(data => data.json());
  },
  getListBySeller: (
    group_type,
    start_date,
    end_date,
    categories_type,
    categories_id,
    province_id,
    product_type,
  ) => {
    let param = 'api/products/bySeller';
    if (group_type !== undefined) {
      param += `?group_type=${group_type}`;
    }
    if (start_date !== undefined) {
      param += `&start_date=${start_date}`;
    }
    if (end_date !== undefined) {
      param += `&end_date=${end_date}`;
    }
    if (categories_type !== undefined) {
      param += `&categories_type=${categories_type}`;
    }
    if (categories_id !== undefined) {
      param += `&categories_type=${categories_id}`;
    }
    if (province_id !== undefined) {
      param += `&province_id=${province_id}`;
    }
    if (product_type !== undefined) {
      param += `&product_type=${product_type}`;
    }
    return fetch(config.endpointUrl + param, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
};
