import {config} from '../config/config';

export const ProjectModel = {
  getListProject: (q, province_id, district_id) => {
    let param = '/api/projects';
    if (q !== undefined) {
      param += `?q=${q}`;
    }
    if (province_id !== undefined) {
      param += `&province_id=${province_id}`;
    }
    if (district_id !== undefined) {
      param += `&district_id=${district_id}`;
    }
    return fetch(config.endpointUrl + param, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
};
