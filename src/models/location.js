import {config} from '../config/config';

export const LocationModel = {
  getListProvinces: filter => {
    return fetch(config.endpointUrl + 'api/location/provinces', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  getListDistricts: filter => {
    return fetch(
      config.endpointUrl + 'api/location/districts?province_id=' + filter,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: global.token,
        },
      },
    ).then(data => data.json());
  },
  getListWardsApi: (keyFilterCity, keyFilterDisctrict) => {
    let param = '/api/location/wards';
    if (keyFilterCity !== undefined) {
      param += `&province_id=${keyFilterCity}`;
    }
    if (keyFilterDisctrict !== undefined) {
      param += `&district_id=${keyFilterDisctrict}`;
    }
    console.log('param', config.endpointUrl + param);
    return fetch(config.endpointUrl + param, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  getListStreetsApi: (keyFilterCity, keyFilterDisctrict) => {
    let param = '/api/location/streets';
    if (keyFilterCity !== undefined) {
      param += `&province_id=${keyFilterCity}`;
    }
    if (keyFilterDisctrict !== undefined) {
      param += `&district_id=${keyFilterDisctrict}`;
    }
    console.log('param', config.endpointUrl + param);
    return fetch(config.endpointUrl + param, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
};
